<?php
class CompanyCore extends ObjectModel
{
    public $active = true;

    public $companyName = null;
    public $address1 = null;
    public $address2 = null;
    public $state   = null;
    public $city    = null;
    public $country = null;
    public $zipcode = null;
    public $company_size = null;
    public $company_type = null;

    public static $definition = array(
          'table' => 'company',
          'primary' => 'id_company',
          'multilang' => false,
          'fields' => array(
            'companyName' => array('type' => self::TYPE_STRING, 'lang' => false, 'validate' => 'isGenericName', 'size' => 128),
            'active' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'address1' => array('type' => self::TYPE_STRING, 'lang' => false, 'validate' => 'isGenericName', 'size' => 128),
            'address2' => array('type' => self::TYPE_STRING, 'lang' => false, 'validate' => 'isGenericName', 'size' => 128),
            'state' => array('type' => self::TYPE_STRING, 'lang' => false, 'validate' => 'isGenericName', 'size' => 128),
            'city' => array('type' => self::TYPE_STRING, 'lang' => false, 'validate' => 'isGenericName', 'size' => 128),
            'country' => array('type' => self::TYPE_STRING, 'lang' => false, 'validate' => 'isGenericName', 'size' => 128),
            'zipcode' => array('type' => self::TYPE_STRING, 'lang' => false, 'validate' => 'isGenericName', 'size' => 128),
            'company_size' => array('type' => self::TYPE_STRING, 'lang' => false, 'size' => 128),
            'company_type' => array('type' => self::TYPE_STRING, 'lang' => false, 'size' => 128),
          ),
      );
    public function _getCompanyList()
    {
        $company = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('SELECT * FROM `'._DB_PREFIX_.'company` WHERE `active`=1 ORDER BY companyName ASC');

        return $company;
        /*
        foreach($company as $key)
        {
            $data[] = $key;
        }
        return $data;
        */
    }

    public static function hasSameComany($request, $company)
    {
            $sql = 'SELECT *
                FROM `'._DB_PREFIX_.'customer`
                WHERE `email` = \''.pSQL($request->email).'\' AND `active` = 0;';
        $response =  Db::getInstance()->ExecuteS($sql);

        if(!empty($response)) {
            if($response[0]['company_id'] == $company['id_company']) {
                return true;
            }
        }

        return false;
    }

    public function _getCompanyName($id)
    {
        //echo $query = 'SELECT companyName FROM `'._DB_PREFIX_.'company` WHERE `id_company`="'.$id.'"';
        $company = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('SELECT companyName FROM `'._DB_PREFIX_.'company` WHERE `id_company`="'.$id.'"');
        //print_r($company);
        //echo 'Company Name: '.$company[0]['companyName'];
        //exit;
        return $company[0]['companyName'];
    }

    /*
     * Check company exists or not in company table
     */
    public function checkCompanyByName($companyName)
    {
        $company = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('SELECT count(*) as countCompany FROM `'._DB_PREFIX_.'company` WHERE `companyName`="'.pSQL($companyName).'"');
        return $company['0']['countCompany'];
    }

    public function getCompanyDetails($companyId)
    {
        $sql = "SELECT * FROM csb_company WHERE id_company = '".$companyId."'";
        $row = Db::getInstance()->getRow($sql);
        return $row;
    }
}