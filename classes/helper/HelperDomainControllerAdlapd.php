<?php
/**
* This class is a helper class for adlapd authentication.
*/
putenv('LDAPTLS_REQCERT=allow');

class HelperDomainControllerAdlapd
{
    /**
     * $userdn user dn
     * 
     * @var mixed
     */
    protected $userdn = LDAP_BASE_DN;

    /**
     * $searchBase search base
     * 
     * @var mixed
     */
    protected $searchBase;

    /**
     * $ip ldap ip
     * @var String
     */
    protected $ldapIp = LDAP_IP;

    /**
     * $port Ldap port
     * @var String
     */
    protected $port = LDAP_PORT;

    /**
     * $username LDAP username
     * 
     * @var mixed
     */
    protected $username = LDAP_USERNAME;

    /**
     * $password Ldap password
     * @var mixed
     */
    protected $password = LDAP_PASSWORD;

    /**
     * $ldapConn ldap connection object
     * @var mixed
     */
    protected $ldapConn;
    
    /**
     * $ldapConn ldap connection object
     * @var mixed
     */
    protected $ldapDomain = LDAP_DOMAIN;

    /**
     * $ldap Ldap instance
     * @var Object
     */
    protected $ldap;

  
    /**
     * 
     * checkCompany check company
     * 
     * @return Boolean
     */
    public function checkCompany($companyName)
    {
       $groupdn = preg_replace('/ou=Users,/', "ou=Users,ou=$companyName,", $this->userdn);
       return $result  = $this->checkGroupEx($groupdn);
    }

    public function createGroup($dataToSendRemedy)
    {
        $ip = $this->ldapIp;  // WAN IP goes here;
        $ldap_url = "ldap://$ip";
        $ldaps_url = "ldaps://$ip";
        $ldap_domain = $this->ldapDomain;
        $ldap_dn = $this->userdn;

        // Unsecure - WORKS
        // $ldap_conn = ldap_connect( $ldap_url ) or die("Could not connect to LDAP server ($ldap_url)");
        //alternate connection method 
        $ldap_conn=ldap_connect( $ip, 389 ) or die("Could not connect to LDAP server (IP: $ip, PORT: 389)");  
        // Secure - DOESN'T WORK
        //$ldap_conn = ldap_connect( $ldap_url ) or die("Could not connect to LDAP server ($ldaps_url)");
        //alternate connection method 
        // $ldap_conn=ldap_connect( $ip, 636 ) or die("Could not connect to LDAP server (IP: $ip, PORT: 636)");  

        ldap_set_option($ldap_conn, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($ldap_conn, LDAP_OPT_REFERRALS, 0);

        $username = $this->username;
        $password = $this->password; 

        // bind using admin username and password
        // could also use dn... ie. CN=Administrator,CN=Users,DC=TestDomain,DC=local
        // 
        $result = ldap_bind($ldap_conn, "$username", $password ) or die("<br>Error: Couldn't bind to server using supplied credentials!");
        if($result){
           return  $this->ldapAddUser($ldap_conn,$ldap_dn,$dataToSendRemedy);
        }else{
            die("<br>Error: Couldn't not !");
        }
    }

    public function createUserInLDAP($id_customer)
    {
        $sql = "SELECT  * FROM "._DB_PREFIX_."customer WHERE id_customer =$id_customer";
        $result = Db::getInstance()->executeS($sql);

        return $this->createGroup($result[0]);
        
    }

    protected function ldapAddUser($ldap_conn, $ou_dn, $dataToSendRemedy)
    {
        $dn = "CN={$dataToSendRemedy['bmc_id']},".$ou_dn;

        $ldaprecord['cn'] = $dataToSendRemedy['bmc_id'];
        $ldaprecord['displayName'] = $dataToSendRemedy['firstname']." ".$dataToSendRemedy['lastname'];
        $ldaprecord['name'] = $dataToSendRemedy['firstname']." ".$dataToSendRemedy['lastname'];
        $ldaprecord['givenName'] = $dataToSendRemedy['firstname'];
        $ldaprecord['sn'] = $dataToSendRemedy['lastname'].$dataToSendRemedy['contract_id'];
        $ldaprecord['mail'] = $dataToSendRemedy['email'];
        $ldaprecord['userPassword'] = 'P@ssw0rd';
        $ldaprecord['objectclass'] = array("top","person","organizationalPerson","user");
        $ldaprecord["userPrincipalName"] = $dataToSendRemedy['bmc_id'];;
        $ldaprecord["UserAccountControl"] = "544"; 

        $newPassword = $dataToSendRemedy['passwd'];

        $newPassword = "\"" . $newPassword . "\"";
        $len = strlen($newPassword);
        for ($i = 0; $i < $len; $i++)
                $newPassw .= "{$newPassword{$i}}\000";
        $newPassword = $newPassw;
        // $ldaprecord["unicodepwd"] = $newPassword;
        $user = ldap_add($ldap_conn, $dn, $ldaprecord);
        ldap_close($ldap_conn);

        return $user;
    }

    public function changePassword($customer)
    {
        $ip = $this->ldapIp;
        $username = $this->username;
        $password = $this->password;

        $ldap_conn = ldap_connect('ldaps://' . $ip) or die("Could not connect to LDAP server (IP: $ip, PORT: 389)");  
        ldap_set_option($ldap_conn, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($ldap_conn, LDAP_OPT_REFERRALS, 0);
        $result = ldap_bind($ldap_conn, "$username", $password ) or die("<br>Error: Couldn't bind to server using supplied credentials!");
        
        $dn = 'cn='.$customer->bmc_id.',cn=Users,dc=cloudselect,dc=com';

        $rj = new RijndaelCore(_RIJNDAEL_KEY_, _RIJNDAEL_IV_);
        $passwordArray = $rj->decrypt($customer->plain_text_password);
        $newPassword = $passwordArray;
        
        $newPassword = "\"" . $newPassword . "\"";
        $len = strlen($newPassword);
        for ($i = 0; $i < $len; $i++)
                $newPassw .= "{$newPassword{$i}}\000";
        $newPassword = $newPassw;
        $userdata["unicodepwd"] = $newPassword;
		//echo $ldap_conn; echo $dn; echo '<pre>'; print_R($userdata); die;
        /*if (ldap_mod_replace($ldap_conn,$dn,$userdata) === false){
            $error = ldap_error($ldap_conn);
            $errno = ldap_errno($ldap_conn);
            $message[] = "E201 - Your password cannot be change, please contact the administrator.";
            $message[] = "$errno - $error";
            //var_dump($userdata);
            //var_dump($message);exit;
            ldap_close($ldap_conn);
            //exit;
          } else {
            ldap_close($ldap_conn);
            return true;
        }*/
        return true;
    }

    /**
     * checkGroupEx check whether user exists in AD
     * 
     * @return boolean
     */
    protected function checkGroupEx($groupdn)
    {
        $this->ldap = ldap_connect($this->ldapIp)
          or die("Couldn't connect to AD!");
  
        ldap_set_option($this->ldap, LDAP_OPT_PROTOCOL_VERSION, 3);

        if (ldap_bind($this->ldap, $this->username, $this->password)) {
            $result = ldap_search($this->ldap,$groupdn, "(cn=*)");
            $data = ldap_get_entries($this->ldap, $result);

            if (!empty($data)) {
                return $data;
            }
        }

        // all done? clean up
        ldap_close($ldapconn);

        return false;
    }
}
