<?php
/*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

function fd($var)
{
    return (Tools::fd($var));
}

function p($var)
{
    return (Tools::p($var));
}

function d($var)
{
    Tools::d($var);
}

function ppp($var)
{
    return (Tools::p($var));
}

function ddd($var)
{
    Tools::d($var);
}

function epr($var, $message_type = null, $destination = null, $extra_headers = null)
{
    return Tools::error_log($var, $message_type, $destination, $extra_headers);
}

/**
 * Sanitize data which will be injected into SQL query
 *
 * @param string $string SQL data which will be injected into SQL query
 * @param bool $htmlOK Does data contain HTML code ? (optional)
 * @return string Sanitized data
 */
function pSQL($string, $htmlOK = false)
{
    return Db::getInstance()->escape($string, $htmlOK);
}

function bqSQL($string)
{
    return str_replace('`', '\`', pSQL($string));
}

function displayFatalError()
{
    $error = null;
    if (function_exists('error_get_last')) {
        $error = error_get_last();
    }
    if ($error !== null && in_array($error['type'], array(E_ERROR, E_PARSE, E_COMPILE_ERROR ))) {
        echo '[PrestaShop] Fatal error in module file :'.$error['file'].':<br />'.$error['message'];
    }
}

/**
 * @deprecated
 */
function nl2br2($string)
{
    Tools::displayAsDeprecated();
    return Tools::nl2br($string);
}

function debug($data)
{
    echo '<pre>';print_r($data);echo '</pre>';
}

function sendData($path, $data)
{
	$ch = curl_init($path);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	curl_setopt ($ch, CURLOPT_POSTFIELDS, $data);
	curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_TIMEOUT, 10); //timeout in seconds

	$response = curl_exec ($ch);
	return  json_decode($response, true);
}

/* middlewear work shakir */
function sendDataToMiddleware($path, $data, $debug=false)
{
	$data['portal_identifier'] = SHOP_ID;
	$debug=false;
    $mwUrl = _MW_BASE_PATH. '/' . $path;
	try {
		$data_string = json_encode($data);
		
		$ch = curl_init($mwUrl);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt ($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10); //timeout in seconds
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		    'Content-Type: application/json',
			'Content-Length: ' . strlen($data_string)
		));

		$response = false;
		$response = curl_exec ($ch);
		$info = curl_getinfo($ch);
		$log = array(
			'path' => $path,
			'time' => date("F j, Y, g:i a"),
			'request' => $data,
			'responseInfo' => $info,
			'response' => json_decode($response, true),
		);
		if ($debug) {
			echo '<pre>'; print_r($log); echo '</pre>';
			echo '<pre>'; echo json_encode($log); echo '</pre>';
		}
		$logJson = json_encode($log);
		//CloudSelectLogger::addLog($logJson, 1, null, 'API', null, true, null);

		$return = false;

		if (!empty($response)) {
			$response = json_decode($response,true);
			if ($response['meta']['exit-code'] === 0) {
				$return = true;
			}
		}

		// Put the request and response in a table so that we can re-run if need be
		logExternalApiRequest($path, $mwUrl, $data_string, json_encode($response), json_encode($info), $return);

		return $return;
	}
		catch (Exception $ex) {
		return false;
	   	// Shows a message related to the error
	   	echo 'Other error: <br />' . $ex->getMessage();
	}
}
	
	function logExternalApiRequest($action, $url, $request, $response, $responseInfo, $success)
	{
		$db = Db::getInstance();
		// Prepare the array of fields/values to be saved
		$data = array(
			'action' => $action,
			'url' => $url,
			'request' => $request,
			'response' => $response,
			'response_info' => $responseInfo,
			'success' => empty($success) ? 0 : 1,
			'is_processing' => 0, // since we are inserting the request, it has already been processed
			'last_tried' => date('Y-m-d H:i:s'),
			'created' => date('Y-m-d H:i:s'),
			'modified' => date('Y-m-d H:i:s'),
		);

		$db->insert('external_api_requests', $data);
	}
/* middlewear work */
