{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<!-- Block provider module -->
<div id="manufacturers_block_left" class="block blockprovider">
	<h4 class="title_block">{if $display_link_provider}<a href="{$link->getPageLink('provider')|escape:'html'}" title="{l s='Provide
	s' mod='blockprovider'}">{/if}{l s='Providers' mod='blockprovider'}{if $display_link_provider}</a>{/if}</h4>
	<div class="block_content">
{if $providers}
	{if $text_list}
	<ul class="bullet">
	{foreach from=$provider item=provider name=provider_list}
		{if $smarty.foreach.provider_list.iteration <= $text_list_nb}
		<li class="{if $smarty.foreach.provider_list.last}last_item{elseif $smarty.foreach.provider_list.first}first_item{else}item{/if}"><a href="{$link->getproviderLink($provider.id_provider, $provider.link_rewrite)|escape:'html'}" title="{l s='More about %s' sprintf=[$provider.name] mod='blockprovider'}">{$provider.name|escape:'html':'UTF-8'}</a></li>
		{/if}
	{/foreach}
	</ul>
	{/if}
	{if $form_list}
		<form action="{$smarty.server.SCRIPT_NAME|escape:'html':'UTF-8'}" method="get">
			<p>
				<select id="provider_list" onchange="autoUrl('provider_list', '');">
					<option value="0">{l s='All providers' mod='blockprovider'}</option>
				{foreach from=$providers item=provider}
					<option value="{$link->getmanufacturerLink($provider.id_provider, $provider.link_rewrite)|escape:'html'}">{$provider.name|escape:'html':'UTF-8'}</option>
				{/foreach}
				</select>
			</p>
		</form>
	{/if}
{else}
	<p>{l s='No provider' mod='blockprovider'}</p>
{/if}
	</div>
</div>
<!-- /Block provider module -->
