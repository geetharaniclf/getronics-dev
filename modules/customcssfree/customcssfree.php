<?php
/**
 * Add pagewise custom CSS style to your webstore.
 *
 * Custom CSS Free by Vipul Hadiya(http://www.vipulhadiya.com) is licensed under a
 * Creative Creative Commons Attribution-NoDerivatives 4.0 International License.
 * Based on a work at http://www.vipulhadiya.com.
 * Permissions beyond the scope of this license may be available at http://www.vipulhadiya.com.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nd/4.0/.
 *
 * @author    Vipul Hadiya <vipulhadiya@ymail.com>
 * @copyright 2016 Vipul Hadiya
 * @license   http://creativecommons.org/licenses/by-nd/4.0/
 */

if (!defined('_PS_VERSION_'))
	exit;
class CustomcssFree extends Module
{
	public function __construct()
	{
		$this->name = 'customcssfree';
		$this->tab = 'front_office_features';
		$this->version = '1.0.0';
		$this->author = 'Vipul Hadiya';
		$this->bootstrap = true;
		$this->module_key = '5b36c38abab17640a3d627088fb08f11';
		parent::__construct();
		$this->displayName = $this->l('Custom CSS free module by Vipul Hadiya');
		$this->description = $this->l('Add pagewise (product, category, cms, home, contact, manufacturer, supplier) custom CSS to your Prestashop site.');
		$this->confirmUninstall = $this->l('Are you sure about removing these details?');
	}
	public function install()
	{
		include_once ($this->local_path.'sql/install.php');
		if (!parent::install()
		|| !$this->registerHook('backOfficeHeader')
		|| !$this->registerHook('displayHeader'))
			return false;
		return true;
	}
	public function uninstall()
	{
		include_once ($this->local_path.'sql/uninstall.php');
		if (!$this->unregisterHook('backOfficeHeader') || !$this->unregisterHook('displayHeader') || !parent::uninstall())
			return false;
		return true;
	}
	public function hookBackOfficeHeader()
	{
		if (!Tools::getIsset('configure') || Tools::getValue('configure') != $this->name)
			return;
		$this->context->controller->addCSS(array(
			$this->_path.'/views/css/jquery-ui.min',
			$this->_path.'/views/css/customcssfree.bo.css',
		));
		$this->context->controller->addJquery();
		$this->context->controller->addJS(array(
			$this->_path.'/views/js/ace/ace.js',
			$this->_path.'/views/js/customcssfree.bo.js',
		));
	}
	public function hookDisplayHeader()
	{
		$this->context->smarty->assign(array(
			'customcss' => $this->createTemplateForFront()
		));
		return $this->display(__FILE__, 'views/templates/hook/customcssfree.tpl');
	}
	public function getContent()
	{
		$this->context->smarty->assign(array(
			'indexcss' => $this->getCssByControllerName('index'),
			'productcss' => $this->getCssByControllerName('product'),
			'categorycss' => $this->getCssByControllerName('category'),
			'cmscss' => $this->getCssByControllerName('cms'),
			'manufacturercss' => $this->getCssByControllerName('manufacturer'),
			'suppliercss' => $this->getCssByControllerName('supplier'),
			'contactcss' => $this->getCssByControllerName('contact'),
			'generalcss' => $this->getCssByControllerName('general'),
		));
		return $this->display(__FILE__, 'views/templates/admin/configure.tpl');
	}
	public function getCssByControllerName($controller)
	{
		return Db::getInstance()->getValue('SELECT `page_css` FROM `'._DB_PREFIX_.'freecss_page` WHERE `page_name` = \''
		.pSQL(Tools::strtolower($controller)).'\'');
	}
	public function saveCss($data)
	{
		Db::getInstance()->execute('DELETE FROM `'._DB_PREFIX_.'freecss_page`');
		$res = true;
		foreach ($data as $page => $css)
			$res &= Db::getInstance()->execute('INSERT INTO `'._DB_PREFIX_.'freecss_page`(`page_name`, `page_css`, `last_updated`) VALUES(\''
			.pSQL($page).'\', \''.pSQL($css).'\', \''.pSQL(date('Y-m-d H:i:s')).'\')');
		if ($res)
			die($this->l('CSS updated successfully.'));
		die($this->l('Error occured while saving custom CSS'));
	}
	public function createTemplateForFront()
	{
		$pages = array('index', 'product', 'category', 'cms', 'manufacturer', 'supplier', 'contact');
		$general = Db::getInstance()->getValue('SELECT `page_css` FROM `'._DB_PREFIX_.'freecss_page` WHERE `page_name` = \'general\'');
		if (in_array(Tools::getValue('controller'), $pages))
			$general .= "\n\r".Db::getInstance()->getValue('SELECT `page_css` FROM `'._DB_PREFIX_.'freecss_page` WHERE `page_name` = \''
			.pSQL(Tools::getValue('controller')).'\'');
		return $general;
	}
}