<?php
/**
 * Add pagewise custom CSS style to your webstore.
 *
 * Custom CSS Free by Vipul Hadiya(http://www.vipulhadiya.com) is licensed under a
 * Creative Creative Commons Attribution-NoDerivatives 4.0 International License.
 * Based on a work at http://www.vipulhadiya.com.
 * Permissions beyond the scope of this license may be available at http://www.vipulhadiya.com.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nd/4.0/.
 *
 * @author    Vipul Hadiya <vipulhadiya@ymail.com>
 * @copyright 2016 Vipul Hadiya
 * @license   http://creativecommons.org/licenses/by-nd/4.0/
 */

$sqls = array();
$sqls[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'freecss_page`(
    `id_page` INT(10) unsigned NOT NULL AUTO_INCREMENT,
	`page_name` VARCHAR(256) DEFAULT NULL,
	`page_css` TEXT DEFAULT NULL,
	`last_updated` DATETIME NULL,
	PRIMARY KEY (`id_page`)
) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=UTF8;';
foreach ($sqls as $sql)
	if (!Db::getInstance()->execute($sql))
		return false;