/**
 * Integrate the whole Wordpress blogging site to your Prestashop online store.
 *
 * Prestashop Wordpress Bridge by Vipul Hadiya(http://www.vipulhadiya.com) is licensed under a
 * Creative Creative Commons Attribution-NoDerivatives 4.0 International License.
 * Based on a work at http://www.clavolike.com.
 * Permissions beyond the scope of this license may be available at http://www.vipulhadiya.com.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nd/4.0/.
 *
 * @author    Vipul Hadiya <vip@vipulhadiya.com>
 * @copyright 2015 Vipul Hadiya
 * @license   http://creativecommons.org/licenses/by-nd/4.0/
 */
 ace.define("ace/snippets/lua",["require","exports","module"],function(e,t,n){"use strict";t.snippetText="snippet #!\n	#!/usr/bin/env lua\n	$1\nsnippet local\n	local ${1:x} = ${2:1}\nsnippet fun\n	function ${1:fname}(${2:...})\n		${3:-- body}\n	end\nsnippet for\n	for ${1:i}=${2:1},${3:10} do\n		${4:print(i)}\n	end\nsnippet forp\n	for ${1:i},${2:v} in pairs(${3:table_name}) do\n	   ${4:-- body}\n	end\nsnippet fori\n	for ${1:i},${2:v} in ipairs(${3:table_name}) do\n	   ${4:-- body}\n	end\n",t.scope="lua"})