/**
 * Integrate the whole Wordpress blogging site to your Prestashop online store.
 *
 * Prestashop Wordpress Bridge by Vipul Hadiya(http://www.vipulhadiya.com) is licensed under a
 * Creative Creative Commons Attribution-NoDerivatives 4.0 International License.
 * Based on a work at http://www.clavolike.com.
 * Permissions beyond the scope of this license may be available at http://www.vipulhadiya.com.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nd/4.0/.
 *
 * @author    Vipul Hadiya <vip@vipulhadiya.com>
 * @copyright 2015 Vipul Hadiya
 * @license   http://creativecommons.org/licenses/by-nd/4.0/
 */
 ace.define("ace/snippets/makefile",["require","exports","module"],function(e,t,n){"use strict";t.snippetText="snippet ifeq\n	ifeq (${1:cond0},${2:cond1})\n		${3:code}\n	endif\n",t.scope="makefile"})