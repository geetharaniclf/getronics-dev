/**
 * Add pagewise custom CSS style to your webstore.
 *
 * Custom CSS Free by Vipul Hadiya(http://www.vipulhadiya.com) is licensed under a
 * Creative Creative Commons Attribution-NoDerivatives 4.0 International License.
 * Based on a work at http://www.vipulhadiya.com.
 * Permissions beyond the scope of this license may be available at http://www.vipulhadiya.com.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nd/4.0/.
 *
 * @author    Vipul Hadiya <vipulhadiya@ymail.com>
 * @copyright 2016 Vipul Hadiya
 * @license   http://creativecommons.org/licenses/by-nd/4.0/
 */

$(document).ready(function(){
	var editorgeneral = ace.edit("generalcss");
    editorgeneral.setTheme("ace/theme/twilight");
    editorgeneral.getSession().setMode("ace/mode/css");
    
    var editorhome = ace.edit("homecss");
    editorhome.setTheme("ace/theme/twilight");
    editorhome.getSession().setMode("ace/mode/css");
    
    var editorproduct = ace.edit("productcss");
    editorproduct.setTheme("ace/theme/twilight");
    editorproduct.getSession().setMode("ace/mode/css");
    
    var editorcategory = ace.edit("categorycss");
    editorcategory.setTheme("ace/theme/twilight");
    editorcategory.getSession().setMode("ace/mode/css");
    
    var editorcms = ace.edit("cmscss");
    editorcms.setTheme("ace/theme/twilight");
    editorcms.getSession().setMode("ace/mode/css");
    
    var editorsupplier = ace.edit("suppliercss");
    editorsupplier.setTheme("ace/theme/twilight");
    editorsupplier.getSession().setMode("ace/mode/css");
    
    var editormanufacturer = ace.edit("manufacturercss");
    editormanufacturer.setTheme("ace/theme/twilight");
    editormanufacturer.getSession().setMode("ace/mode/css");
    
    var editorcontact = ace.edit("contactcss");
    editorcontact.setTheme("ace/theme/twilight");
    editorcontact.getSession().setMode("ace/mode/css");
    
    $('#savecustomcss').click(function(){
    	$('i', this).addClass('process-icon-loading').removeClass('process-icon-save');
		$('#cssmsg').html('Saving custom css..').addClass('label-info');
		$.ajax({
			url:'../modules/customcssfree/ajax.php?action=savecss&token=' + new Date().getTime(),
			method:'POST',
			data:{
				css:{
					'general':editorgeneral.getValue(),
					'index':editorhome.getValue(),
					'product':editorproduct.getValue(),
					'category':editorcategory.getValue(),
					'cms':editorcms.getValue(),
					'supplier':editorsupplier.getValue(),
					'contact':editorcontact.getValue(),
					'manufacturer':editormanufacturer.getValue()
				}
			},
			context:$(this),
			success:function(data){
				console.log(data);
				$('i', this).addClass('process-icon-save').removeClass('process-icon-loading');
				$('#cssmsg').html(data).addClass('label-info');
			}
		});	
    });
});