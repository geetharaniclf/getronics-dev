<?php
$dir = opendir('ace/snippets');
$i = 0;
while($file = readdir($dir))
{
	if($file != '.' && $file !='..')
	{
		$f = fopen('ace/snippets/'.$file,'r');
		$context = file_get_contents('ace/snippets/'.$file);
		fclose($f);
		$newcontent = str_replace('/**
 * Integrate the whole Wordpress blogging site to your Prestashop online store.
 *
 * Prestashop Wordpress Bridge by Vipul Hadiya(http://www.vipulhadiya.com) is licensed under a
 * Creative Creative Commons Attribution-NoDerivatives 4.0 International License.
 * Based on a work at http://www.clavolike.com.
 * Permissions beyond the scope of this license may be available at http://www.vipulhadiya.com.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nd/4.0/.
 *
 * @author    Vipul Hadiya <vip@vipulhadiya.com>
 * @copyright 2015 Vipul Hadiya
 * @license   http://creativecommons.org/licenses/by-nd/4.0/
 */
 /**
 * Integrate the whole Wordpress blogging site to your Prestashop online store.
 *
 * Prestashop Wordpress Bridge by Vipul Hadiya(http://www.vipulhadiya.com) is licensed under a
 * Creative Creative Commons Attribution-NoDerivatives 4.0 International License.
 * Based on a work at http://www.clavolike.com.
 * Permissions beyond the scope of this license may be available at http://www.vipulhadiya.com.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nd/4.0/.
 *
 * @author    Vipul Hadiya <vip@vipulhadiya.com>
 * @copyright 2015 Vipul Hadiya
 * @license   http://creativecommons.org/licenses/by-nd/4.0/
 */', '/**
 * Add pagewise custom CSS style to your webstore.
 *
 * Custom CSS Free by Vipul Hadiya(http://www.vipulhadiya.com) is licensed under a
 * Creative Creative Commons Attribution-NoDerivatives 4.0 International License.
 * Based on a work at http://www.vipulhadiya.com.
 * Permissions beyond the scope of this license may be available at http://www.vipulhadiya.com.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nd/4.0/.
 *
 * @author    Vipul Hadiya <vipulhadiya@ymail.com>
 * @copyright 2016 Vipul Hadiya
 * @license   http://creativecommons.org/licenses/by-nd/4.0/
 */', $context);
 $nf = fopen('ace-edit/snippets/'.$file, 'w');
 fwrite($nf, $newcontent);
 fclose($nf);
 $i++;
	}
	
}
echo $i;