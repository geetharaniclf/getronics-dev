{**
 * Add pagewise custom CSS style to your webstore.
 *
 * Custom CSS Free by Vipul Hadiya(http://www.vipulhadiya.com) is licensed under a
 * Creative Creative Commons Attribution-NoDerivatives 4.0 International License.
 * Based on a work at http://www.vipulhadiya.com.
 * Permissions beyond the scope of this license may be available at http://www.vipulhadiya.com.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nd/4.0/.
 *
 * @author    Vipul Hadiya <vipulhadiya@ymail.com>
 * @copyright 2016 Vipul Hadiya
 * @license   http://creativecommons.org/licenses/by-nd/4.0/
 *}
<div class="panel">
	<h3><i class="icon-user"></i>&nbsp;{l s='More Info' mod='customcssfree'}</h3>
	<div class="panel-body">
		<ul>
			<li><a target="_blank" href="https://addons.prestashop.com/en/2_community-developer?contributor=368899">More PrestaShop Modules</a></li>
			<li><a target="_blank" href="https://www.youtube.com/user/iHadiya">PrestaShop Video Tutorials</a></li>
			<li><a target="_blank" href="http://www.vipulhadiya.com/category/prestashop-module-development-tutorial/">PrestaShop Module Development Tutorials</a></li>
			<li><a target="_blank" href="https://www.facebook.com/vipultheprofessional/">Contact Me</a></li>
			<li><a target="_blank" href="https://www.facebook.com/vipulhadiya.ceh">Developer Profile</a></li>
		</ul>
	</div>
</div>
<div class="panel">
	<h3><i class="icon-css3"></i>&nbsp;{l s='CSS' mod='customcssfree'}</h3>
	<div class="panel-body">
		<div>
		  <ul class="nav nav-tabs" role="tablist">
		    <li role="presentation" class="active"><a href="#general" aria-controls="general" role="tab" data-toggle="tab">General</a></li>
		    <li role="presentation"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Home</a></li>
		    <li role="presentation"><a href="#product" aria-controls="product" role="tab" data-toggle="tab">Product</a></li>
		    <li role="presentation"><a href="#category" aria-controls="category" role="tab" data-toggle="tab">Category</a></li>
		    <li role="presentation"><a href="#cms" aria-controls="cms" role="tab" data-toggle="tab">CMS</a></li>
		    <li role="presentation"><a href="#supplier" aria-controls="supplier" role="tab" data-toggle="tab">Supplier</a></li>
		    <li role="presentation"><a href="#manufacturer" aria-controls="manufacturer" role="tab" data-toggle="tab">Manufacturer</a></li>
		    <li role="presentation"><a href="#contact" aria-controls="contact" role="tab" data-toggle="tab">Contact</a></li>
		  </ul>
		  <div class="tab-content" style="margin-top:20px">
		    <div role="tabpanel" class="tab-pane active" id="general">
				<div class="panel">
					<h3 class="panel-heading">{l s='General' mod='customcssfree'}</h3>
					{strip}<div class="ace-css" id="generalcss">{$generalcss|escape:'htmlall':'UTF-8'}</div>{/strip}
				</div>
			</div>
			<div role="tabpanel" class="tab-pane" id="home">
				<div class="panel">
					<h3 class="panel-heading">{l s='Home' mod='customcssfree'}</h3>
					{strip}<div class="ace-css" id="homecss">{$indexcss|escape:'htmlall':'UTF-8'}</div>{/strip}
				</div>
			</div>
		    <div role="tabpanel" class="tab-pane" id="product">
				<div class="panel">
					<h3 class="panel-heading">{l s='Product' mod='customcssfree'}</h3>
					{strip}<div class="ace-css" id="productcss">{$productcss|escape:'htmlall':'UTF-8'}</div>{/strip}
				</div>
			</div>
		    <div role="tabpanel" class="tab-pane" id="category">
				<div class="panel">
					<h3 class="panel-heading">{l s='Category' mod='customcssfree'}</h3>
					{strip}<div class="ace-css" id="categorycss">{$categorycss|escape:'htmlall':'UTF-8'}</div>{/strip}
				</div>
			</div>
		    <div role="tabpanel" class="tab-pane" id="cms">
				<div class="panel">
					<h3 class="panel-heading">{l s='CMS' mod='customcssfree'}</h3>
					{strip}<div class="ace-css" id="cmscss">{$cmscss|escape:'htmlall':'UTF-8'}</div>{/strip}
				</div>
			</div>
		    <div role="tabpanel" class="tab-pane" id="supplier">
				<div class="panel">
					<h3 class="panel-heading">{l s='Supplier' mod='customcssfree'}</h3>
					{strip}<div class="ace-css" id="suppliercss">{$suppliercss|escape:'htmlall':'UTF-8'}</div>{/strip}
				</div>
			</div>
		    <div role="tabpanel" class="tab-pane" id="manufacturer">
				<div class="panel">
					<h3 class="panel-heading">{l s='Manufacturer' mod='customcssfree'}</h3>
					{strip}<div class="ace-css" id="manufacturercss">{$manufacturercss|escape:'htmlall':'UTF-8'}</div>{/strip}
				</div>
			</div>
		    <div role="tabpanel" class="tab-pane" id="contact">
				<div class="panel">
					<h3 class="panel-heading">{l s='Contact' mod='customcssfree'}</h3>
					{strip}<div class="ace-css" id="contactcss">{$contactcss|escape:'htmlall':'UTF-8'}</div>{/strip}
				</div>
			</div>
		  </div>
		</div>
	</div>
	<div class="panel-footer">
		<button class="btn btn-default pull-right" id="savecustomcss"><i class="process-icon-save"></i>
			{l s='Save' mod='customcssfree'}
		</button>
		<div id="cssmsg" class="label">&nbsp;</label>
	</div>
</div>