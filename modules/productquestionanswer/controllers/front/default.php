<?php


class ProductQuestionAnswerDefaultModuleFrontController extends ModuleFrontController
{
    public $errors = array();
	//public function __construct()
	//{
	//	parent::__construct();
	//
	//	$this->context = Context::getContext();
	//}
	public function init()
	{

		parent::init();
		if(Tools::getValue('checkTanancy')){
			$this->isValidTanancyName();
		}

		// Send noindex to avoid ghost carts by bots
		header("X-Robots-Tag: noindex, nofollow", true);

		// Get page main parameters

	}
	public function initContent()
	{
		$this->setTemplate(_PS_THEME_DIR_.'errors.tpl');
		if (!$this->ajax)
			parent::initContent();
	}
	public function postProcess()
	{
        $this->validateProductQuestionandAnswer($_POST);
        $this->processForInstance();


		if(Tools::getValue('actionType')=='append_qna' && Tools::getValue('id_product')>0 && Tools::getValue('qty')>0)
		{
			$this->generateQnaList();
		}

		$post = $_POST;
        $dataString = count ($post)." COUNT ";

		foreach ($post as $tKey => $tVal)
			$dataString .= $tKey." => ".$tVal." == ";

		$pId = $post['pId'];

		if(isset($_POST['bundle']) && !empty($_POST['bundle']))
		{
			$pId = substr($pId,1);
			$sqlRow = "SELECT `bmc_id` FROM `"._DB_PREFIX_."product` WHERE id_product IN (".$pId.")";
			$sqlDataRow = Db::getInstance()->ExecuteS($sqlRow);
			$bmc_data = '';
			foreach((array)$sqlDataRow as $val)
			{
				$bmc_data .= ",'".$val['bmc_id']."'";
			}

			$bmc_data = substr($bmc_data,1);
			$sqlRow = "SELECT `catalog_id` FROM `"._DB_PREFIX_."product_qna_temp` WHERE `catalog_id` IN (".$bmc_data.") GROUP BY(`catalog_id`)";
			$sqlDataRow = Db::getInstance()->ExecuteS($sqlRow);

			$bmc_data = array();
			foreach((array)$sqlDataRow as $key)
			{
				$bmc_data[] =$key['catalog_id'];
			}

			//if no selected record has QNA
			if(empty($bmc_data))
			{
				echo Tools::jsonEncode(array('result' 	=> 'success'));
				exit;
			}

			$cook = $_COOKIE[$this->context->cookie->getName()];
			$cartId = $this->context->cart->id;
			if (!$cartId && isset($cook))
			{
				//echo 'error';
				echo Tools::jsonEncode(array('result' 	=> 'No cart is generated.'));
				exit;
			}

			foreach($bmc_data as $key)
			{
				$sqlRow = "SELECT COUNT(*) AS count FROM "._DB_PREFIX_."product_qna_response WHERE catalog_id = '".$key."' AND cart_id = '".$cartId."' AND order_id = ''  ";
				$sqlDataRow = Db::getInstance()->ExecuteS($sqlRow);

				if($sqlDataRow[0]['count'] == 0 )
				{
					echo Tools::jsonEncode(array('result' 	=> 'NO data Found'));
					exit;
				}
			}

			echo Tools::jsonEncode(array('result' 	=> 'success'));
			exit;
		}

		unset ($post['pId']);
		unset ($post['controller']);
		unset ($post['fc']);
		unset ($post['ajax']);
		unset ($post['module']);
		unset ($post['id_lang']);
		unset ($post['qnaAction']);

		unset ($post['p_pro_id']);
		unset ($post['p_bmc_id']);

		$conName = '';
		foreach ($post as $keyCoun => $valCoun){
			$sqlCoun = "SELECT question_id,question_type FROM "._DB_PREFIX_."product_qna_temp WHERE question_id = '".$keyCoun."'  ";   // shipping_dates
			if ($qDataCoun = Db::getInstance()->getRow($sqlCoun)){
				if($qDataCoun['question_type'] == 10) {
					$countryId = $qDataCoun['question_id'];
					$conName = $post[$countryId];
				}
			}
		}
		$state_count = 0;
		$countryObj = new Country();
		if($conName){
			$conId = $countryObj->getIdByName(1, $conName);
			if($conId){
				$objState = new State();
				$state_count = count($objState->getStatesByIdCountry ($conId));
			}

		}

		$cook = $_COOKIE[$this->context->cookie->getName()];
		$cartId = $this->context->cart->id;
		if (!$cartId && isset($cook))
		{
			$checkIf = "IFIF";
			$this->context->cart->add();
			$cartId = (int)$this->context->cart->id;
			$this->context->cookie->id_cart = $cartId;
		} else {
			$checkIf = "ELSEELSE";
		}
		$userData = array ('address1' => '', 'address2' => '', 'city' => '','country' => '','email' => '','fname' => '','lname' => '', 'state' => '', 'pcode' => '', 'phone' => '');
		$result = true;
		$errors = array ();
		$prodID = 0;
		//====
		$passwordField = array();
		//====

		foreach ($post as $key => $val) {
			$dataString22 .= $key." => ".$val." == ";
			$nval = $val;

			$textID = 0;
			for($iCounter = 1; $iCounter <= count($_POST[$key]); $iCounter++){
				$val = $_POST[$key][$textID];

			$question_ID = explode('_', $key);
 			$p_prod_id = $_POST['p_pro_id'][$prodID];
			// print_r($_POST);exit;
			if($post['office365tanancyname_'.$p_prod_id]){
				$office365_tanancyname = trim(addslashes($post['office365tanancyname_'.$p_prod_id]));
				$office365_CountryName = trim(addslashes($post['office365tanancycountry_'.$p_prod_id]));
			
				$this->addinOrdersTableSAASProduct($nval, $p_prod_id,$office365_tanancyname, $office365_CountryName);
			}

			if(in_array(663, $_POST['p_pro_id'])) {

				$data['number_of_employe'] = trim(addslashes($_POST['Number_of_Employees_663'][0]));
				$data['bandwidth'] = trim(addslashes($_POST['Bandwidth_663'][0]));
				$data['acess_card'] = trim(addslashes($_POST['Access_Card_Type_663'][0]));
				$data['device_type'] = trim(addslashes($_POST['Device_Type_663'][0]));
				
				$this->addinOrdersTableDexus($nval, $p_prod_id,json_encode($data));
			}

            // $this->addinOrdersTable($field, $val, $productID);


			$sql = "SELECT * FROM "._DB_PREFIX_."product_qna_temp WHERE question_id = '".$question_ID."' ";    // shipping_dates
			if ($qData = Db::getInstance()->getRow($sql)){ // ExecuteS

				if(($qData['question_type'] == 11 && $qData['required'])  ) { // && !$state_count && !$conName &&
					if( (!trim($val) && $state_count) || !$conName )
					{
						//$errors[] = "Question <strong>".$qData['question']."</strong>  is required.";
						if (!in_array("All fields are mandatory.", $errors))
						$errors[] = "All fields are mandatory.";
					}
				} else if($qData['required'] && !trim($val) && $qData['question_type'] != 11){
					if (!in_array("All fields are mandatory.", $errors))
					$errors[] = "All fields are mandatory.";
				}
				if($qData['required'] && !trim($val) && $qData['question_type']=='AZURE')
				{
					if(!$this->isValidName($val))
					{
						//if (!in_array("Question <strong>".$qData['question']."</strong> is not valid. <br>Container names must start with a letter or number, and can contain only letters, numbers, and the dash (-) character. All letters in a container name must be lowercase", $errors))
						//$errors[] = "Question <strong>".$qData['question']."</strong> is not valid. <br>Container names must start with a letter or number, and can contain only letters, numbers, and the dash (-) character. All letters in a container name must be lowercase";

						//if (!in_array("Question is not valid. <br>Container names must start with a letter or number, and can contain only letters, numbers, and the dash (-) character. All letters in a container name must be lowercase", $errors))
						//$errors[] = "Question is not valid. <br>Container names must start with a letter or number, and can contain only letters, numbers, and the dash (-) character. All letters in a container name must be lowercase";
					}
				}

				//====
				//if(($qData['question_type'] == "PASSWORD") && strlen($val)>0 && ($_POST['p_pro_id'][$prodID]==$_POST['p_pro_id'][$prodID+1]) && ($val != $_POST[key(($post))][$textID-1]) && ($textID>0 && $_POST[key(($post))][0] != $_POST[key(($post))][$textID-1]))
				//if(($qData['question_type'] == "PASSWORD") && strlen($val)>0 && ($_POST['p_pro_id'][$prodID]==$_POST['p_pro_id'][$prodID+1]) && ($val != $_POST[key(($post))][$textID]))
				if(($qData['question_type'] == "PASSWORD") && strlen($val)>0 && ($_POST['p_pro_id'][$prodID]==$_POST['p_pro_id'][$prodID+1]) && ($val != $_POST[key(($post))][$textID]))
				{
					if (!in_array("Confirm password is not matched.", $errors))
					$errors[] = "Confirm password is not matched.";
					//$errors[] = $qData['question_type']. ' && ' .strlen($val).'>0 && '.$_POST['p_pro_id'][$prodID].' == '.$_POST['p_pro_id'][$prodID+1].' && '.$val.' != '.$_POST[key(($post))][$textID] .' check '.$_POST[key(($post))][0].' -- '.key(($post)).'---'.$key.' = '.$textID;
				}
				//$errors[] = $qData['question_type']. ' && ' .strlen($val).'>0 && '.$_POST['p_pro_id'][$prodID].' == '.$_POST['p_pro_id'][$prodID+1].' && '.$val.' != '.$_POST[key(($post))][$textID-1] .' check '.$textID;
				//====

				$userData[$qData['question_type_value']] = $val;

				if (!count($this->errors)){

					//$sqlRow = "SELECT id FROM "._DB_PREFIX_."product_qna_response WHERE question_id = '".$key."' AND cart_id = '".$cartId."' AND order_id = ''  ";   // shipping_dates
					//if ($sqlDataRow = Db::getInstance()->getRow($sqlRow)){
					//	//if(trim($val)){
					//		$sqlQUpdate = " UPDATE  "._DB_PREFIX_."product_qna_response SET
					//			question_srd_instance_id = '".$qData['question_srd_instance_id']."', question_id = '".$key."',
					//			question = '".$qData['question']."', question_order = '".$qData['question_order']."',
					//			answer = '".$val."'
					//			 WHERE question_id = '".$key."' AND cart_id = '".$cartId."' AND order_id = ''  ";
					//		Db::getInstance()->execute($sqlQUpdate);
					//	//}
					//} else {
						//if(trim($val)){


							$sqlQUpdate = " INSERT INTO  "._DB_PREFIX_."product_qna_response SET
								catalog_id = '".$qData['catalog_id']."', customer_id = '".$this->context->customer->id."',
								question_srd_instance_id = '".$qData['question_srd_instance_id']."', question_id = '".$question_ID."',
								question = '".$qData['question']."', question_order = '".$qData['question_order']."',
								answer = '".$val."', answer_format = '".$qData['question_type']."',
								cart_id = '".$cartId."', id_product = '".$_POST['p_pro_id'][$prodID]."'
								";
							Db::getInstance()->execute($sqlQUpdate);



						//}
					//}
				} else {
					$result = false;
				}
			}
			$prodID++;

			////=============
			$textID++;
			}
			////=============
		}

		if(!empty($this->errors))
		{
			$result = false;
		}else{

			$result = true;
		}

		echo Tools::jsonEncode(array(
				'result' 	=> $result,
				'userData' 	=> $userData,
				'echo' 		=> $state_count.'='.$sqlCoun.'=='. $country,
				'errors' 	=> $this->errors
			));


		if (Tools::getValue('process') == 'remove')
			$this->processRemove();
		else if (Tools::getValue('process') == 'add')
			$this->processAdd();
		exit;
	}


	public function _isValidName($var='')
	{
	       $var1 =  preg_match("/--/", $var) ? 0 : 1;
	       if($var1 == 0)
	       {
		       return false;
	       }
	       else
	       {
		       return ( ! preg_match("/^[a-z0-9]+([\w\-*]{0,1})+([a-z0-9]+)$/", $var)) ? false : true;
	       }
	}

	public function isValidName($var)
	{
		if(count($var)>0)
			return ( ! preg_match('/^[^0-9!<>,;?=+()@#"�{}_$%:]*$/u', $var)) ? false : true;
		else
			return false;

	}
	
	
	public function isValidTanancyName($var){
		$result = 0;
		$tanancyName = $_POST['tanancyName'];
		$requestURL = "https://login.windows.net/$tanancyName.onmicrosoft.com/FederationMetadata/2007-06/FederationMetadata.xml";
		$reader = new XMLReader();
		$request = $HTTP_RAW_POST_DATA;
		$reader->XML($request);
		$reader->open($requestURL);
		while ($reader->read()) {
			if ($reader->hasValue) {
			  $result = 1;
			}
		}
		$reader->close();
		echo Tools::jsonEncode(array(
			'result' 	=> $result,
			'message'	=> '<li>This tenancy already exists, please enter a unique tenancy name.</li>'
		));
		exit;
	}

	public function generateQnaList()
	{
		$id_product = Tools::getValue('id_product');
		$sqlRow = "SELECT `bmc_id` FROM `"._DB_PREFIX_."product` WHERE id_product = ".$id_product;
		$sqlDataRow = Db::getInstance()->ExecuteS($sqlRow);
		
		//print_r($sqlDataRow[0]['bmc_id']);

		$question = array();
		//QDGAJ1AEG03G4AMY4QRTAAF4MQACJ2, QDGAJ1AEG03G4AMY4R2HAALF1YAD89, QDGAJ1AEG03G4AMY4RFHAA5RCLACZH, QDGAJ1AEG03G4AMY4RFXAA60URADF6
		$sql_qns = "SELECT * FROM "._DB_PREFIX_."product_qna_temp  WHERE q_status = '0' AND catalog_id = '".$sqlDataRow[0]['bmc_id']."' GROUP BY question_id ORDER By question_order";
		$res_data = Db::getInstance()->ExecuteS($sql_qns);

		 if(count($res_data)) {
			$JK = 0;
			foreach ($res_data as $valData) {
				$optionArray = array();
				switch ($valData['question_type']){
					case 'PASSWORD' : // DONE
						$question[$JK]['qdetail'] = $valData;
						$question[$JK]['password'] = array();
					break;
					case 'CONFIRMPASSWORD' : // DONE
						$question[$JK]['qdetail'] = $valData;
						$question[$JK]['password'] = array();
					break;
					case '0' : // DONE
						$question[$JK]['qdetail'] = $valData;
						$question[$JK]['text'] = array();
					break;
					case '1' : // DONE
						$sql_radio = "SELECT choice_label,choice_value FROM "._DB_PREFIX_."product_qna_temp  WHERE q_status = '0' AND catalog_id = '".$p_bmc_id."' AND question_id = '".$valData['question_id']."' ORDER BY choice_order ";
						$data_radio = Db::getInstance()->ExecuteS($sql_radio);
						foreach ((array)$data_radio as $valRadio)
							$radioArray [$valRadio['choice_value']] =  $valRadio['choice_label'];

						$question[$JK]['qdetail'] = $valData;
						$question[$JK]['radio'] = $radioArray;
					break;
					case '2' : // DONE
						$sql_radio = "SELECT choice_label,choice_value FROM "._DB_PREFIX_."product_qna_temp  WHERE q_status = '0' AND catalog_id = '".$p_bmc_id."' AND question_id = '".$valData['question_id']."' ORDER BY choice_order ";
						$data_radio = Db::getInstance()->ExecuteS($sql_radio);
						foreach ((array)$data_radio as $valRadio)
							$radioArray [$valRadio['choice_value']] =  $valRadio['choice_label'];

						$question[$JK]['qdetail'] = $valData;
						$question[$JK]['checkbox'] = $radioArray;

						//$question['checkbox'] = $valData;
					break;
					case '4' : // DONE
						$sql_option = "SELECT choice_label,choice_value FROM "._DB_PREFIX_."product_qna_temp  WHERE q_status = '0' AND catalog_id = '".$p_bmc_id."' AND question_id = '".$valData['question_id']."' ORDER BY choice_order ";
						$data_option = Db::getInstance()->ExecuteS($sql_option);
						foreach ((array)$data_option as $valOption)
							$optionArray [$valOption['choice_value']] =  $valOption['choice_label'];

						$question[$JK]['qdetail'] = $valData;
						$question[$JK]['select'] = $optionArray;
					break;
					case '7' : // DONE date time
						$question[$JK]['qdetail'] = $valData;
						$question[$JK]['datetime'] = array();
					break;
					case '8' : // DONE date
						$question[$JK]['qdetail'] = $valData;
						$question[$JK]['dateonly'] = array();
					break;
					case '9' : // DONE  time
						$question[$JK]['qdetail'] = $valData;
						$question[$JK]['timeonly'] = array();
					break;

					case '10' : // Country Add new
						//$sql_option = "SELECT choice_label,choice_value FROM "._DB_PREFIX_."product_qna_temp  WHERE q_status = '0' AND catalog_id = '".$p_bmc_id."' AND question_id = '".$valData['question_id']."' ORDER BY choice_order ";
						//$data_option = Db::getInstance()->ExecuteS($sql_option);
						//foreach ((array)$data_option as $valOption)
						//	$optionArray [$valOption['choice_value']] =  $valOption['choice_label'];

						$question[$JK]['qdetail'] = $valData;
						$question[$JK]['conSelect'] = $countries; //$countries_list;
					break;
					case '11' : // State Add new
						//$sql_option = "SELECT choice_label,choice_value FROM "._DB_PREFIX_."product_qna_temp  WHERE q_status = '0' AND catalog_id = '".$p_bmc_id."' AND question_id = '".$valData['question_id']."' ORDER BY choice_order ";
						//$data_option = Db::getInstance()->ExecuteS($sql_option);
						//foreach ((array)$data_option as $valOption)
						//	$optionArray [$valOption['choice_value']] =  $valOption['choice_label'];

						$question[$JK]['qdetail'] 	= $valData;
						$question[$JK]['stateSelect'] 	= $countries; //$countries_list;
					break;
					case '12' : // Time zone
						$zoneList = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('SELECT name FROM '._DB_PREFIX_.'timezone');

						$question[$JK]['qdetail'] 	= $valData;
						$question[$JK]['zoneSelect'] 	= $zoneList; //$countries_list;
					break;
					case 'AZURE' :
						$question[$JK]['qdetail'] = $valData;
						$question[$JK]['text'] = array();
					break;
				}
				$JK++;
			}
		 	die(json_encode($question));
		}

		//die(json_encode($sqlDataRow));
		//echo Tools::getValue('id_product').' = '.Tools::getValue('qty');
		//exit;
	}

	// public function addinOrdersTable($questionans)
 //    {
 //        $context = Context::getContext();
 //        $updateOrderstable = '';
 //        print_r($questionans);exit;
 //        foreach($questionans as $key => $value)
 //        {
 //        	if(is_numeric($key))
 //           {
 //                $updateOrderstable = 'UPDATE '._DB_PREFIX_.'cart_product SET ';
 //                $updateOrderstable .='`instance_name` = "'.pSQL($value['instance_name']).'",`user_name` = "'.pSQL($value['user_name']).'",';
 //                $updateOrderstable .=' `password` = "'.pSQL($value['password']).'",`confirm_password` = "'.pSQL($value['confirm_password']).'"';
 //           		$updateOrderstable .=' where id_cart='.$context->cart->id.' and id_product ='.$key;

 //           		echo $updateOrderstable;exit;
 //                Db::getInstance()->execute($updateOrderstable);
 //            }
 //        }
 //    }


    /**
     * validateProductQuestionandAnswer validate product question
     * @return string error message
     */
    private function validateProductQuestionandAnswer($postData)
    {
    	foreach($postData as $key => $value)
        {
           if(is_numeric($key) || strpos($key, '-'))
            {
               if(empty($value['instance_name']))
                 {
                    $this->errors[]="Instance name can not be empty.";
                    return $this->errors;
                }

               if(!empty($value['instance_name']))
                 {
                    if(!preg_match("/^[a-z0-9]{3,63}+(-[a-z0-9]+)*$/", $value['instance_name']))
                    {
                       $this->errors[]="Instance name is not valid. Only lowercase letter, numbers, - are allowed and minimum character in instance name is 3 to 63.";
                       return $this->errors;
                    }

                    if(strpos($value['instance_name'],'!') !== false || strpos($value['instance_name'],' ') !== false)
                    {
                       $this->errors[]="Instance name is not valid. Exclamation mark(!) and white space not allowed";
                       return $this->errors;
                    }
                }

                if(empty($value['user_name']))
                {
                    $this->errors[]="User name can not be empty.";
                    return $this->errors;
                }

                if(!empty($value['user_name']))
                 {
                    if(strpos($value['user_name'],'!') !== false || strpos($value['user_name'],' ') !== false)
                    {
                       $this->errors[]="User name is not valid. Exclamation mark(!) and white space not allowed.";
                       return $this->errors;
                    }
                }

                if(empty($value['password']))
                {
                    $this->errors[]="Password field can not be empty.";
                    return $this->errors;
                }

                if(empty($value['confirm_password']))
                {
                    $this->errors[]="Confirm password can not be empty.";
                    return $this->errors;
                }

                if(!empty($value['password']))
                {
                    if(!preg_match_all('$\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])(?=\S*[\W])\S*$', $value['password']))
                    {
                        $this->errors[]="Password should be minimum of 8 chars with containing at least one lowercase letter, one uppercase letter, one number & a special character, ! and white space not allowed.";
                        return $this->errors;
                    }

                    if (strpos($value['password'],'!') !== false || strpos($value['password'],' ') !== false ) {
                        $this->errors[]="Password should be minimum of 8 chars with containing at least one lowercase letter, one uppercase letter, one number & a special character,! and white space not allowed.";
                        return $this->errors;
                    }
                }

               if($value['confirm_password'] != $value['password'])
                {
                    $this->errors[]="Password and confirm password does not match";
                    return $this->errors;
                }
            }
        }
    }
	
	   /**
     * processForInstance description
     * 
     * @return [type] [description]
     */
    public function processForInstance()
    {
    	$data = array(
    			'Instance_Name',
    			'User_Name',
    			'Password',
    			'Confirm_Password'
    		);


    	foreach (Tools::getValue('p_pro_id') as $key => $value) {
    		$temp['Instance_Name'] = Tools::getValue('Instance_Name' . '_' .$value);
    		$temp['User_Name']     = Tools::getValue('User_Name' . '_' .$value);
    		$temp['Password']      = Tools::getValue('Password' . '_' .$value);
    		
    		$this->addinOrdersTable($temp, $value);
    		
    	}
    	// echo json_encode(array('success' => 1));exit;
	}

	public function addinOrdersTable($val, $productId){
               $context = Context::getContext();
               $updateOrderstable = 'UPDATE '._DB_PREFIX_.'cart_product SET `instance_name` ="'.$val['Instance_Name'][0].'", `user_name` ="'.$val['User_Name'][0].'", `password` ="'.$val['Password'][0].'", `confirm_password` ="'.$val['Password'][0].'"
           		where id_cart='.$context->cart->id.' and id_product ='.$productId;
            Db::getInstance()->execute($updateOrderstable);
        }
	/**
	 * addinOrdersTableDexus description
	 * @param  [type] $nval      [description]
	 * @param  [type] $productId [description]
	 * @param  [type] $data      [description]
	 * 
	 * @return [type]            [description]
	 */
	public function addinOrdersTableDexus($nval,$productId,$data)
	{
		$context = Context::getContext();
		$data = addslashes($data);
		$updateOrderstable =  'UPDATE '._DB_PREFIX_.'cart_product SET `office365_tanancyname` ="'.$data.'",
			`id_bundle`="'.$context->cart->id.'" where id_cart='.$context->cart->id.' and id_product ='.$productId;

			Db::getInstance()->execute($updateOrderstable);
	}

	public function addinOrdersTableSAASProduct($nval,$productId,$tanancyname, $o365Country=null){
		
		if($nval=='Yes'){
			$newProduct_id= 498;
			$nvalNew = 1;
		}elseif($nval=='No'){
			$newProduct_id= 497;
			$nvalNew = 0;
		}
		$context = Context::getContext();
		//$insertedId = 0;
		
		if($newProduct_id){
			
			$updateOrderstable = 'UPDATE '._DB_PREFIX_.'cart_product SET `office365_tanancyname` ="'.$tanancyname.'", `nval` ="'.$nvalNew.'",
			`id_bundle`="'.$context->cart->id.'", `country_code`="'.$o365Country.'" where id_cart='.$context->cart->id.' and id_product ='.$productId;
			Db::getInstance()->execute($updateOrderstable);
			
			// $insertOrderstable = "INSERT INTO "._DB_PREFIX_."cart_product SET `id_cart`='".$context->cart->id."',`id_address_delivery`='".$context->cart->id_address_delivery."',`id_shop`='".$context->cart->id_shop."',`id_product`='".$newProduct_id."',`id_product_attribute`='0', `quantity`='1', `subscription`='1',`id_bundle`='".$context->cart->id."',`date_add`='".$context->cart->date_add."',`nval`='".$nvalNew."',  `office365_tanancyname`='".$tanancyname."'";
			
			Db::getInstance()->execute($insertOrderstable);
			
			//$insertedId = Db::getInstance()->Insert_ID();
		}
		
	}
}
