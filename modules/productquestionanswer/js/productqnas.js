var pre_address1 	= '';
var pre_address2 	= '';
var pre_city 		= '';
var pre_country 	= '';
var pre_email 		= '';
var pre_fname 		= '';
var pre_lname 		= '';
var pre_state 		= '';
var pre_zipcode		= '';
var pre_mobile		= '';

function loadpreviousdate (data){
	if (data != '') {
		pre_address1 	= data.userData.address1;
		pre_address2 	= data.userData.address2;
		pre_city 		= data.userData.city;
		pre_country 	= data.userData.country;
		pre_email 		= data.userData.email;
		pre_fname 		= data.userData.fname;
		pre_lname 		= data.userData.lname;
		pre_state 		= data.userData.state;
		pre_zipcode		= data.userData.pcode;
		pre_mobile		= data.userData.phone;
	}
}

//==============
function loadAll()
{

}
//==============

$(document).ready(function(){

	$('#open_qna_form').click(function(){
		var tnccheckbox = document.getElementById("tnccgv").checked;
		if (tnccheckbox==false) {
			alert(tncRequired);
			return false;
		}
	});

	//code to auto load for QnA section
	$("button[name='autoload']").click(function(){




		$event = this;

		$($event).parent().parent().parent().find('[title="fname"]').each(function(i){
			$(this).val(fname);
		});
		$($event).parent().parent().parent().find('[title="lname"]').each(function(i){
			$(this).val(lname);
		});
		$($event).parent().parent().parent().find('[title="email"]').each(function(i){
			$(this).val(email);
		});
		$($event).parent().parent().parent().find('[title="address1"]').each(function(i){
			$(this).val(address1);
		});

		$($event).parent().parent().parent().find('[title="address2"]').each(function(i){
			$(this).val(address2);
		});
		$($event).parent().parent().parent().find('[title="pcode"]').each(function(i){
			$(this).val(zipcode);
		});
		$($event).parent().parent().parent().find('[title="phone"]').each(function(i){
			$(this).val(mobile);
		});
		$($event).parent().parent().parent().find('[title="city"]').each(function(i){
			$(this).val(city);
		});

		$($event).parent().parent().parent().find('[title="state"]').each(function(i){
			$(this).find('option[value="'+state+'"]').attr("selected",true);
		});

		$($event).parent().parent().parent().find('[title="country"]').each(function(i){
			$(this).find('option[value="'+country+'"]').attr("selected",true);
		});
		if (state) {
			updateStateQna(state);
		}
	});

	$("button[name='loadprevious']").click(function(){
		$event = this;
		$($event).parent().parent().parent().find('[title="fname"]').each(function(i){
			$(this).val(pre_fname);
		});
		$($event).parent().parent().parent().find('[title="lname"]').each(function(i){
			$(this).val(pre_lname);
		});
		$($event).parent().parent().parent().find('[title="email"]').each(function(i){
			$(this).val(pre_email);
		});
		$($event).parent().parent().parent().find('[title="address1"]').each(function(i){
			$(this).val(pre_address1);
		});
		$($event).parent().parent().parent().find('[title="address2"]').each(function(i){
			$(this).val(pre_address2);
		});
		$($event).parent().parent().parent().find('[title="pcode"]').each(function(i){
			$(this).val(pre_zipcode);
		});
		$($event).parent().parent().parent().find('[title="phone"]').each(function(i){
			$(this).val(pre_mobile);
		});

		$($event).parent().parent().parent().find('[title="state"]').each(function(i){
			$(this).find('option[value="'+pre_state+'"]').attr("selected",true);
		});

		$($event).parent().parent().parent().find('[title="country"]').each(function(i){
			$(this).find('option[value="'+pre_country+'"]').attr("selected",true);
		});
		if (pre_state) {
			updateStateQna(pre_state);
		}

	});

	$('select#id_country').change(function(){
		updateStateQna();
	});

	$('.classDatePicker').datepicker();
	$('.classDateTimePicker').datetimepicker({
		hourGrid: 4,
		minuteGrid: 10,
		timeFormat: 'hh:mm tt'
	});
	$('.classTimePicker').timepicker({
		hourGrid: 4,
		minuteGrid: 10,
		timeFormat: 'hh:mm tt'
	});

	$("button[name='submitQnA']").click(function(e) {

	var pId = $(this).parent().siblings('#p_pro_id').val();

	var fromstr 	= $( "#id_addQNA_from").serialize();

	//$('#question_form_error_'+pId).slideUp('slow');

		$.ajax({
			type: 'POST',
			headers: { "cache-control": "no-cache" },
			url: baseUri + '?rand=' + new Date().getTime(),
			async: true,
			cache: false,
			dataType: 'json',
			data: 'controller=default'
				+ '&fc=module&ajax=true'
				+ '&module=productquestionanswer&id_lang=1&qnaAction=add&pId='+pId+'&'+fromstr,
			success: function(data){
				if (data.result)
				{
					//alert('qna');
					//window.open($('#HOOK_PAYMENT a ').attr('href'),'_self');
					$.fancybox.close();
					
				} else {

					$('#question_form_error ul').html('');
					$.each(data.errors, function(index, value) {
						$('#question_form_error ul').append('<li>'+value+'</li>');
					});
					$('#question_form_error').slideDown('slow');
				}

			}

		});

	});

});


function updateStateQna(suffix)
{
	var conId = $('#id_country').val();
	var selName = $('#id_state').attr('name');

	// $.ajax({
	// 	type: 'POST',
	// 	headers: { "cache-control": "no-cache" },
	// 	url: 'ajax-call.php?rand=' + new Date().getTime(),
	// 	async: true,
	// 	cache: false,
	// 	//dataType: 'json',
	// 	data: 'action=getstate&conId='+conId+'&selName='+selName+'&selected='+suffix,
	// 	success: function(data){
	// 		//alert(data);
	// 		$('#id_state_div').html(data);
	// 	},
	// 	error: function(XMLHttpRequest, textStatus, errorThrown) {
	// 		alert("TECHNICAL ERROR: \n\nDetails:\nError thrown: " + XMLHttpRequest + "\n" + 'Text status: ' + textStatus);
	// 	}
	// });

}

//function validatePassword(id)
//{
//	alert(id);
//}

function productcommentRefreshPage() {
    window.location.reload();
}