<?php
if (!defined('_PS_VERSION_'))
	exit;

class Productquestionanswer extends Module
{
	private $_html = '';
	private $_baseUrl;

	public function __construct()
	{

		$this->name = 'productquestionanswer';
		$this->tab = 'front_office_features';
		$this->version = '1.0.0';
		$this->author = 'Pushpendra';
		$this->need_instance = 0;

		parent::__construct();

		$this->displayName = $this->l('Product Question Answer');
		$this->description = $this->l('Product Question Answer display on product details page.');

	}

	public function install()
	{

		if (!parent::install()
			|| !$this->registerHook('header')
			|| !$this->registerHook('newOrder')
			|| !$this->registerHook('displayProductQnA')
		    )
			return false;
		return true;
	}

	public function uninstall()
	{
		if (!parent::uninstall() || $this->unregisterHook('header')
					 || $this->unregisterHook('newOrder')
					 || $this->unregisterHook('displayProductQnA')
			)
			return false;
		return true;
	}


	public function hookDisplayProductQnA($params)
	{ 
		
		$countries = Country::getCountries($this->context->language->id, true);
		
		//js_dir
		//$id_product = Tools::getValue('id_product');
		//$prodct_details = new Product ($id_product);
		//$p_bmc_id = $prodct_details->bmc_id;
		$count = 0;
		
		$profile = array();
		//code to get customers data
		if(isset($this->context->cookie->id_customer) && !empty($this->context->cookie->id_customer))
		{
			//$sql = "SELECT * FROM "._DB_PREFIX_."customer WHERE id_customer = '".$this->context->cookie->id_customer."' ";
			$sqlcust = "SELECT cust.*,addr.* FROM "._DB_PREFIX_."customer cust INNER JOIN "._DB_PREFIX_."address addr ON addr.id_customer = cust.id_customer WHERE cust.id_customer = '".$this->context->cookie->id_customer."' ";
			$res_sql = Db::getInstance()->ExecuteS($sqlcust);
				
			if(count($res_sql))
			{
				foreach($res_sql as $val)
				{
					$countryName = Country::getNameById($this->context->language->id,$val['id_country']);
					
					$stateName = State::getNameById($val['id_state']);

					$profile['fname'] = $val['firstname'];
					$profile['lname'] = $val['lastname'];
					$profile['email'] = $val['email'];
					$profile['address1'] = $val['address1'];
					$profile['address2'] = $val['address2'];
					$profile['city'] = $val['city'];
					$profile['country'] = $countryName;
					$profile['state'] = $stateName;
					$profile['zipcode'] = $val['postcode'];
					$profile['phone_mobile'] = $val['phone_mobile'];
				}
			}
			
			//$sql = "SELECT * FROM "._DB_PREFIX_."company WHERE id_company = '".$val["company_id"]."' ";
			//$res_sql = Db::getInstance()->ExecuteS($sql);
			//if(count($res_sql))
			//{
			//	foreach($res_sql as $val)
			//	{
			//		$profile['address1'] = $val['address1'];
			//		$profile['address2'] = $val['address2'];
			//		$profile['city'] = $val['city'];
			//		$profile['country'] = $val['country'];
			//		$profile['state'] = $val['state'];
			//		$profile['zipcode'] = $val['zipcode'];
			//	}
			//}
		}
	
		$cartId = (int)$this->context->cart->id;
		
		$is_bundle = 0;
		$product = '';
		if(isset($cartId) && !empty($cartId))
		{
			$customerCompany = Customer::getCustomerDetailById($this->context->customer->id);
			$customerOrders = Order::getCustomerOrders($this->context->customer->id);
				
			$existOffice365 = array();
			//$companyOrders = Order::getCustomerOrdersByCompanyId($customerCompany['company_id']);

			$existOffice365 = array();
			/*if($companyOrders['total_orders'] > 0){
				$existOffice365[]=1;
			}*/
			$existOffice365[]=1;
			foreach($customerOrders as $key=>$val){
				$orderDetail = OrderDetail::getList($val['id_order']);
				
				foreach($orderDetail as $Okey=>$Pval){
					
					if($Pval['office365_order']=='Y' && ($customerCompany['company_id']==$Pval['id_company'])){
						$existOffice365[] = 1;
					}

					if ($valData['product_name'] == 'DEXUS OFFICE CATALOG') {
						$existBundle663 = 1;
						$valData['question_type'] = 'DEXUSOFFICE';
					}
				}
				
			}
			
			$sql = "SELECT * FROM "._DB_PREFIX_."cart_product WHERE id_cart = '".$cartId."' ";
			$res_sql = Db::getInstance()->ExecuteS($sql);
				
			if(count($res_sql))
			{
				foreach($res_sql as $val)
				{
					$prodct_details = new Product ($val['id_product']);
						
					//echo 'shakir';//$val['id_product'].'<br>';
					//print_r($prodct_details);
					//print_r($prodct_details->operating_system);
					//echo '</pre>'; 
					if(empty($existOffice365)){ //die('ddd');  // Comment this block to get questions
					if(($prodct_details->operating_system == 'OFFICE365' && count($existOffice365)==0) || ((strtolower($prodct_details->xero_account) == 'iaas') || (strtolower($prodct_details->xero_account) == 'saas' && $prodct_details->operating_system == 'OFFICE365')))
					{
						$product_id[] = $val['id_product'];
						$product[] = $prodct_details->bmc_id.'#'.$prodct_details->operating_system;
						$product_name[] = $prodct_details->name[1];
						$product_OS[] = $prodct_details->operating_system;
	
						//== Updated on 15 Aug 2014 By Ankush
						$link_rewrite[] = $prodct_details->link_rewrite;
						$id_image[] = $prodct_details->getWsImages();
						$productQTY[] = $val['quantity'];
					}
				    } else {
					if(($prodct_details->operating_system == 'OFFICE365' && count($existOffice365)==0) || ((strtolower($prodct_details->xero_account) == 'iaas') || (strtolower($prodct_details->xero_account) == 'saas' && $prodct_details->operating_system == 'OFFICE365')))
					{ 
						$product_id[] = $val['id_product'];
						$product[] = $prodct_details->bmc_id.'#'.$prodct_details->operating_system;
						$product_name[] = $prodct_details->name[1];
						$product_OS[] = $prodct_details->operating_system;
	
						//== Updated on 15 Aug 2014 By Ankush
						$link_rewrite[] = $prodct_details->link_rewrite;
						$id_image[] = $prodct_details->getWsImages();
						$productQTY[] = $val['quantity'];
					}	
					}
				}
			}
		}
		
		foreach($product as $p_bmc_id)
		{
			$question = array();
			//QDGAJ1AEG03G4AMY4QRTAAF4MQACJ2, QDGAJ1AEG03G4AMY4R2HAALF1YAD89, QDGAJ1AEG03G4AMY4RFHAA5RCLACZH, QDGAJ1AEG03G4AMY4RFXAA60URADF6
			// $sql_qns = "SELECT * FROM "._DB_PREFIX_."product_qna_temp  WHERE q_status = '0' AND catalog_id = '".$p_bmc_id."' GROUP BY question_id ORDER By question_order";
			$pbmc_idAry = explode('#',$p_bmc_id);
			
			$pbmc_id = $pbmc_idAry[0]; 
			if($pbmc_idAry[1]!='OFFICE365'){
				$query = new DbQuery();
				//$query->select('*')->from('product_qna_temp')->where("`q_status` = '0' AND `catalog_id` = 'GLOBAL'");
				$query = "SELECT * FROM `csb_product_qna_temp` WHERE (`q_status` = '0' AND `catalog_id` = 'GLOBAL') ORDER BY id DESC";
				//echo 'in global';
			}
			else{
				$query = new DbQuery();
				//$query->select('*')->from('product_qna_temp')->where("`q_status` = '0' AND `catalog_id` = 'OFFICE365'");
				$query = "SELECT * FROM `csb_product_qna_temp` WHERE (`q_status` = '0' AND `catalog_id` = 'OFFICE365') ORDER BY id DESC";
			}
			
			$res_data = Db::getInstance()->executeS($query);
			
			 if(count($res_data)) {
				$JK = 0;
				foreach ($res_data as $valData) {
					$optionArray = array();
					switch ($valData['question_type']){
						case 'PASSWORD' : // DONE
							$question[$JK]['qdetail'] = $valData;
							$question[$JK]['qdetail']['question_id'] = $pbmc_id;
							$question[$JK]['qdetail']['index_name'] = 'pwd';
							$question[$JK]['password'] = array();
						break;
						case 'CONFIRMPASSWORD' : // DONE
							$question[$JK]['qdetail'] = $valData;
							$question[$JK]['qdetail']['question_id'] = $pbmc_id;
							$question[$JK]['qdetail']['index_name'] = 'confpwd';
							$question[$JK]['confirmpassword'] = array();
						break;
						case '0' : // DONE
							$question[$JK]['qdetail'] = $valData;
							$question[$JK]['qdetail']['question_id'] = $pbmc_id;
							$question[$JK]['qdetail']['instance_name'] = 'pwd';
							$question[$JK]['text'] = array();
						break;
						case '1' : // DONE
							$query = new DbQuery();
							$query->select('choice_label,choice_value')->from('product_qna_temp')->where('`q_status` = "0" AND `catalog_id` = "'.$p_bmc_id.'" AND `question_id` = "'.$valData['question_id'].'" ORDER BY choice_order');
							// $sql_radio = "SELECT choice_label,choice_value FROM "._DB_PREFIX_."product_qna_temp  WHERE q_status = '0' AND catalog_id = '".$p_bmc_id."' AND question_id = '".$valData['question_id']."' ORDER BY choice_order ";
							$res_data = Db::getInstance()->executeS($query);
							foreach ((array)$data_radio as $valRadio)
								$radioArray [$valRadio['choice_value']] =  $valRadio['choice_label'];

							$question[$JK]['qdetail'] = $valData;
							$question[$JK]['radio'] = $radioArray;
						break;
						case '2' : // DONE
							// $sql_radio = "SELECT choice_label,choice_value FROM "._DB_PREFIX_."product_qna_temp  WHERE q_status = '0' AND catalog_id = '".$p_bmc_id."' AND question_id = '".$valData['question_id']."' ORDER BY choice_order ";
							$query = new DbQuery();
							$query->select('choice_label,choice_value')->from('product_qna_temp')->where('`q_status` = "0" AND `catalog_id` = "'.$p_bmc_id.'" AND `question_id` = "'.$valData['question_id'].'" ORDER BY choice_order');
							$res_data = Db::getInstance()->executeS($query);
							foreach ((array)$data_radio as $valRadio)
								$radioArray [$valRadio['choice_value']] =  $valRadio['choice_label'];

							$question[$JK]['qdetail'] = $valData;
							$question[$JK]['checkbox'] = $radioArray;

							//$question['checkbox'] = $valData;
						break;
						case '4' : // DONE
							// $sql_option = "SELECT choice_label,choice_value FROM "._DB_PREFIX_."product_qna_temp  WHERE q_status = '0' AND catalog_id = '".$p_bmc_id."' AND question_id = '".$valData['question_id']."' ORDER BY choice_order ";
							$query = new DbQuery();
							$query->select('choice_label,choice_value')->from('product_qna_temp')->where('`q_status` = "0" AND `catalog_id` = "'.$p_bmc_id.'" AND `question_id` = "'.$valData['question_id'].'" ORDER BY choice_order');
							$res_data = Db::getInstance()->executeS($query);
							foreach ((array)$data_option as $valOption)
								$optionArray [$valOption['choice_value']] =  $valOption['choice_label'];

							$question[$JK]['qdetail'] = $valData;
							$question[$JK]['select'] = $optionArray;
						break;
						case '7' : // DONE date time
							$question[$JK]['qdetail'] = $valData;
							$question[$JK]['datetime'] = array();
						break;
						case '8' : // DONE date
							$question[$JK]['qdetail'] = $valData;
							$question[$JK]['dateonly'] = array();
						break;
						case '9' : // DONE  time
							$question[$JK]['qdetail'] = $valData;
							$question[$JK]['timeonly'] = array();
						break;

						case '10' : // Country Add new
							//$sql_option = "SELECT choice_label,choice_value FROM "._DB_PREFIX_."product_qna_temp  WHERE q_status = '0' AND catalog_id = '".$p_bmc_id."' AND question_id = '".$valData['question_id']."' ORDER BY choice_order ";
							//$data_option = Db::getInstance()->ExecuteS($sql_option);
							//foreach ((array)$data_option as $valOption)
							//	$optionArray [$valOption['choice_value']] =  $valOption['choice_label'];

							$question[$JK]['qdetail'] = $valData;
							$question[$JK]['conSelect'] = $countries; //$countries_list;
						break;
						case '11' : // State Add new
							//$sql_option = "SELECT choice_label,choice_value FROM "._DB_PREFIX_."product_qna_temp  WHERE q_status = '0' AND catalog_id = '".$p_bmc_id."' AND question_id = '".$valData['question_id']."' ORDER BY choice_order ";
							//$data_option = Db::getInstance()->ExecuteS($sql_option);
							//foreach ((array)$data_option as $valOption)
							//	$optionArray [$valOption['choice_value']] =  $valOption['choice_label'];

							$question[$JK]['qdetail'] 	= $valData;
							$question[$JK]['stateSelect'] 	= $countries; //$countries_list;
						break;
						case '12' : // Time zone
							$zoneList = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('SELECT name FROM '._DB_PREFIX_.'timezone');

							$question[$JK]['qdetail'] 	= $valData;
							$question[$JK]['zoneSelect'] 	= $zoneList; //$countries_list;
						break;
						case 'AZURE' :
							$question[$JK]['qdetail'] = $valData;
							$question[$JK]['qdetail']['question_id'] = $pbmc_id;
							$question[$JK]['qdetail']['index_name'] = 'username';
							$question[$JK]['text'] = array();
						break;
						case '13' :
							$question[$JK]['qdetail'] = $valData;
							$question[$JK]['qdetail']['question_id'] = $pbmc_id;
							$question[$JK]['tenancyname'] = 'tenancyname';
							$question[$JK]['o365Select'] = array();
						break;
						case '14' :
							$question[$JK]['qdetail'] = $valData;
							$question[$JK]['qdetail']['question_id'] = $pbmc_id;
							$question[$JK]['tenancyname'] = 'tenancyname';
							$question[$JK]['o365Country'] = array();
						break;
						case 'OFFICE365' :
							$question[$JK]['qdetail'] = $valData;
							$question[$JK]['qdetail']['question_id'] = $pbmc_id;
							//$question[$JK]['tenancyname'] = 'tenancyname';
							$question[$JK]['office365'] = array();
						break;
					}
					$JK++;
				}
			$final_qna[$count]->p_bmc_id = $pbmc_id;
			$final_qna[$count]->p_id = $product_id[$count];

			//== Update On 15 Aug 2014 By Ankush
			$final_qna[$count]->link_rewrite = $link_rewrite[$count];
			$final_qna[$count]->id_image = $id_image[$count];
			$final_qna[$count]->productQTY = $productQTY[$count];


			$final_qna[$count]->product_OS = $product_OS[$count];
			$final_qna[$count]->p_name = $product_name[$count];
			$final_qna[$count++]->questions = $question;
				
		 
			}

		}

		//echo 'ProdID: ' . $id_product . '<br>';
//		$prod_detail = new Product($id_product);
//		$prod_detail->image_cover = $prod_detail->getCover((int)$id_product);
//
//                if(!isset($prod_detail->image_cover))
//                    $product->image_cover['id_image'] = 'en-default';

		//$prod_detail = $prod_detail->getgetProducts();
		$this->context->smarty->assign(array(
				'temp'		=> count($final_qna),
				'countries'	=> $countries,
				'qnaCount'	=> count($res_data),
				'questions' 	=> $question,
				'p_id' 		=> $id_product,
				'p_bmc_id' 	=> $p_bmc_id,
				'final_qna'	=> $final_qna,
				'is_bundle'	=> $is_bundle,
				'fname'	=> $profile['fname'],
				'lname'	=> $profile['lname'],
				'email'	=> $profile['email'],
				'city'	=> $profile['city'],
				'country'	=> $profile['country'],
				'address1'	=> $profile['address1'],
				'address2'	=> $profile['address2'],
				'state'	=> $profile['state'],
				'zipcode'	=> $profile['zipcode'],
				'mobile'	=> $profile['phone_mobile'],
				'pro_qna_controller_url' => $this->context->link->getModuleLink('productquestionanswer'),
				'pro_qna_url_rewriting_activated' => Configuration::get('PS_REWRITING_SETTINGS', 0),
				'secure_keyQna' => Tools::getToken(false), //$this->secure_key
				'query' => $sqlcust,
				'existOffice365'=>count($existOffice365),
				'existBundle663' => $existBundle663
			));

		return ($this->display(__FILE__, '/questionlist.tpl'));
	}

	public function hookHeader($params)
	{
		//including CSS file
		$this->context->controller->addCSS(_MODULE_DIR_.'productquestionanswer/productqna.css', 'all');
		$this->context->controller->addCSS(_PS_JS_DIR_.'jquery/ui/themes/base/jquery.ui.all.css', 'all');
		$this->context->controller->addCSS(_PS_JS_DIR_.'jquery/ui/themes/base/jquery.ui.datepicker.css', 'all');
		$this->context->controller->addCSS(_PS_JS_DIR_.'jquery/plugins/timepicker/jquery-ui-timepicker-addon.css', 'all');

		//including JS Files
		$this->context->controller->addJS(_MODULE_DIR_.'productquestionanswer/js/productqnas.js');
		$this->context->controller->addJS(_PS_JS_DIR_.'jquery/ui/jquery.ui.core.min.js');
		$this->context->controller->addJS(_PS_JS_DIR_.'jquery/ui/jquery.ui.widget.min.js');
		$this->context->controller->addJS(_PS_JS_DIR_.'jquery/ui/jquery.ui.mouse.min.js');
		$this->context->controller->addJS(_PS_JS_DIR_.'jquery/ui/jquery.ui.slider.min.js');
		$this->context->controller->addJS(_PS_JS_DIR_.'jquery/ui/jquery.ui.datepicker.min.js');
		$this->context->controller->addJS(_PS_JS_DIR_.'jquery/plugins/timepicker/jquery-ui-timepicker-addon.js');
	}

    public function hookNewOrder($params)
	{
		$id_customer 	= (int)$params['customer']->id;
		$id_order 	= (int)$params['order']->id;
		$id_cart 	= (int)$params['order']->id_cart;
		if ($id_cart){
			$sql_product = "SELECT id_order_detail, product_id FROM "._DB_PREFIX_."order_detail  WHERE id_order = '".$id_order."'  ";
			$res_product = Db::getInstance()->ExecuteS($sql_product);
			if(count($res_product)) {
				foreach ($res_product as $valData) {
					$sqlQUpdate = " UPDATE  "._DB_PREFIX_."product_qna_response SET
						customer_id = '".$id_customer."', order_id = '".$id_order."',
						order_detail_id = '".$valData['id_order_detail']."'
						WHERE  cart_id = '".$id_cart."' AND order_detail_id = '".$valData['product_id']."' AND order_id = ''  ";
					Db::getInstance()->execute($sqlQUpdate);
				}
			}
		}
		return true;
	}
}
