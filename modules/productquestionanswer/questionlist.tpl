<script type="text/javascript">
var pro_qna_controller_url = '{$pro_qna_controller_url}';
var confirm_report_message = '{l s='Are you sure you want report this comment?' mod='productquestionanswer' js=1}';
var secure_key = '{$secure_keyQna}';
var pro_qna_url_rewrite = '{$pro_qna_url_rewriting_activated}';
var is_bundle = '{$is_bundle}';
var fname = '{$fname}';
var lname = '{$lname}';
var email = '{$email}';
var city = '{$city}';
var country = '{$country}';
var address1 = '{$address1}';
var address2 = '{$address2}';
var state = '{$state}';
var zipcode = '{$zipcode}';
var mobile = '{$mobile}';

//var productcomment_added = '{l s='Your comment has been added!' mod='productquestionanswer' js=1}';
//var productcomment_added_moderation = '{l s='Your comment has been added and will be available once approved by a moderator' mod='productquestionanswer' js=1}';
//var productcomment_title = '{l s='New comment' mod='productquestionanswer' js=1}';
//var productcomment_ok = '{l s='OK' mod='productquestionanswer' js=1}';
</script>
<script type="text/javascript">
// <![CDATA[
var countries = new Array();
{foreach from=$countries item='country'}
	{if isset($country.states) && $country.contains_states}
		countries[{$country.id_country|intval}] = new Array();
		{foreach from=$country.states item='state' name='states'}
			countries[{$country.id_country|intval}].push({ldelim}'id' : '{$state.id_state}', 'name' : '{$state.name|addslashes}'{rdelim});
		{/foreach}
	{/if}
{/foreach}
//]]>
</script>

    <div id="idTab12">
	{if $final_qna|count > 0}
	<input type="hidden" id="num_of_questions" value="{$final_qna|count}">
	<!-- Fancybox -->
	{$i=1}
	{$j=1}
	{*{foreach from=$final_qna key=finalKey item=bmcdata}*}
	<div>
	    <div class="qna-box copy-page-wrapper qna-response addQNA light-popup" id="addQNA">
	    <div class="copy-page">
	    <div class="copy-page-index">
		<form id="id_addQNA_from" action="javascript:void(0);">
		    <div class="copy-page-index-header">
			<h4>Product Registration<span class="qna-crox" onclick="for_global_qna='close';$.fancybox.close();">X</span></h4>

		    </div>

		    <div class="qna-cart-popup mCustomScrollbar">

		    <div id="question_form_error" class="error" style="display: none; padding: 15px 25px; color: #f3515c;">
			<ul></ul>
		    </div>

		    {foreach from=$final_qna key=finalKey item=bmcdata}
		    <div class="prod-line" id="new_appended_record_{$bmcdata->p_id}">

			<div class="prod-line-row">
			{$i = $i+1}


			<div class="prod-line-content" id="this_is_one_prod_line_{$bmcdata->p_id}">

			    {$foo=1}
			    {*{for $foo=1 to $bmcdata->productQTY}*}
			    <input type="hidden" class="question_id" value="{$bmcdata->p_id}">



			    <div class="ques-list{if $foo%2==0} qna-list-rep{/if}">

				{if isset ($bmcdata->questions)}
				{if $bmcdata->product_OS=='OFFICE365'}
				    <div style="float: right; width: 46%; font-weight: bold;display:none;">Tenancy Country: {$country.name}</div>
				{/if}


				<div class="prod-img-line" id="this_is_prod_line_{$bmcdata->p_id}">
				    {if $foo==1}
				    <img
				    class="img-responsive1"
				    src="{$link->getImageLink($bmcdata->link_rewrite[1], $bmcdata->id_image[0]['id'], 'cart_default')|escape:'html':'UTF-8'}"
				    alt="{$bmcdata->name|escape:'html':'UTF-8'}" />
				    {*<img
				    class="img-responsive"
				    src="{$img_prod_dir}en-default-list_default.jpg"
				    alt="{$bmcdata->name|escape:'html':'UTF-8'}" />*}
				    {/if}
				</div>
				<div class="product-question-listing">
				{if $foo==1}
				<h2>{$bmcdata->p_name}</h2>


				{/if}

				<div style="width: 84%; float: left;" id="product_question_row_detail_{$bmcdata->p_id}">
				{$insItem=0}
				{foreach from=$bmcdata->questions key=typeKey item=questionType}

				    {assign var ="QID" value = $questionType.qdetail.question_id}
				    {assign var ="defaultResponse" value = $questionType.qdetail.default_response}
				    {assign var ="countLoop" value = 0}

				    {$insItem=$insItem+1}

				    <div class="qna-row quesList {if $bmcdata->product_OS=='OFFICE365'}qna-row-o365{/if}">
				    {foreach from=$questionType key=quKey item=question}

					{*<input type="hidden" class="quesList">*}

					{if $quKey == 'qdetail'}
						<div class="qna-left">{$question.question}{if $question.required}<span class="required"><sup>*</sup></span>{/if}
						    {if $question.description}<span class="dec">({$question.description})</span>{/if}
						</div>
					{elseif $quKey == 'password'}
						<div class="qna-right">
						    <input type="password" name="{$questionType.qdetail.question}_{$bmcdata->p_id}[]" id="{$questionType.qdetail.id}1" title="{$questionType.qdetail.question_type_value}" class="form-control">
						</div>
						{$passID = $questionType.qdetail.id}
					{elseif $quKey =='confirmpassword'}
						<div class="qna-right">
						    <input type="password" onblur="validatePassword({$questionType.qdetail.id}1, {$passID}1)" name="{$questionType.qdetail.question}_{$bmcdata->p_id}[]" id="{$questionType.qdetail.id}1" title="{$questionType.qdetail.question_type_value}" class="form-control">
						</div>
					{elseif $quKey=='o365Select'}
						<div class="qna-right">
						    <div class="inline form-inline">
							<div class="styled-before"></div>
							<div class="styled">
							    <select id="o365Select" style="width: 250px;height: 27px" name="{$QID}_{$questionType.qdetail.id}" title="{$questionType.qdetail.question_type_value}">
							    
								<option value="">Select</option>
								<option value="Yes">Yes</option>
								<option value="No">No</option>
								
							</select>
							</div>
						    </div>
						</div>
					{elseif $quKey=='o365Country'}
						<div class="qna-right">
						    <div class="inline form-inline">
							<div class="styled-before"></div>
							<div class="styled">
							    <select id="o365Country" style="width: 250px;height: 27px" name="office365tanancycountry_{$bmcdata->p_id}" title="{$questionType.qdetail.question_type_value}">
							    
								<option value="DE" {if $country == 'Germany'} selected {/if}>
								Germany
								</option>
								<option value="AT" {if $country == 'Austria'}  selected {/if}>	Austria
								</option>
								<option value="CH" {if $country == 'Switzerland'}  selected {/if}>	Switzerland
								</option>
								
							</select>
							</div>
						    </div>
						</div>					
					{elseif $quKey == 'text'}
						<div class="qna-right">
						    <input type="text"{if $insItem==1} onblur="validateInstance({$bmcdata->p_id}, {$questionType.qdetail.id})"{/if} name="{$questionType.qdetail.question}_{$bmcdata->p_id}[]" id="{$questionType.qdetail.id}_1" title="{$questionType.qdetail.question_type_value}" class="form-control{if $insItem==1} instance-name{$bmcdata->p_id}{/if}">
						</div>
					{elseif $quKey == 'office365'}
						<div class="qna-right">
						    <input type="text"{if $insItem==1} onblur="validateInstance({$bmcdata->p_id}, {$questionType.qdetail.id})"{/if} name="office365tanancyname_{$bmcdata->p_id}" id="{$questionType.qdetail.id}_1" title="{$questionType.qdetail.question_type_value}" class="form-control tanancyName" style="display: inline; width: 65% !important">.onmicrosoft.com
						</div>	
					{elseif $quKey == 'select' }
					    <div class="qna-right">
						<div class="inline form-inline">
						    <div class="styled-before"></div>
						    <div class="styled">
							<select name="{$QID}_{$questionType.qdetail.id}[]" title="{$questionType.qdetail.question_type_value}">
							    {if !isset($defaultResponse)}
								<option value="">Select</option>
							    {/if}
							    {foreach from=$questionType.select key=poKey item=polabel}
								<option value="{$poKey}" {if $poKey == $defaultResponse}selected="selected"{/if}>{$polabel}</option>
							    {/foreach}
							</select>
						    </div>
						</div>
					    </div>
					{elseif $quKey == 'conSelect' }
					    <div class="qna-right">
						<div class="inline form-inline">
						    <div class="styled-before"></div>
						    <div class="styled">
							<select id="id_country" name="{$QID}_{$questionType.qdetail.id}[]" title="{$questionType.qdetail.question_type_value}">
							{if !isset($defaultResponse)}
							    <option value="">Select</option>
							{/if}
							{foreach from=$questionType.conSelect key=poKey item=polabel}
							    <option value="{$polabel.name}" {if $polabel.name == $defaultResponse}selected="selected"{/if}>{$polabel.name}</option>
							{/foreach}
						    </select>
						    </div>
						</div>
					    </div>
					{elseif $quKey == 'stateSelect'}
					    <div class="qna-right">
						<div class="inline form-inline">
						    <div class="styled-before"></div>
						    <div class="styled" id="id_state_div">
							<select name="{$QID}_{$questionType.qdetail.id}[]" id="id_state" title="{$questionType.qdetail.question_type_value}">
								<option value="">-</option>
							</select>
						    </div>
						</div>
					    </div>
					{elseif $quKey == 'zoneSelect'}
					    <div class="qna-right">
						<div class="inline form-inline">
						    <div class="styled-before"></div>
						    <div class="styled">
							<select id="zoneSelect" name="{$QID}_{$questionType.qdetail.id}[]" title="{$questionType.qdetail.question_type_value}">
							{if !isset($defaultResponse)}
							    <option value="">Select</option>
							{/if}
							{foreach from=$questionType.zoneSelect key=zoneKey item=zonePolabel}
							    <option value="{$zonePolabel.name}" {if $zonePolabel == $defaultResponse}selected="selected"{/if}>{$zonePolabel.name}</option>
							{/foreach}
						    </select>
						    </div>
						</div>
					    </div>
					{elseif $quKey == 'radio' }
						<div class="qna-right">
							{foreach from=$questionType.radio key=poKey item=polabel}
							    <div class="col-sec">
								<input id="{$QID}_{$countLoop}" name="{$QID}_{$questionType.qdetail.id}[]" type="radio" value="{$poKey}" {if $QID == $defaultResponse}checked="checked"{/if} />
								<label for="{$QID}_{$countLoop}">{$polabel}</label>
							    </div>
							{$countLoop = $countLoop+1}
							{/foreach}
						</div>
					{elseif $quKey == 'checkbox' }
						<div class="qna-right">
							{foreach from=$questionType.checkbox key=poKey item=polabel}
							    <div class="col-sec">
								<input id="{$QID}_{$countLoop}" name="{$QID}_{$questionType.qdetail.id}[]" type="checkbox" value="{$poKey}" {if $QID == $defaultResponse}checked="checked"{/if} />
								<label for="{$QID}_{$countLoop}">{$polabel}</label>
							    </div>
							    {$countLoop = $countLoop+1}
							{/foreach}
						</div>
					{elseif $quKey == 'datetime' }
						<div class="qna-right">
						    <input type="text" name="{$QID}_{$questionType.qdetail.id}[]" class="classDateTimePicker form-control" title="{$questionType.qdetail.question_type_value}">
						</div>
					{elseif $quKey == 'dateonly' }
						<div class="qna-right">
						    <input type="text" name="{$QID}_{$questionType.qdetail.id}[]" class="classDatePicker form-control" title="{$questionType.qdetail.question_type_value}">
						</div>
					{elseif $quKey == 'timeonly' }
						<div class="qna-right">
						    <input type="text" name="{$QID}_{$questionType.qdetail.id}[]" class="classTimePicker form-control" title="{$questionType.qdetail.question_type_value}">
						</div>
					{/if}

				    {/foreach}

				    </div>




				<input id="p_pro_id" name="p_pro_id[]" type="hidden" value='{$bmcdata->p_id}' />
				{/foreach}
				<div class="clearfix"></div>
				<div id="product_question_row_{$bmcdata->p_id}"></div>
				</div>



				<div class="prod-img-line" style="min-height: 10px;"></div>
				<!-- <div style="width: 84%; float: left;">* Please enter unique instance name</div> -->
					{if $bmcdata->p_name == 'Dexus Office Catalog'}

					{elseif $bmcdata->product_OS!='OFFICE365'}
				    <div style="width: 84%; float: left;">* Please enter unique instance name</div>
					{/if}
				{else}
				<div id="new_qna_form_footer">
				    <p style="color: #c44405; width: 300px; padding: 0px 10px;">No Question found.</p>
				</div>
				<div id="new_qna_form_footer">
				    <p class="fr">
					<a href="javascript:void(0);" onclick="for_global_qna='close';$.fancybox.close('close');">{l s='Cancel' mod='productquestionanswer'}</a>
				    </p>
				</div>
			    {/if}
			    </div>
			    </div>



			    {*{/for}*}

			    {if isset ($bmcdata->questions)}
			    {*<div class="apply-all">
				<div class="qna-row">
				    <div class="qna-left">
					{if $bmcdata->productQTY>1}
					<button name="autoload_{$j}" onclick="loadAll()" id="autoload_{$j}" class="button button-small apply-btn">Apply to all</button>
					{/if}
					<div class="apply-user"></div>
				    </div>
				    {if $finalKey!=0}
				    <div class="qna-right">
					<button name="loadprevious" id="loadprevious" class="button button-small">Load Previous Details</button>
				    </div>
				    {/if}
				</div>
			    </div>*}
			    {/if}
			<div class="line"></div>
			</div>


			</div>


		    </div>

		    <div>

		    </div>
		    {$j=$j+1}
		    {/foreach}
		    {*<input type="hidden" name="qna_counter" id="qna_counter" value="{$j}">*}

		    {*{if isset ($bmcdata->questions)}*}
			<div class="qna-row btn-next">
			    {*<input id="p_pro_id" name="p_pro_id" type="hidden" value='{$bmcdata->p_id}' />
			    <input id="p_bmc_id" name="p_bmc_id" type="hidden" value='{$bmcdata->p_bmc_id}' />*}
			    <div class="qna-left">
				{*<p class="flr required"><sup>*</sup> {l s='Required fields' mod='productquestionanswer'}</p>*}
			    </div>
			    <div class="qna-right btn-sec">
				<a href="javascript:void(0);" onclick="for_global_qna='close';$.fancybox.close();" class="button button-small cancle-btn">{l s='Cancel' mod='productquestionanswer'}</a>
				<button id="submitNewQnA" class="button button-small goto-btn" onclick="for_global_qna='submit';" name="submitQnA" type="submit">{l s='Next' mod='productquestionanswer'}</button>
			    </div>
			</div>
		    {*{/if}*}
		    {*
		    <div class="qna-row top-row-qna">


			<div class="qna-left">Question</div>
			<div class="qna-right">Answer</div>

		    </div>
		    *}
		    <div class="copy-page-index-body">


		    {*
		    <div class="qna-row">
			<div class="qna-left">Product Name: {$bmcdata->p_name}</div>
		    </div>
		    *}

		</div>
			<div class="tanancy_no" style="padding: 0px 20px 0px 20px">
		    <b>IMPORTANT: WHAT'S A TENANCY NAME?</b><br/>
		    Every customer on Office365 is provided with a single Tenancy under which all their services are created and reside. This tenancy name must be globally unique and cannot be changed. The tenancy takes the form of a domain name which you need to input above. Don't worry, you can still use your own domain names such as MyCompany.com  for your email address and other services hosted in your tenancy. However some services do require you to use this tenancy name to connect, so we recommend that you make it
		    <ul>
			<li>a) Short and easy to remember</li>
			<li>b) Relevant to your organisation</li>
		    </ul>
		    
		</div>
		<div class="tanancy_yes" style="padding: 0px 20px 0px 20px">
		    <b>IMPORTANT: PLEASE READ CAREFULLY</b><br/>
		    Please put in your existing Office365 tenancy name in the field above. It is critical that you put this information in correctly in order for the order to process automatically.<br/>
		    After you finish the purchase of these products, you will be sent an email with a link and instructions on how to link that Tenancy to our portal so that we can add the additional licenses into your existing tenancy. Please follow the instructions on that email carefully. Only an existing administrator of your current tenancy can complete the steps in that email.
		    
		</div>

		    </div>
			<input type="hidden" name="existOffice365" value="{$existOffice365}">
			<input type="hidden" name="existBundle663" value="{$existBundle663}">
		</form>
	    </div>
	    </div>
	    </div>
	</div>
	{/if}
	<!-- Fancybox -->
	{*{/foreach}*}
    </div>
