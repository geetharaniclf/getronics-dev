{*
* 2007-2014 CloudSelect
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@cloudselect.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade CloudSelect to newer
* versions in the future. If you wish to customize CloudSelect for your
* needs please refer to http://www.cloudselect.com for more information.
*
*  @author CloudSelect SA <contact@cloudselect.com>
*  @copyright  2007-2014 CloudSelect SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of CloudSelect SA
*}

<li><a href="#idTab12" class="idTabHrefShort">{l s='Questions' mod='productquestionanswer'}</a></li>