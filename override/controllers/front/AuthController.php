<?php

class AuthControllerCore extends FrontController
{
    /**
     * [$postData description]
     * @var array
     */
    protected $postData = array();

    /**
     * Initialize auth controller
     * @see FrontController::init()
     */
    public function init()
    {
        parent::init();
    }

    /**
     * Set default medias for this controller
     * @see FrontController::setMedia()
     */
    public function setMedia()
    {
        parent::setMedia();
        
        unset($this->js_files);
        if (!$this->useMobileTheme())
        $this->addCSS(_THEME_CSS_DIR_.'authentication.css');
        $countries = Country::getCountries($this->context->language->id, true);
        $countries = $this->getKeyID($countries);
        $this->context->smarty->assign('countries', $countries);
        $this->addJS(array(
            _THEME_JS_DIR_.'jquery.min.js',
            _THEME_JS_DIR_.'bootstrap.min.js',
            _THEME_JS_DIR_.'vue.min.js',
            _THEME_JS_DIR_.'vue-resource.min.js',
            _THEME_JS_DIR_.'registration.js',
        ));

    }

     /**
     * getExsistingCustomer description
     * @return [type] [description]
     */
    protected function proccessExsistingCustomer($customer)
    {
        $company = new Company(Tools::getValue('company_id'));
        $customer->contract_id = Tools::getValue('contract_id');
        $customer->company     = $company->companyName;
        $is_reseller           = Customer::getAccesslevel(Tools::getValue('contract_id'), Tools::getValue('company_id'));

        $access_level = '2';

         if ($is_reseller == '0') {
            $access_level = '1';
        }

        $customer->access_level = $access_level;
        $customer->company_id = Tools::getValue('company_id');
        
        return $customer;
    }

    /**
     * getKeyID description
     * @param  [type] $countries [description]
     * @return [type]            [description]
     */
    protected function getKeyID($countries)
    {
        $countriesRet = array();
        foreach ($countries as $key => $value) {
            $countriesRet[$value['id_country']] = $value;
        }

        return $countriesRet;
    }

     /**
     * Assign template vars related to page content
     * @see FrontController::initContent()
     */
    public function initContent()
    {
        $this->postData = json_decode(file_get_contents('php://input'), 1);
		//echo '<pre>'; print_R($this->postData); die; //json_encode(json_decode(file_get_contents('php://input'), 1);); die;
        if ($this->postData || !empty($_POST)) {
            $this->validateRegistration();
        }
        $this->display_header = false;
        $this->display_footer = false;
		
        $this->setTemplate(_PS_THEME_DIR_.'registration.tpl');  
    }

     /**
     * Start forms process
     * @see FrontController::postProcess()
     */
    public function validateRegistration()
    {
        $_POST = $this->postData['step1']; 
        if(is_array($this->postData['step2'])){
           $_POST = array_merge($_POST, $this->postData['step2']);
	    }
	    
        if(is_array($this->postData['step3'])){ 
		   $_POST = array_merge($_POST, $this->postData['step3']);
        }
		
        //Initiate Customer
        $customer = new Customer;        
        $errors = $customer->validateCustomer();
        
        
        /*if (!$this->checkCaptch() && $_POST['captcha']) {
            $errors['captcha'] = 1;
            $res =  array('error' => true, 'errors' => $errors);
            echo json_encode($res);exit;
        }*/
			
        if (!empty($errors)) {
            $res =  array('error' => true, 'errors' => $errors);
            echo json_encode($res); exit;
        }

        $this->register();
    }

    /**
     * [register description]
     * @return [type] [description]
     */
    protected function register()
    { 
        //Initiate Customer
        $customer = new Customer;
        $customer = $this->setCustomerData($customer);
        
        $customer = $this->setCustomerStaticData($customer);
        $customer->active = 0;
        $rj = new RijndaelCore(_RIJNDAEL_KEY_, _RIJNDAEL_IV_);
        $encryped = $rj->encrypt(Tools::getValue('password'));
		
        $encryped = $encryped;

        $customer->bmc_id = preg_replace('/ |  /','',$customer->firstname.$customer->lastname.$customer->contract_id.rand(1,200)).'@cloudselect.com';
        $customer->samFirstname = $customer->lastname.$customer->contract_id;
        $customer->plain_text_password = $encryped;
        $customer->phone_no_business = Tools::getValue('phone_no_business');
        
        $customer->add(); 
        $this->addAdress($customer); 
        if (Tools::getValue('already_registered') == 0) {
            $this->addExistingAdress($customer);
        }
        $this->afterRegistrationSuccess($customer);
    }

    protected function addExistingAdress($customer)
    {
        $address = new Address(Address::getFirstCompanyAddressId($customer->company_id));
        $address->id_customer = $customer->id;
        $address->add();
    }

    /**
     * checkCaptch description
     * @return [type] [description]
     */
    protected function checkCaptch()
    {
        $captcaUrl = 'https://www.google.com/recaptcha/api/siteverify';
        $data['secret']    = '6LdEOwwUAAAAALx6VAdEgnEPd1gFBG8j8F0ybPub';
        $data['remoteip']  = $_SERVER['HTTP_CLIENT_IP'];
        $data['response']  = Tools::getValue('captcha');

        return sendData($captcaUrl, $data);
    }

    /**
     * setCustomerData description
     */
    protected function setCustomerData($customer)
    {
        foreach ($customer->fillable as $key => $value) {
            if ($key == 'passwd') {
                $customer->passwd = Tools::encrypt(Tools::getValue('password'));
                continue;
            }

            $customer->{$key} = Tools::getValue($value);
        }

        return $customer;
    }

    /**
     * setCustomerStaticData description
     */
    protected function setCustomerStaticData($customer)
    {
        if (Tools::getValue('already_registered') == 1) {
            $customer->company_id = $this->addCompany();
            $customer->contract_id = $this->generateContractId('8', '4', '4');
            $customer->access_level = 3;
            $customer->company = Tools::getValue('company_name');
            
            return $customer;
        }

       return $this->proccessExsistingCustomer($customer);
    }

    /**
     * Process submit on a creation
     */
    protected function addCompany()
    {
        //$_POST['company'] = Tools::getValue('company_name');
        $company_name = Tools::getValue('company_name');
        //echo $company_name; exit;
        if(isset($company_name) && !$company_name)
        {
            $this->errors[] = Tools::displayError('Please enter company name.');
        }

        $address1 = Tools::getValue('address1');
        $address2 = Tools::getValue('address2');
        if(isset($address1) && !$address1)
        {
            $this->errors[] = Tools::displayError('Please enter address1.');
        }

        if ($id_country = Tools::getValue('country'))
        {
            // Check country
            if (!($country = new Country($id_country)) || !Validate::isLoadedObject($country))
            throw new PrestaShopException('Country cannot be loaded with address->id_country');

            if ((int)$country->contains_states && !(int)Tools::getValue('id_state'))
            $this->errors[] = Tools::displayError('This country requires you to chose a State.');

            // US customer: normalize the address
            if ($id_country == Country::getByIso('US') && Configuration::get('PS_TAASC'))
            {
                include_once(_PS_TAASC_PATH_.'AddressStandardizationSolution.php');
                $normalize = new AddressStandardizationSolution;
                $address1 = $normalize->AddressLineStandardization($address1);
                $address2 = $normalize->AddressLineStandardization($address2);
            }

            $postcode = Tools::getValue('postcode');
            /* Check zip code format */
            if ($country->zip_code_format && !$country->checkZipCode($postcode))
            $this->errors[] = sprintf(Tools::displayError('The Zip/Postal code you\'ve entered is invalid. It must follow this format: %s'), str_replace('C', $country->iso_code, str_replace('N', '0', str_replace('L', 'A', $country->zip_code_format))));
            elseif(empty($postcode) && $country->need_zip_code)
            $this->errors[] = Tools::displayError('A Zip / Postal code is required.');
            elseif ($postcode && !Validate::isPostCode($postcode))
            $this->errors[] = Tools::displayError('The Zip / Postal code is invalid.');

            // Check country DNI
            if ($country->isNeedDni() && (!Tools::getValue('dni') || !Validate::isDniLite(Tools::getValue('dni'))))
            $this->errors[] = Tools::displayError('The identification number is incorrect or has already been used.');
            else if (!$country->isNeedDni())
            $address->dni = null;

            $city = Tools::getValue('city');
            
        }

        $companyDomain = 'admin@'.end(explode('@', $_POST['email']));
        $country_name = Country::getNameById($this->context->language->id, $_POST['id_country']);
        $country_Iso = Country::getIsoById($_POST['id_country']);
        $id_state = Tools::getValue('id_state');
        $state_name = State::getNameById($_POST['state']);
        $company_size = Tools::getValue('company_size');
        $company_type = Tools::getValue('company_type');
        $is_reseller = Tools::getValue('is_reseller');
        $city = Tools::getValue('city');
        $postcode = Tools::getValue('postcode');

        
        if(!$this->errors)
        {
            $address1 = Tools::getValue('address1');

            Db::getInstance()->execute('INSERT INTO '._DB_PREFIX_.'company (`companyName`, `active`, `bmc_id`, `companyDomain`, `address1`, `address2`, `state`, `city`, `country`, `zipcode`, `country_iso`, `company_size`, `company_type`, `is_reseller`)
                           VALUES ("'.$company_name.'", "1", "", "'.$companyDomain.'", "'.$address1.'", "'.$address2.'", "'.$state_name.'", "'.$city.'", "'.$country_name.'", "'.$postcode.'", "'.$country_Iso.'", "'.$company_size.'", "'.$company_type.'", "'.$is_reseller.'")');

            $companyID = Db::getInstance()->Insert_ID();
        }

        return $companyID;
    }

    /**
     * Generating Contract_id
     */
    public function generateContractId($numchars=8,$digits=4,$letters=4)
    {
        $dig = "012345678923456789";
        $abc = "ABCDEFGHJKLMNOPQRSTUVWXYZ";
        $str ='';
        $randomized = '';
        if($letters == 4)
        {
            $str .= $abc;
        }
        if($digits == 4)
        {
            $str .= $dig;
        }
        for($i=0; $i < $numchars; $i++)
        {
            $randomized .= $str{rand() % strlen($str)};
        }
        return $randomized;
    }

    /**
     * addAdress description
     */
    protected function addAdress($customer)
    {
        $address = new Address(); 
        $address->id_country    = Tools::getValue('id_country'); //id_country
        $address->id_state      = Tools::getValue('state');

        $address->id_customer   = $customer->id;
        $address->alias         = Tools::ucwords(Tools::getValue('firstname'));
        $address->company       = @trim(Tools::getValue('company_name'));

        $address->lastname      = trim(Tools::getValue('lastname'));
        $address->firstname     = trim(Tools::getValue('firstname'));
        $address->address1      = Tools::getValue('address1'); //Tools::getValue('address1')
        $address->address2      = Tools::getValue('address2');
        $address->postcode      = Tools::getValue('postcode');
        $address->city          = Tools::getValue('city');
        $address->phone_mobile  = Tools::getValue('phone_no_business');
         $address->phone  = Tools::getValue('phone_no_business');
        $address->date_add      = date('Y-m-d H:i:s');
        $address->date_upd      = date('Y-m-d H:i:s');
        $address->company_id    = $customer->company_id;
        $address->add(); 
    }

     /**
     * [afterRegistrationSuccess description
     * @return [type] [description]
     */
    protected function afterRegistrationSuccess($customer)
    {
        $this->sendConfirmationMail($customer);
        // $this->sendDataToRemedy($customer);
        
        //Send mail to user
        echo json_encode(array('error' => false, 'success' => 1));exit;
    }

    /**
     * sendConfirmationMail
     * @param Customer $customer
     * @return bool
     */
    protected function sendConfirmationMail(Customer $customer)
    {
        if (!Configuration::get('PS_CUSTOMER_CREATION_EMAIL'))
        return true;

        return Mail::Send(
        $this->context->language->id,
            'account',
        Mail::l('Welcome!'),
        array(
                '{firstname}' => $customer->firstname,
                '{lastname}' => $customer->lastname,
                '{email}' => $customer->email,
                '{passwd}' => Tools::getValue('passwd'),
                '{link}' => $this->context->link->getPageLink('login', true, null, 'token='.$customer->secure_key.'&id_customer='.(int)$customer->id)),
        $customer->email,
        $customer->firstname.' '.$customer->lastname
        );
    }

}
