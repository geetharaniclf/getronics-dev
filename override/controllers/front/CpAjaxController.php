<?php

/*
 * 2007-2013 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author PrestaShop SA <contact@prestashop.com>
 *  @copyright  2007-2013 PrestaShop SA
 *  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

class CpAjaxControllerCore extends FrontController {

    protected $create_account;

    // public $isLogged;
    //  protected $ajax_refresh = false;
//
//    public function setMedia() {
//
//        parent::setMedia();
//        $this->addCSS(_PS_THEME_DIR_ . 'css/ctrlCss/all_skins.css');
//        $this->addCSS(_PS_THEME_DIR_ . 'css/ctrlCss/custom_main.css');
//        $this->addCSS(_PS_THEME_DIR_ . 'css/ctrlCss/font-awesome.css');
//    }

    public function init() {

        //$this->page_name = 'allproducts';
        if (!$this->context->customer->isLogged() && $this->php_self != 'authentication' && $this->php_self != 'password')
            Tools::redirect('index.php?controller=authentication?back=my-account');
        parent::init();
        $this->display_column_right = false; // hides left column
        $this->display_column_left = false; // hides left column
        $this->display_header = false;
        $this->display_footer = false;
    }

    public function initContent() {
        parent::initContent();


        //  echo '<pre>'; print_r($rowData); exit;
        $rowsData = array();
        $page = Tools::getValue('page');
        $search = Tools::getValue('s');

        if ($page == 'virtual_machines') {
             
             $vmDetail = Tools::getValue('vmdetail');
             if($vmDetail==''){
            $data = $this->virtual_matchine();
            
             }else{
                 $data = $this->vmDetail($vmDetail);
                 $data = array('data'=>$data);
             }
            
            $this->context->smarty->assign($data);
        }
        
        if ($page == 'monitoring') {
             
           $data = $this->monitoring();
           $this->context->smarty->assign($data);
        }
        
        
        
        
        if ($page == 'update_state') {
            $data = $this->update_state();
            $this->context->smarty->assign('rowData',$data);
        }

        $this->context->smarty->assign(array('one_var' => _PS_THEME_DIR_,
            'page' => $page
                )
        );

        if ($page == '') {
            $this->setTemplate(_PS_THEME_DIR_ . 'control_panel/dashboard.tpl');
        } elseif ($page == 'tenant_management') {
            $this->setTemplate(_PS_THEME_DIR_ . 'control_panel/tenant_management.tpl');
        } elseif ($page == 'o365') {
            $this->setTemplate(_PS_THEME_DIR_ . 'control_panel/o365.tpl');
        } elseif ($page == 'virtual_machines') {
            if($vmDetail!=''){
            $this->setTemplate(_PS_THEME_DIR_ . 'control_panel/ajax_file/virtual_machines_details.tpl');
             }else{
            $this->setTemplate(_PS_THEME_DIR_ . 'control_panel/ajax_file/virtual_machines.tpl');
             }
        } elseif ($page == 'users') {
            $this->setTemplate(_PS_THEME_DIR_ . 'control_panel/users.tpl');
        } elseif ($page == 'circuit_unify') {
            $this->setTemplate(_PS_THEME_DIR_ . 'control_panel/circuit_unify.tpl');
        } elseif ($page == 'reseller_unify') {
            $this->setTemplate(_PS_THEME_DIR_ . 'control_panel/reseller_unify.tpl');
        } elseif ($page == 'billing') {
            $this->setTemplate(_PS_THEME_DIR_ . 'control_panel/billing.tpl');
        } elseif ($page == 'support') {
            $this->setTemplate(_PS_THEME_DIR_ . 'control_panel/ajax_file/support.tpl');
        } elseif ($page == 'virtual_data_center') {
            $this->setTemplate(_PS_THEME_DIR_ . 'control_panel/virtual_data_center.tpl');
        } elseif ($page == 'waas_management') {
            $this->setTemplate(_PS_THEME_DIR_ . 'control_panel/waas_management.tpl');
        } elseif ($page == 'service_request') {
            $this->setTemplate(_PS_THEME_DIR_ . 'control_panel/service_request.tpl');
        } elseif ($page == 'monitoring') {
            $this->setTemplate(_PS_THEME_DIR_ . 'control_panel/ajax_file/monitoring.tpl');
        } elseif ($page == 'invoices') {
            $this->setTemplate(_PS_THEME_DIR_ . 'control_panel/invoices.tpl');
        
        } elseif ($page == 'excl') {
            $this->excl();
        
        } elseif ($page == 'pdf') {
            $this->pdf();
        
        } elseif ($page == 'update_state') {
            
            $this->setTemplate(_PS_THEME_DIR_ . 'control_panel/ajax_file/state.tpl');
        
        } elseif ($page == 'startvm') {
            
           $this->startvm();
        
        } elseif ($page == 'stopvm') {
            
           $this->stopvm();
        }
        
    }
    
    
     public function monitoring() {
      
        $offset = 0;
        $p = Tools::getValue('p');
        $search = Tools::getValue('search');
        
        $n = 10;

        if ($p != '') {
            if ($p > 1) {
                $offset = ($p - 1) * $n;
            }
        }
        
        $masterColumns = array();
		
		$mssql = "SELECT * FROM `cfx_monitor_master`"; 
		$mssqlData = DB::getInstance()->executeS($mssql);
		
		if(!empty($mssqlData)){ 
			foreach($mssqlData as $column){ 
				$masterColumn[$column['monitor_id']] = $column['monitor_name'].' ('.$column['monitor_unit'].')' ;
			}
		}

        $sql = "Select name,state,provider,guid From cfx_virtual_machine order by id DESC limit $n offset $offset";
        if($search!=''){
        $sql = "Select name,state,provider,guid From cfx_virtual_machine Where "
                . "name Like '%$search%'"
                . "OR provider Like '%$search%'" 
                . " order by id DESC limit $n offset $offset";
        }
        $rowsData = DB::getInstance()->executeS($sql);

            $sql = "Select name,state,provider,guid From cfx_virtual_machine";
            if($search!=''){
              
				$sql = "Select name,state,provider,guid From cfx_virtual_machine Where "
						. "name Like '%$search%'"
						. "OR provider Like '%$search%'" ;
            }
           $rowsDataCount = DB::getInstance()->executeS($sql);
           
        
        if(!empty($rowsData)){ 
			
			foreach($rowsData as $vmrow){ 
				
				$snglvm = array();
				$snglvm = $vmrow;
				$moniappendsql = "SELECT * FROM (SELECT * FROM `cfx_raw_data` order by timestamp DESC) as sorted_raw_data where guid='".$vmrow['guid']."' GROUP by monitor_id";
                $moniappendData =  DB::getInstance()->executeS($moniappendsql);
                if(!empty($moniappendData)){ 
					foreach($moniappendData as $monidata)
					{ 
						$snglvm['monitor_'.$monidata['monitor_id']] = $monidata['value'];
					}
				}
             
                $vertualMachines[] = $snglvm;
    			
			}
			
		}
		
        $nb_orders = count($rowsDataCount);
        $pages_nb = ceil($nb_orders / (int) $n);
        $range = 2; /* how many pages around page selected */
        $start = (int) ($p - $range);

        if ($start < 1)
            $start = 1;
        $stop = (int) ($p + $range);
        if ($stop > $pages_nb)
            $stop = (int) $pages_nb;

        if (!$p)
            $p = 1;


        return array(
            'pages_nb' => $pages_nb,
            'prev_p' => $p != 1 ? $p - 1 : 1,
            'next_p' => (int) $p + 1 > $pages_nb ? $pages_nb : $p + 1,
            //'requestPage' => 'ctrlpanel?page=virtual_machines',
            'requestPage' => '',
            'p' => $p,
            'n' => $n,
            'range' => $range,
            'start' => $start,
            'stop' => $stop,
            'vertualMachines' => $vertualMachines,
            'mssqlData'=>$mssqlData
        );
        exit;
    }
    
    
    
    
    

    public function virtual_matchine() {
        $offset = 0;
        $p = Tools::getValue('p');
        $search = Tools::getValue('search');
        
        $n = 10;

        if ($p != '') {
            if ($p > 1) {

                $offset = ($p - 1) * $n;
            }
        }
        $sql = "Select * From cfx_virtual_machine Where user_id='democs3@mailinator.com' order by id DESC limit $n offset $offset";
        if($search!=''){
        $sql = "Select * From cfx_virtual_machine Where user_id='democs3@mailinator.com' AND ("
                . "name Like '%$search%'"
                . "OR service_name Like '%$search%'"
                . "OR state Like '%$search%'" 
                . "OR private_ip Like '%$search%'" 
                . "OR public_ip Like '%$search%'" 
                . "OR type Like '%$search%'" 
                . "OR provider Like '%$search%')" 
                . " order by id DESC limit $n offset $offset";
        }
        $rowsData = DB::getInstance()->executeS($sql);

        $sql = "Select * From cfx_virtual_machine Where user_id='democs3@mailinator.com'";
          if($search!=''){
              
        $sql = "Select * From cfx_virtual_machine Where user_id='democs3@mailinator.com' AND ("
                . "name Like '%$search%'"
                . "OR service_name Like '%$search%'"
                . "OR state Like '%$search%'" 
                . "OR private_ip Like '%$search%'" 
                . "OR public_ip Like '%$search%'" 
                . "OR type Like '%$search%'" 
                . "OR provider Like '%$search%')" 
                ;
        }
        $rowsDataCount = DB::getInstance()->executeS($sql);


        $nb_orders = count($rowsDataCount);
        $pages_nb = ceil($nb_orders / (int) $n);
        $range = 2; /* how many pages around page selected */
        $start = (int) ($p - $range);

        if ($start < 1)
            $start = 1;
        $stop = (int) ($p + $range);
        if ($stop > $pages_nb)
            $stop = (int) $pages_nb;

        if (!$p)
            $p = 1;


        return array(
            'pages_nb' => $pages_nb,
            'prev_p' => $p != 1 ? $p - 1 : 1,
            'next_p' => (int) $p + 1 > $pages_nb ? $pages_nb : $p + 1,
            //'requestPage' => 'ctrlpanel?page=virtual_machines',
            'requestPage' => '',
            'p' => $p,
            'n' => $n,
            'range' => $range,
            'start' => $start,
            'stop' => $stop,
            'rowsData' => $rowsData
        );
        exit;
    }
    
    
    public function excl(){
         $offset = 0;
        $p = Tools::getValue('p');
        $search = Tools::getValue('search');
        
        $n = 10;

        if ($p != '') {
            if ($p > 1) {

                $offset = ($p - 1) * $n;
            }
        }
          $sql = "Select name, service_name, state, private_ip, public_ip, type, provider From cfx_virtual_machine Where user_id='democs3@mailinator.com' AND ("
                . "name Like '%$search%'"
                . "OR service_name Like '%$search%'"
                . "OR state Like '%$search%'" 
                . "OR private_ip Like '%$search%'" 
                . "OR public_ip Like '%$search%'" 
                . "OR type Like '%$search%'" 
                . "OR provider Like '%$search%')" 
                . " order by id DESC limit $n offset $offset";
        
        $rowsDataCount = DB::getInstance()->executeS($sql);
//print_r($rowsDataCount); exit;
        
        
        $result = $rowsDataCount;
//while ($row = $result) {
//	$header[] = $row[0];
//}	
$header=array(
  'Name','Service Name','Status','Private IP', 'Public IP', 'Type', 'Provider'
);

$filename = 'abc.xls';
$fp = fopen('php://output', 'w');
header('Content-type: application/xls');
header('Content-Disposition: attachment; filename='.$filename);
fputcsv($fp, $header);

$num_column = count($header);		
$arrayval = array();
        if(!empty($rowsDataCount)) {
foreach($rowsDataCount as $row){
    
    
    
    
    	fputcsv($fp, $row);
}
        }else{
            fputcsv($fp, $arrayval);
        }
exit;
    }

    
    
    public function pdf(){
        
          $offset = 0;
        $p = Tools::getValue('p');
        $search = Tools::getValue('search');
        
        $n = 10;

        if ($p != '') {
            if ($p > 1) {

                $offset = ($p - 1) * $n;
            }
        }
          $sql = "Select name, service_name, state, private_ip, public_ip, type, provider From cfx_virtual_machine Where user_id='democs3@mailinator.com' AND ("
                . "name Like '%$search%'"
                . "OR service_name Like '%$search%'"
                . "OR state Like '%$search%'" 
                . "OR private_ip Like '%$search%'" 
                . "OR public_ip Like '%$search%'" 
                . "OR type Like '%$search%'" 
                . "OR provider Like '%$search%')" 
                . " order by id DESC limit 100 offset $offset";
        
        $rowsDataCount = DB::getInstance()->executeS($sql);

$arrayval = array();
        if(!empty($rowsDataCount)) {
            
            
$datarows[]=$rowsDataCount;
             //   require_once '/var/www/html/getronics/classes/pdf/HTMLTemplateVirtualMachine.php';
    $pdf = new PDF($datarows, 'VirtualMachine', Context::getContext()->smarty);
//    echo '<pre>';
//               print_r($pdf); exit;
        $pdf->render();
    
    	

        }
        
        
        
    }
    
    
    function update_state(){
         $guid = Tools::getValue('guid');
          $state = Tools::getValue('state');
          
          $sql = "Select state, guid From cfx_virtual_machine Where guid='$guid'";
   return     $current_state = DB::getInstance()->getRow($sql);
   
       
    }
    
       function vmDetail($vmdetail){
        
        
        $sql = "Select * From cfx_virtual_machine Where guid='$vmdetail'";
      return  $vmdata = DB::getInstance()->getRow($sql);
        
      
    }
    
    function startvm(){
         $arrayguid = Tools::getValue('arrayguid');
        echo $arrayguid;
        
         $guidsarray = explode(',', $arrayguid);
         
         foreach($guidsarray as $guid){
             
          $sql = "Select * From cfx_virtual_machine Where guid='$guid'";  
          $vmachine = DB::getInstance()->getRow($sql);
          
          print_r($vmachine);
          
          if($vmachine['state']=='Installed' || $vmachine['state']=='Error' || $vmachine['state']=='Stopped' )
          if($vmachine['provider']=='Amazon'){
              
             $instance_id = $vmachine['name'];
             
              //$aws_response = shell_exec('AWS_ACCESS_KEY_ID=AKIAJLEHUY4HJVJLMB6A AWS_SECRET_ACCESS_KEY=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  aws ec2 start-instances --instance-id '.$instance_id.' --region eu-west-1');
          }
          elseif($vmachine['provider']=='Azure'){
              echo 'hi'; exit;
              
          }
             
         }
         exit;
         
     }
    function stopvm(){
         $arrayguid = Tools::getValue('arrayguid');
        print_r($arrayguid); exit;
    }
}
