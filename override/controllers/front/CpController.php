<?php

/*
 * 2007-2013 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author PrestaShop SA <contact@prestashop.com>
 *  @copyright  2007-2013 PrestaShop SA
 *  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

class CpControllerCore extends FrontController {

    protected $create_account;

    // public $isLogged;
    //  protected $ajax_refresh = false;


    public function setMedia() {

        parent::setMedia();
        $this->addCSS(_PS_THEME_DIR_ . 'css/ctrlCss/all_skins.css');
        $this->addCSS(_PS_THEME_DIR_ . 'css/ctrlCss/custom_main.css');
        $this->addCSS(_PS_THEME_DIR_ . 'css/ctrlCss/font-awesome.css');
    }

    public function init() {
        //$this->page_name = 'allproducts';
        if (!$this->context->customer->isLogged() && $this->php_self != 'authentication' && $this->php_self != 'password')
            Tools::redirect('index.php?controller=authentication?back=my-account');
        parent::init();
        $this->display_column_right = false; // hides left column
        $this->display_column_left = false; // hides left column
        
    }

    public function initContent() {
        parent::initContent();
        
      
     //  echo '<pre>'; print_r($rowData); exit;
        $rowsData =array();
           $page = Tools::getValue('page');
           $search = Tools::getValue('s');
       
      if ($page == 'virtual_machines') {
          
          $vmDetail = Tools::getValue('vmdetail');
             if($vmDetail==''){
            $data = $this->virtual_matchine();
            
             }else{
                 $data = $this->vmDetail($vmDetail);
                 $data = array('data'=>$data);
             }
        
        $this->context->smarty->assign($data);
        
      }
      if ($page == 'monitoring') {
          
          
            $data = $this->sys_montring();
          
        $this->context->smarty->assign($data);
        
      }

        $this->context->smarty->assign(array('one_var'=> _PS_THEME_DIR_,
                'page'=>$page
                
                )
               
                );
        
        if ($page == '') {
            $this->setTemplate(_PS_THEME_DIR_ . 'control_panel/dashboard.tpl');
        } elseif ($page == 'tenant_management') {
            $this->setTemplate(_PS_THEME_DIR_ . 'control_panel/tenant_management.tpl');
        } elseif ($page == 'o365') {
            $this->setTemplate(_PS_THEME_DIR_ . 'control_panel/o365.tpl');
        } elseif ($page == 'virtual_machines') {
            if($vmDetail!=''){
            $this->setTemplate(_PS_THEME_DIR_ . 'control_panel/virtual_machines_details.tpl');
             }else{
            $this->setTemplate(_PS_THEME_DIR_ . 'control_panel/virtual_machines.tpl');
             }
        } elseif ($page == 'users') {
            $this->setTemplate(_PS_THEME_DIR_ . 'control_panel/users.tpl');
        } elseif ($page == 'circuit_unify') {
            $this->setTemplate(_PS_THEME_DIR_ . 'control_panel/circuit_unify.tpl');
        } elseif ($page == 'reseller_unify') {
            $this->setTemplate(_PS_THEME_DIR_ . 'control_panel/reseller_unify.tpl');
        } elseif ($page == 'billing') {
            $this->setTemplate(_PS_THEME_DIR_ . 'control_panel/billing.tpl');        
        } elseif ($page == 'support') {
            $this->setTemplate(_PS_THEME_DIR_ . 'control_panel/support.tpl');
        }
        elseif ($page == 'virtual_data_center') {
            $this->setTemplate(_PS_THEME_DIR_ . 'control_panel/virtual_data_center.tpl');
        }
        elseif ($page == 'waas_management') {
            $this->setTemplate(_PS_THEME_DIR_ . 'control_panel/waas_management.tpl');
        }
        elseif ($page == 'service_request') {
            $this->setTemplate(_PS_THEME_DIR_ . 'control_panel/service_request.tpl');
        }
        elseif ($page == 'monitoring') {
			$data = $this->sys_montring();
            $this->context->smarty->assign($data);
            $this->setTemplate(_PS_THEME_DIR_ . 'control_panel/monitoring.tpl');
        }
        elseif ($page == 'invoices') {
            $this->setTemplate(_PS_THEME_DIR_ . 'control_panel/invoices.tpl');
        }
    }

    
    
    
    public function virtual_matchine(){ 
           $offset = 0;
          $p = Tools::getValue('p');
           $n = 10; 
          
          if($p!=''){
              if($p>1){
                  
                  $offset=($p-1)*$n;
              }
              
          }
            $sql = "Select * From cfx_virtual_machine Where user_id='democs3@mailinator.com' order by id DESC limit $n offset $offset";
      $rowsData =  DB::getInstance()->executeS($sql);
        
      $sql = "Select * From cfx_virtual_machine Where user_id='democs3@mailinator.com'";
      $rowsDataCount =  DB::getInstance()->executeS($sql);
        
      
      $nb_orders=count($rowsDataCount);
      $pages_nb = ceil($nb_orders / (int)$n);
       $range = 2; /* how many pages around page selected */
        $start = (int)($p - $range);
      
    if ($start < 1)
            $start = 1;
        $stop = (int)($p + $range);
        if ($stop > $pages_nb)
            $stop = (int)$pages_nb;
 
        if (!$p) $p = 1;
      
        
      return  array(
            'pages_nb' => $pages_nb,
            'prev_p' => $p != 1 ? $p - 1 : 1,
            'next_p' => (int)$p + 1  > $pages_nb ? $pages_nb : $p + 1,
            //'requestPage' => 'ctrlpanel?page=virtual_machines',
            'requestPage' => '',
            'p' => $p,
            'n' => $n,
            'range' => $range,
            'start' => $start,
            'stop' => $stop,
          'rowsData'=>$rowsData  
        );
      exit;
    }
    
     function vmDetail($vmdetail){
        
        
        $sql = "Select * From cfx_virtual_machine Where guid='$vmdetail'";
        $vmdata = DB::getInstance()->getRow($sql);
        if(!empty($vmdata)){
      return $vmdata;
        }else{
            
       Tools::redirect('index.php?controller=cp?page=virtual_machines');     
        }
    }
    
    function sys_montring(){
		 
		
		$offset = 0;
        $p = Tools::getValue('p');
        $n = 10; 
          
        if($p!=''){
            if($p>1){
                  $offset=($p-1)*$n;
            }
        } 
		
        $masterColumns = array();
		
		$mssql = "SELECT * FROM `cfx_monitor_master`"; 
		$mssqlData = DB::getInstance()->executeS($mssql);
		
		if(!empty($mssqlData)){ 
			foreach($mssqlData as $column){ 
				$masterColumn[$column['monitor_id']] = $column['monitor_name'].' ('.$column['monitor_unit'].')' ;
			}
		}
		
		$vertualMachines = array();
		$sql = "Select name,state,provider,guid From cfx_virtual_machine order by id DESC limit $n offset $offset";
        $vmsData =  DB::getInstance()->executeS($sql);
        
        $rawdatasql = "Select name,state,provider,guid From cfx_virtual_machine";
        
        $rowsDataCount =  DB::getInstance()->executeS($rawdatasql);
        
        if(!empty($vmsData)){ 
			
			foreach($vmsData as $vmrow){ 
				
				$snglvm = array();
				$snglvm = $vmrow;
				$moniappendsql = "SELECT * FROM (SELECT * FROM `cfx_raw_data` order by timestamp DESC) as sorted_raw_data where guid='".$vmrow['guid']."' GROUP by monitor_id";
                $moniappendData =  DB::getInstance()->executeS($moniappendsql);
                if(!empty($moniappendData)){ 
					foreach($moniappendData as $monidata)
					{ 
						$snglvm['monitor_'.$monidata['monitor_id']] = $monidata['value'];
					}
				}
             
                $vertualMachines[] = $snglvm;
    			
			}
			
		}
		
		
	 $nb_orders=count($rowsDataCount);
     $pages_nb = ceil($nb_orders / (int)$n);
     $range = 2; /* how many pages around page selected */
     $start = (int)($p - $range);
      
     if ($start < 1)
     {
          $start = 1;
          $stop = (int)($p + $range);
          if($stop > $pages_nb){ $stop = (int)$pages_nb; }
          if(!$p){ $p = 1; }
          
     }    
      
        
      return  array(
            'pages_nb' => $pages_nb,
            'prev_p' => $p != 1 ? $p - 1 : 1,
            'next_p' => (int)$p + 1  > $pages_nb ? $pages_nb : $p + 1,
            'requestPage' => '',
            'p' => $p,
            'n' => $n,
            'range' => $range,
            'start' => $start,
            'stop' => $stop,
            'vertualMachines'=>$vertualMachines,
            'mssqlData'=>$mssqlData,
        );
		
		
	
    }
    
}
