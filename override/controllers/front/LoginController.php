<?php
/*
* 2007-2013 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2013 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/
//require(_PS_HELPER_DIR_.'HelperDomainControllerAdlapd.php');
class LoginControllerCore extends FrontController
{


	/**
	 * @var bool create_account
	 */
	protected $create_account;

	/**
	 * Initialize auth controller
	 * @see FrontController::init()
	 */
	public function init()
	{
		parent::init();
		$this->display_column_right = false; // hides left column
		$this->display_column_left = false; // hides left column

		if(Tools::getValue('token') && Tools::getValue('id_customer'))
			$this->active_account();

	}

	/**
	 * Assign template vars related to page content
	 * @see FrontController::initContent()
	 */
	public function initContent()
	{
		if (!Tools::getIsset('step') && $this->context->customer->isLogged() && !$this->ajax)
			Tools::redirect('index.php?controller='.(($this->authRedirection !== false) ? urlencode($this->authRedirection) : 'my-account'));

		$back = Tools::getValue('back');
		$key = Tools::safeOutput(Tools::getValue('key'));
		if (!empty($key))
			$back .= (strpos($back, '?') !== false ? '&' : '?').'key='.$key;

		if ($back == Tools::secureReferrer(Tools::getValue('back')))
			$this->context->smarty->assign('back', html_entity_decode($back));
		else
			$this->context->smarty->assign('back', Tools::safeOutput($back));


		parent::initContent();
		if(Tools::getValue('success')){
			$this->context->smarty->assign('success',2);
		}
		$this->setTemplate(_PS_THEME_DIR_.'login.tpl');

	}

	public function active_account()
	{
		$id_customer = Tools::getValue('id_customer');
		$secure_key = Tools::getValue('token');
		$customer = new Customer($id_customer, $secure_key);
		$customer->getCustomersById($id_customer, $secure_key);

		$customer->active = 1;

		if($customer->update())
		{
			$mail_params = array(
				'{email}' => $customer->email,
				'{lastname}' => $customer->lastname,
				'{firstname}' => $customer->firstname,
			    '{contract_id}' => $customer->contract_id,
				'{link}' => $this->context->link->getPageLink('login', true, null, null)
			);
			if (Mail::Send($this->context->language->id, 'active_account', Mail::l('Account Successfully Activated'), $mail_params, $customer->email, $customer->firstname.' '.$customer->lastname))
				$this->context->smarty->assign(array('success' => 1, 'customer_email' => $customer->email));
			else
				$this->errors[] = Tools::displayError('An error occurred while sending the email.');
		}



		if(!$customer->active){

			$result = Db::getInstance()->executeS("SELECT  plain_text_password FROM "._DB_PREFIX_."customer WHERE id_customer =".$id_customer);


			$customer->active = 1;
			$rj = new RijndaelCore(_RIJNDAEL_KEY_, _RIJNDAEL_IV_);
			$customer->plain_text_password = $result[0]['plain_text_password'];
            if($customer->update())
			{
				$result[0]['plain_text_password'] ? $pass = $rj->decrypt($result[0]['plain_text_password']) : $pass = 'Your choosen password';
				$mail_params = array(
					'{email}' => $customer->email,
					'{lastname}' => $customer->lastname,
					'{firstname}' => $customer->firstname,
					'{showpassword}' => $pass,
				    '{contract_id}' => $customer->contract_id,
					'{link}' => $this->context->link->getPageLink('login', true, null, null)
				);
				if (Mail::Send($this->context->language->id, 'active_account', Mail::l('Account Successfully Activated'), $mail_params, $customer->email, $customer->firstname.' '.$customer->lastname))
					$this->context->smarty->assign(array('success' => 1, 'customer_email' => $customer->email));
				else
					$this->errors[] = Tools::displayError('An error occurred while sending the email.');
			}
            $this->sendDataToRemedy($customer);
            
            // reupdate country in company table
			
        }
        $companyID = $customer->company_id;
		$adress = (new Address(Address::getFirstCompanyAddressId($customer->company_id)));
		$id_state = $adress->id_state;
		$id_country = $adress->id_country;
		
		$sql = 'UPDATE '._DB_PREFIX_.'company SET country = '.$id_country.', state = '.$id_state.' WHERE id_company = '.$companyID;
		$sql2 = 'UPDATE '._DB_PREFIX_.'address SET id_country = '.$id_country.', id_state = '.$id_state.' WHERE company_id = '.$companyID;
		Db::getInstance()->execute($sql);
		Db::getInstance()->execute($sql2);		



	}

	 /**
     * sendDataToRemedy description
     * @return [type] [description]
     */
    protected function sendDataToRemedy($customer)
    {
    	$adress = (new Address(Address::getFirstCompanyAddressId($customer->company_id)));
        $country = new Country($adress->id_country);
        
        $dataToSendRemedy = array(
            'firstname' => $customer->firstname,
            'lastname' => $customer->lastname,
            'reg_id' => $customer->reg_id,
            'email' => $customer->email,
            'passwd' => $customer->passwd,
            'id_customer' => "{$customer->id}",
            'id_shop' => (int)$customer->id_shop,
            'id_shop_group' => $customer->id_shop_group,
            'company_name' =>  $customer->company,
            'address_1' => $adress->address1,
            'city' => $adress->city,
            'state' => $this->getStateName($adress->id_state),
            'postcode' => $adress->postcode,
            'country' => $country->name[1],
            'company_type' => array("1"),
            'access_level' => (int)$customer->access_level,
            'is_reseller' => (int)$customer->is_reseller,
            'contract_id' => $customer->contract_id,
            'payment_method' => array("2"),
            'shop_id' => null,
            'user_id' => $customer->email,
            'company_id' => $customer->company_id,
            'reseller_id' => (int)$customer->id_shop,
            'create_new_user' => '1',
            'cloudselect_unique_key' => $customer->bmc_id,
            'phone_no_business' => $customer->phone_no_business,
            'country_code' => "DE",
    		'country_culture' => "EN-US",
    		'country_language' => "EN",
        );
        sendDataToMiddleware('remedy/createuser', $dataToSendRemedy, true);
    }

	public function postProcess()
	{
       // print_r($_POST);exit;
            //Login process
            if (Tools::isSubmit('SubmitCreateNew'))
		Tools::redirect('register');
	    elseif (Tools::isSubmit('SubmitLoginNew'))
		$this->processSubmitLoginNew();


	}

	protected function getStateName($stateId)
    {
        $sql = "SELECT name FROM csb_state WHERE id_state = '".$stateId."'";
        $row = Db::getInstance()->getRow($sql);

        return $row['name'];
    }
    
	/**
	 * Process login
	 */
	protected function processSubmitLoginNew()
	{
		Hook::exec('actionBeforeAuthentication');
		$passwd = trim(Tools::getValue('passwd'));
		$email = trim(Tools::getValue('email'));

		if (empty($email))
			$this->errors[] = Tools::displayError('An email address required.');
		elseif (!Validate::isEmail($email))
			$this->errors[] = Tools::displayError('Invalid email address.');
		elseif (empty($passwd))
			$this->errors[] = Tools::displayError('Password is required.');
		elseif (!Validate::isPasswd($passwd))
			$this->errors[] = Tools::displayError('Invalid password.');
		else
		{
			$customer = new Customer();
			// if($ldapHelper = new HelperDomainControllerAdlapd()) {
			// 	if($ldap = $ldapHelper->aldapAuthAccess($email))
			// 		$passwd = null;
			// }
			$authentication = $customer->getByEmail(trim($email), trim($passwd), true, $ldap);
			if (!$authentication || !$customer->id)
				$this->errors[] = Tools::displayError('Authentication failed.');
			else
			{
				$this->context->cookie->id_compare = isset($this->context->cookie->id_compare) ? $this->context->cookie->id_compare: CompareProduct::getIdCompareByIdCustomer($customer->id);
				$this->context->cookie->id_customer = (int)($customer->id);
				$this->context->cookie->customer_lastname = $customer->lastname;
				$this->context->cookie->customer_firstname = $customer->firstname;
				$this->context->cookie->logged = 1;
				$customer->logged = 1;
				$this->context->cookie->is_guest = $customer->isGuest();
				$this->context->cookie->passwd = $customer->passwd;
				$this->context->cookie->email = $customer->email;

				// Add customer to the context
				$this->context->customer = $customer;

				if (Configuration::get('PS_CART_FOLLOWING') && (empty($this->context->cookie->id_cart) || Cart::getNbProducts($this->context->cookie->id_cart) == 0) && $id_cart = (int)Cart::lastNoneOrderedCart($this->context->customer->id))
					$this->context->cart = new Cart($id_cart);
				else
				{
					$this->context->cart->id_carrier = 0;
					$this->context->cart->setDeliveryOption(null);
					$this->context->cart->id_address_delivery = Address::getFirstCustomerAddressId((int)($customer->id));
					$this->context->cart->id_address_invoice = Address::getFirstCustomerAddressId((int)($customer->id));
				}
				$this->context->cart->id_customer = (int)$customer->id;
				$this->context->cart->secure_key = $customer->secure_key;
				$this->context->cart->save();
				$this->context->cookie->id_cart = (int)$this->context->cart->id;
				$this->context->cookie->write();
				$this->context->cart->autosetProductAddress();

				Hook::exec('actionAuthentication');

				// Login information have changed, so we check if the cart rules still apply
				CartRule::autoRemoveFromCart($this->context);
				CartRule::autoAddToCart($this->context);

				if (!$this->ajax)
				{
					if (($back = Tools::getValue('back')))// && $back == Tools::secureReferrer($back)
					{
						header('Location: '.$back.'');
						exit;
						//Tools::redirect(html_entity_decode($back));
					}

					else

					//Tools::redirect('index.php?controller='.(($this->authRedirection !== false) ? urlencode($this->authRedirection) : 'store'));
					Tools::redirect(__PS_BASE_URI__.'index.php?controller=login'.($this->authRedirection ? '&back='.$this->authRedirection : ''));
				}
			}
		}
		if ($this->ajax)
		{
			$return = array(
				'hasError' => !empty($this->errors),
				'errors' => $this->errors,
				'token' => Tools::getToken(false)
			);
			die(Tools::jsonEncode($return));
		}
		else
			$this->context->smarty->assign('authentification_error', $this->errors);
	}



}
