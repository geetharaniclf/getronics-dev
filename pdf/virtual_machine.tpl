
          
		
		  <div id="exportable">
	<div class="table-responsive" >
		<table class="table"  cellpadding="0" cellspacing="0">
			<thead>
                <tr>
                  <th width="2%">
				  <input  id="selectAllRecords"  class="ng-pristine ng-valid" type="checkbox"></th>
                  <th width="20%">Name</th>
                  <th width="20%">Service Name</th>
                  <th width="10%">Status</th>
                  <th width="10%">Private IP</th>
                  <th width="10%">Public IP</th>
                  <th width="10%">Type</th>
                  <th width="18%">Provider</th>                  
                </tr>
              </thead>  
			  <tbody>
                             {if !empty($rowsData)}
                              
                              {foreach $rowsData as $rowData}
                            <tr>
                  <td><input id="virtual_machine" type="checkbox"></td>
                  <td><a href="#" class="ng-binding">{$rowData['name']}</a></td>
                  <td >{$rowData['service_name']}</td>
                  {if $rowData['state']=='Stopping'}
				  <td ng-switch-when="Stopping" id="vm-IBMSLVM-1" class="ng-scope">
                    <span id="spinner_IBMSLVM-1">
                      <img src="{$img_dir}images/select2-spinner.gif" class="ajax-loader">
                    </span>&nbsp;&nbsp;Stopping
                  </td>
                  {/if}
                  
                  {if $rowData['state']=='Running'}
				  <td ng-switch-when="Running" id="vm-IBMSLVM-1" class="ng-scope">
                    <span id="spinner_IBMSLVM-1">
                      <i class="fa fa-circle" aria-hidden="true" style="color:#65d0a0"></i>
                      
                    </span>&nbsp;&nbsp;Running
                  </td>
                  {/if}
                  {if $rowData['state']=='Installed'}
				  <td ng-switch-when="Installed" id="vm-IBMSLVM-1" class="ng-scope">
                    <span id="spinner_IBMSLVM-1">
                      <i class="fa fa-circle" aria-hidden="true" style="color:#65d0a0"></i>
                    </span>&nbsp;&nbsp;Installed
                  </td>
                  {/if}
                  {if $rowData['state']=='Deleted'}
				  <td ng-switch-when="Deleted" id="vm-IBMSLVM-1" class="ng-scope">
                    <span id="spinner_IBMSLVM-1">
                      <i class="fa fa-circle" aria-hidden="true"></i>
                    </span>&nbsp;&nbsp;Deleted
                  </td>
                  {/if}
                  {if $rowData['state']=='ShuttingDown'}
				  <td ng-switch-when="ShuttingDown" id="vm-IBMSLVM-1" class="ng-scope">
                    <span id="spinner_IBMSLVM-1">
                      <i class="fa fa-circle" aria-hidden="true"></i>
                    </span>&nbsp;&nbsp;ShuttingDown
                  </td>
                  {/if}
                  
                  <td >{$rowData['private_ip']}</td>
                  <td >{$rowData['public_ip']}</td>
                  <td >{$rowData['type']}</td>
                  <td >{$rowData['provider']}</td>
                  
                </tr>      
                                  {/foreach}
                                     {/if}
                
              </tbody>				  
		</table>
	</div>

	
	
	</div>
		  
		  
		  
	