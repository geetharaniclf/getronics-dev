    
	<div id="columns" class="container">
	<div class="row">
             
<div id="left_column" class="column col-xs-12 col-sm-3">
	{include file="{$one_var}control_panel/sidebar.tpl"}
</div>

<div id="center_column" class="center_column col-xs-12 col-sm-9">
		
	<div class="billing-main">
		<div class="row">
		<div class="calendar-main">
    <div class="container">
      <div class="col-sm-12">
        <div class="form-group">
          
          <div class="row db-boxes">
            <div class="text-left head-text ondetail-view cal-text">

            </div>
          </div>
        </div>
      </div>
     
    </div>

	</div>
  </div>
	  
	  <div class="row">
    <div class="billing-main">
      <div class="col-lg-4">
        <div class="mini-box">
          <div class="box-title">
            <p>Billing Summary</p>
          </div>          
          <ul>
            <li><span class="amazon"> </span> Getronics</li>
            <li><span class="azure"> </span> Azure Cloud</li>
            <li><span class="o365"> </span> Digital Desktop</li>
          </ul>
          <div class="chart-container">
				<img src="{$img_dir}images/billing-circle.jpg" alt="">
		  </div>
        </div>
      </div>
      <div class="col-lg-8">
        <div class="mini-box">
          <div class="col-lg-12">
            <div class="box-title">
              <p>Billing Trend</p>
            </div>
            <div class="chart-container">
				<img src="{$img_dir}images/graph.jpg" alt="" style="padding:12px;">
			
			</div>
          </div>
        </div>
      </div>
    </div>
  </div>
	  
	
		<div class="row">
    <div class="three-box-main">
      <div class="col-lg-4">
        <div class="mini-box-bg-2">
          <div class="box-title">
            <p>Getronics</p>
          </div>
          <div class="col-lg-12">
            <div class="box-info">
              <p class="size-h1 text-center ng-binding" id="total_price_amount" style="display:block;color:#000">$3275.00</p>
            </div>
          </div>
          <div class="billing-details">            
            <div class="clearfix">&nbsp;</div>
            <div class="col-lg-12 text-center">
              <a ng-click="ShowAmazoneDetails();" class="btn btn-w-md btn-gap-v btn-dark btn-blue">Details</a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4">
        <div class="mini-box-bg-2">
          <div class="box-title">
            <p>Azure Cloud</p>
          </div>
          <div class="col-lg-12">
            <div class="box-info">
              <p class="size-h1 text-center ng-binding" id="total_price_amount" style="display:block;color:#000">$2147.39</p>
            </div>
          </div>
          <div class="billing-details">            
            <div class="clearfix">&nbsp;</div>
            <div class="col-lg-12 text-center">
              <a ng-click="ShowAzureDetails();" class="btn btn-w-md btn-gap-v btn-dark btn-blue">Details</a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4">
        <div class="mini-box-bg-2">
          <div class="box-title">
            <p>Digital Desktops</p>
          </div>
          <div class="col-lg-12">
            <div class="box-info">
              <p class="size-h1 text-center ng-binding" id="total_price_amount" style="display:block;color:#000">$12417.96</p>
            </div>
          </div>
          <div class="billing-details">            
            <div class="clearfix">&nbsp;</div>
            <div class="col-lg-12 text-center">
              <a ng-click="ShowO365Details();" class="btn btn-w-md btn-gap-v btn-dark btn-blue">Details</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
	
	
	<div class="table-responsive ng-scope">
             <div class="col-sm-2 col-xs-1" style="float:left;padding-top: 3px; width:100px;">
              <a href="#" onclick="">
				<i class="fa fa-file-excel-o" aria-hidden="true"></i>
			  </a>
              <a href="#" onclick="">
                <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
				</a>
            </div>
            <div class="col-md-3" style="padding-bottom:15px;">
              <input class="form-control ng-pristine" ng-model="search" placeholder="Search" type="text">
            </div>
			<div id="exportable">
            <table class="circuit-table" cellspacing="0" cellpadding="0" border="0" width="100%">
              <thead>
                <tr>
                  <td width="15%">Customer ID</td>
                  <td width="20%">Customer Comapany Name</td>
                  <td width="20%">Service Name</td>
                  <td width="15%">Post Tax Effective Rate</td>
                </tr>
              </thead>
              <tbody>
                <tr  >
                  <td  >ID_12345</td>
                  <td  >GETRONICS_CUST</td>
                  <td  >STORAGE</td>
                  <td  >$123.51</td>
                </tr>
				<tr  >
                  <td  >ID_34345</td>
                  <td  >GETRONICS_CUST</td>
                  <td  >VIRTUAL MACHINES</td>
                  <td  >$340.00</td>
                </tr>
				<tr  >
                  <td  >ID_54654</td>
                  <td  >GETRONICS_CUST</td>
                  <td  >VIRTUAL MACHINES</td>
                  <td  >$423.00</td>
                </tr>
				<tr  >
                  <td  >ID_82353</td>
                  <td  >GETRONICS_CUST</td>
                  <td  >DATA MANAGEMENT</td>
                  <td  >$50.00</td>
                </tr>
				<tr  >
                  <td  >ID_12345</td>
                  <td  >GETRONICS_CUST</td>
                  <td  >VIRTUAL MACHINES</td>
                  <td  >$70.00</td>
                </tr> <tr  >
                  <td  >ID_12345</td>
                  <td  >GETRONICS_CUST</td>
                  <td  >DATA MANAGEMENT</td>
                  <td  >$90.00</td>
                </tr>
				<tr  >
                  <td  >ID_12345</td>
                  <td  >GETRONICS_CUST</td>
                  <td  >DATA MANAGEMENT</td>
                  <td  >$30.00</td>
                </tr>
				<tr  >
                  <td  >ID_12345</td>
                  <td  >GETRONICS_CUST</td>
                  <td  >VIRTUAL MACHINES</td>
                  <td  >$70.09</td>
                </tr>
				<tr  >
                  <td  >ID_12345</td>
                  <td  >GETRONICS_CUST</td>
                  <td  >STORAGE</td>
                  <td  >$0.00</td>
                </tr>
				<tr >
                  <td  >ID_12345</td>
                  <td  >GETRONICS_CUST</td>
                  <td  >DATA MANAGEMENT</td>
                  <td  >$20.00</td>
                </tr>
              </tbody>
            </table>
			<div class="col-lg-12 text-right">
	
	
	<ul class="pagination-sm pagination " >
  <li class=""><a href="" class="ng-binding">First</a></li>
  <li><a href="">Previous</a></li>
 <li class="active"><a href="">1</a></li>
 <li class=""><a href="">2</a></li>
 <li class=""><a href="">3</a></li>
  <li><a href="">Next</a></li>
<li><a href="">Last</a></li>
</ul>
	
	
	</div>
			</div>
          </div>
	
	
	
	</div>
	  
    </div>
 
 </div>
		
	
	</div>
	
	
	</div>
	</div>
	
	
	
	
