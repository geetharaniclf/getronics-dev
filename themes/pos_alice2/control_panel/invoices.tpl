
	
	<script type='text/javascript'>
//<![CDATA[
$(window).load(function(){
$(document).ready(function() {
    $(".tabs-menu a").click(function(event) {
        event.preventDefault();
        $(this).parent().addClass("current");
        $(this).parent().siblings().removeClass("current");
        var tab = $(this).attr("href");
        $(".tab-content").not(tab).css("display", "none");
       $(".inner-tab-content").not(tab).css("display", "none");
		$(".listing_user").css('display', 'block');
        <!-- $(".tab-content").firstChild('div.nav-wrapper').next(div).attr("display", "block"); -->
        <!-- $(this).div('.nav-wrapper').next.css("display", "block"); -->
		
        $(tab).fadeIn();
    });

    $(".inner-tabs-menu a").click(function(event) {
        event.preventDefault();
        $(this).parent().addClass("current_1");
        $(this).parent().siblings().removeClass("current_1");
        var tab = $(this).attr("href");
       $(".inner-tab-content").not(tab).css("display", "none");
        $(".listing_user").css('display', 'none');
        $(tab).fadeIn();
    });
});
});



</script>

	<div id="columns" class="container">
	<div class="row">
             
<div id="left_column" class="column col-xs-12 col-sm-3">
	{include file="{$one_var}control_panel/sidebar.tpl"}
</div>

<div id="center_column" class="center_column col-xs-12 col-sm-9">
		
		
    <div class="row">
        <div class="col-xs-12 col-lg-12 themed-nav">
						 <div class="box-title">
                              <p>Invoices - SAP</p>
                            </div>
                          <div class="mini-box">

                            <div class="panel-header">
                             <aside data-ng-include=" 'views/virtual_machines/virtual_machine_nav.html' " id="nav-container" class="nav-container ng-scope nav-fixed nav-horizontal nav-vertical" data-ng-class="">

                            <div class="nav-wrapper">
                                <ul id="nav" class="nav ng-scope" data-ng-controller="NavCtrl" data-collapse-nav="" data-highlight-active="">
                                    <li class="">

                                        <a href="#"><i class="fa fa-plus"></i><span>New</span></a>
                                    </li>

                                    <li>
                                        <a href="#" ><i class="fa fa-trash-o"></i><span>Delete</span></a>


                                    </li>

                                    <li>
                                        <a href="#" >
                                            <i class="fa fa-file-excel-o" aria-hidden="true"></i>

                                            <span data-i18n="Generate Excel">Generate Excel</span> 
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" >
                                            <i class="fa fa-file-pdf-o" aria-hidden="true"></i>

                                            <span data-i18n="Generate PDF">Generate PDF</span> 
                                        </a>
                                    </li>
                                </ul>
                            </div>    



                      </aside>

                            </div>
                            <div class="divider divider"></div>
                              
                              
                              
        <div class="sub-user-lice">
            <div class="col-lg-12">
				
                <section class="tabs-container-main">
				<div id="tabs-container">
				    <ul class="tabs-menu nav-pills menu_class">
                        <li class="current"><a href="#tab-1" >All</a> </li>
                        <li><a href="#tab-2" >Unpaid</a></li>
                        <li><a href="#tab-3" >Paid</a></li>
                        <li><a href="#tab-4" >Draft</a></li>
                        <li><a href="#tab-5" >Credit Notes</a></li>
                        
                    </ul>
               
				
				
                <!---- First Tab Starts ---->
                <div id="tab-1" class="tab-content">
                    <div class="col-xsm-12 col-lg-12">
                        
                            <div class="row">
                                <div class="panel-body" >
                                    <div class="table-responsive dashboard-table col-md-12">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <input ng-model="selectAll" ng-click="oval.isAll()" id="checkedAllSubscription" class="ng-pristine ng-valid" type="checkbox">
                                                    </th>
                                                        <th>Invoice #</th>
                                                        <th style="">To</th>
                                                        <th style=" width: 0.5%">Summary</th>
                                                        <th style="">Date</th>
                                                        <th style="">Date Due</th>
                                                        <th style="">Origional</th>
                                                        <th style="width: 1%;">Type</th>
                                                        <th style="">Status</th>
                                                        <th style="width: 5%;">Options</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <input ng-model="sub.selected" ng-checked="selectAll" ng-click="selectedSubscription(sub)" id="subscription_83AE0045-E0A6-49A5-B123-1652FFE1D9F1" class="ng-pristine ng-valid" type="checkbox">
                                                        </td>
                                                        <td>
                                                            <p>INV0000362</p>
                                                        </td>
                                                        <td>
                                                            <p>Barclays</p>
                                                        </td>
                                                        <td>
                                                            <p>Interest Income</p>
                                                        </td>
                                                        <td>
                                                            <p>06 Feb 17</p>
                                                        </td>
                                                        <td>
                                                            <p>06 May 17</p>
                                                        </td>
                                                        <td>
                                                            <p>161.68</p>
                                                        </td>
                                                        <td>
                                                            <p>paid</p>
                                                        </td>
                                                        <td>
                                                            <img src="{$img_dir}images/envelope.png" class="envelope-img">
                                                            <img src="{$img_dir}images/cloud.png" class="cloud-img">

                                                        </td>
                                                        <td>
                                                            <div class="dropdown">
                                                              Manage<span class="caret">
                                                              <div class="dropdown-content">
                                                                <a href="#">Attachment</a>
                                                                <a href="#">Make a Copy</a>
                                                                <a href="#">Edit</a>
                                                                <a href="#">Email</a>
                                                                <a href="#">HTML</a>
                                                                <a href="#">PDF</a>
                                                              </div>
                                                            </div>
                                                        </td>
                                                        
                                                    </tr>
										            <tr>
                                                        <td>
                                                            <input ng-model="sub.selected" ng-checked="selectAll" ng-click="selectedSubscription(sub)" id="subscription_83AE0045-E0A6-49A5-B123-1652FFE1D9F1" class="ng-pristine ng-valid" type="checkbox">
                                                        </td>
                                                        <td>
                                                            <p>INV0004582</p>
                                                        </td>
                                                        <td>
                                                            <p>Commision 4u limited</p>
                                                        </td>
                                                        <td>
                                                            <p>May Commision</p>
                                                        </td>
                                                        <td>
                                                            <p>09 MArch 17</p>
                                                        </td>
                                                        <td>
                                                            <p>22 June 17</p>
                                                        </td>
                                                        <td>
                                                            <p>7951.68</p>
                                                        </td>
                                                        <td>
                                                            <p>paid</p>
                                                        </td>
                                                        <td>
                                                            <img src="{$img_dir}images/envelope.png" class="envelope-img">
                                                            <img src="{$img_dir}images/cloud.png" class="cloud-img">

                                                        </td>
                                                        <td>
                                                            <div class="dropdown">
                                                              Manage<span class="caret">
                                                              <div class="dropdown-content">
                                                                <a href="#">Attachment</a>
                                                                <a href="#">Make a Copy</a>
                                                                <a href="#">Edit</a>
                                                                <a href="#">Email</a>
                                                                <a href="#">HTML</a>
                                                                <a href="#">PDF</a>
                                                              </div>
                                                            </div>
                                                        </td>
                                                        
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input ng-model="sub.selected" ng-checked="selectAll" ng-click="selectedSubscription(sub)" id="subscription_83AE0045-E0A6-49A5-B123-1652FFE1D9F1" class="ng-pristine ng-valid" type="checkbox">
                                                        </td>
                                                        <td>
                                                            <p>INV0007945</p>
                                                        </td>
                                                        <td>
                                                            <p>Subscriptions R Us</p>
                                                        </td>
                                                        <td>
                                                            <p>Monthly Retainer</p>
                                                        </td>
                                                        <td>
                                                            <p>15 Feb 17</p>
                                                        </td>
                                                        <td>
                                                            <p>08 July 17</p>
                                                        </td>
                                                        <td>
                                                            <p>1542.36</p>
                                                        </td>
                                                        <td>
                                                            <p>paid</p>
                                                        </td>
                                                        <td>
                                                            <img src="{$img_dir}images/envelope.png" class="envelope-img">
                                                            <img src="{$img_dir}images/cloud.png" class="cloud-img">

                                                        </td>
                                                        <td>
                                                            <div class="dropdown">
                                                              Manage<span class="caret">
                                                              <div class="dropdown-content">
                                                                <a href="#">Attachment</a>
                                                                <a href="#">Make a Copy</a>
                                                                <a href="#">Edit</a>
                                                                <a href="#">Email</a>
                                                                <a href="#">HTML</a>
                                                                <a href="#">PDF</a>
                                                              </div>
                                                            </div>
                                                        </td>
                                                        
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input ng-model="sub.selected" ng-checked="selectAll" ng-click="selectedSubscription(sub)" id="subscription_83AE0045-E0A6-49A5-B123-1652FFE1D9F1" class="ng-pristine ng-valid" type="checkbox">
                                                        </td>
                                                        <td>
                                                            <p>INV0003625</p>
                                                        </td>
                                                        <td>
                                                            <p>Advertising Junction</p>
                                                        </td>
                                                        <td>
                                                            <p>Quarterly Fixed</p>
                                                        </td>
                                                        <td>
                                                            <p>11 Feb 17</p>
                                                        </td>
                                                        <td>
                                                            <p>13 May 17</p>
                                                        </td>
                                                        <td>
                                                            <p>1246.32</p>
                                                        </td>
                                                        <td>
                                                            <p>paid</p>
                                                        </td>
                                                        <td>
                                                            <img src="{$img_dir}images/envelope.png" class="envelope-img">
                                                            <img src="{$img_dir}images/cloud.png" class="cloud-img">

                                                        </td>
                                                        <td>
                                                            <div class="dropdown">
                                                              Manage<span class="caret">
                                                              <div class="dropdown-content">
                                                                <a href="#">Attachment</a>
                                                                <a href="#">Make a Copy</a>
                                                                <a href="#">Edit</a>
                                                                <a href="#">Email</a>
                                                                <a href="#">HTML</a>
                                                                <a href="#">PDF</a>
                                                              </div>
                                                            </div>
                                                        </td>
                                                        
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input ng-model="sub.selected" ng-checked="selectAll" ng-click="selectedSubscription(sub)" id="subscription_83AE0045-E0A6-49A5-B123-1652FFE1D9F1" class="ng-pristine ng-valid" type="checkbox">
                                                        </td>
                                                        <td>
                                                            <p>INV0004855</p>
                                                        </td>
                                                        <td>
                                                            <p>WPYS Limited</p>
                                                        </td>
                                                        <td>
                                                            <p>Monthly Subscription</p>
                                                        </td>
                                                        <td>
                                                            <p>01 Feb 17</p>
                                                        </td>
                                                        <td>
                                                            <p>01 May 17</p>
                                                        </td>
                                                        <td>
                                                            <p>699.99</p>
                                                        </td>
                                                        <td>
                                                            <p>paid</p>
                                                        </td>
                                                        <td>
                                                            <img src="{$img_dir}images/envelope.png" class="envelope-img">
                                                            <img src="{$img_dir}images/cloud.png" class="cloud-img">

                                                        </td>
                                                        <td>
                                                            <div class="dropdown">
                                                              Manage<span class="caret">
                                                              <div class="dropdown-content">
                                                                <a href="#">Attachment</a>
                                                                <a href="#">Make a Copy</a>
                                                                <a href="#">Edit</a>
                                                                <a href="#">Email</a>
                                                                <a href="#">HTML</a>
                                                                <a href="#">PDF</a>
                                                              </div>
                                                            </div>
                                                        </td>
                                                        
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input ng-model="sub.selected" ng-checked="selectAll" ng-click="selectedSubscription(sub)" id="subscription_83AE0045-E0A6-49A5-B123-1652FFE1D9F1" class="ng-pristine ng-valid" type="checkbox">
                                                        </td>
                                                        <td>
                                                            <p>INV0004466</p>
                                                        </td>
                                                        <td>
                                                            <p>Yellow Pages</p>
                                                        </td>
                                                        <td>
                                                            <p>Advertising Revenue</p>
                                                        </td>
                                                        <td>
                                                            <p>01 Feb 17</p>
                                                        </td>
                                                        <td>
                                                            <p>01 March 17</p>
                                                        </td>
                                                        <td>
                                                            <p>755.66</p>
                                                        </td>
                                                        <td>
                                                            <p>paid</p>
                                                        </td>
                                                        <td>
                                                            <img src="{$img_dir}images/envelope.png" class="envelope-img">
                                                            <img src="{$img_dir}images/cloud.png" class="cloud-img">

                                                        </td>
                                                        <td>
                                                            <div class="dropdown">
                                                              Manage<span class="caret">
                                                              <div class="dropdown-content">
                                                                <a href="#">Attachment</a>
                                                                <a href="#">Make a Copy</a>
                                                                <a href="#">Edit</a>
                                                                <a href="#">Email</a>
                                                                <a href="#">HTML</a>
                                                                <a href="#">PDF</a>
                                                              </div>
                                                            </div>
                                                        </td>
                                                        
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input ng-model="sub.selected" ng-checked="selectAll" ng-click="selectedSubscription(sub)" id="subscription_83AE0045-E0A6-49A5-B123-1652FFE1D9F1" class="ng-pristine ng-valid" type="checkbox">
                                                        </td>
                                                        <td>
                                                            <p>INV004875</p>
                                                        </td>
                                                        <td>
                                                            <p>Freedom Salvage</p>
                                                        </td>
                                                        <td>
                                                            <p>Commision</p>
                                                        </td>
                                                        <td>
                                                            <p>12 Jan 17</p>
                                                        </td>
                                                        <td>
                                                            <p>22 July 17</p>
                                                        </td>
                                                        <td>
                                                            <p>55201.36</p>
                                                        </td>
                                                        <td>
                                                            <p>paid</p>
                                                        </td>
                                                        <td>
                                                            <img src="{$img_dir}images/envelope.png" class="envelope-img">
                                                            <img src="{$img_dir}images/cloud.png" class="cloud-img">

                                                        </td>
                                                        <td>
                                                            <div class="dropdown">
                                                              Manage<span class="caret">
                                                              <div class="dropdown-content">
                                                                <a href="#">Attachment</a>
                                                                <a href="#">Make a Copy</a>
                                                                <a href="#">Edit</a>
                                                                <a href="#">Email</a>
                                                                <a href="#">HTML</a>
                                                                <a href="#">PDF</a>
                                                              </div>
                                                            </div>
                                                        </td>
                                                        <tr>
                                                        <td>
                                                            <input ng-model="sub.selected" ng-checked="selectAll" ng-click="selectedSubscription(sub)" id="subscription_83AE0045-E0A6-49A5-B123-1652FFE1D9F1" class="ng-pristine ng-valid" type="checkbox">
                                                        </td>
                                                        <td>
                                                            <p>INV0001452</p>
                                                        </td>
                                                        <td>
                                                            <p>Easy Subscription</p>
                                                        </td>
                                                        <td>
                                                            <p>Interest Expense</p>
                                                        </td>
                                                        <td>
                                                            <p>14 Feb 17</p>
                                                        </td>
                                                        <td>
                                                            <p>30 May 17</p>
                                                        </td>
                                                        <td>
                                                            <p>45871.68</p>
                                                        </td>
                                                        <td>
                                                            <p>unpaid</p>
                                                        </td>
                                                        <td>
                                                            <img src="{$img_dir}images/envelope.png" class="envelope-img">
                                                            <img src="{$img_dir}images/cloud.png" class="cloud-img">

                                                        </td>
                                                        <td>
                                                            <div class="dropdown">
                                                              Manage<span class="caret">
                                                              <div class="dropdown-content">
                                                                <a href="#">Attachment</a>
                                                                <a href="#">Make a Copy</a>
                                                                <a href="#">Edit</a>
                                                                <a href="#">Email</a>
                                                                <a href="#">HTML</a>
                                                                <a href="#">PDF</a>
                                                              </div>
                                                            </div>
                                                        </td>
                                                        
                                                    </tr>
										            <tr>
                                                        <td>
                                                            <input ng-model="sub.selected" ng-checked="selectAll" ng-click="selectedSubscription(sub)" id="subscription_83AE0045-E0A6-49A5-B123-1652FFE1D9F1" class="ng-pristine ng-valid" type="checkbox">
                                                        </td>
                                                        <td>
                                                            <p>INV0004112</p>
                                                        </td>
                                                        <td>
                                                            <p>Alpine Unlimited</p>
                                                        </td>
                                                        <td>
                                                            <p>Monthly Features</p>
                                                        </td>
                                                        <td>
                                                            <p>06 March 17</p>
                                                        </td>
                                                        <td>
                                                            <p>11 June 17</p>
                                                        </td>
                                                        <td>
                                                            <p>78551.68</p>
                                                        </td>
                                                        <td>
                                                            <p>unpaid</p>
                                                        </td>
                                                        <td>
                                                            <img src="{$img_dir}images/envelope.png" class="envelope-img">
                                                            <img src="{$img_dir}images/cloud.png" class="cloud-img">

                                                        </td>
                                                        <td>
                                                            <div class="dropdown">
                                                              Manage<span class="caret">
                                                              <div class="dropdown-content">
                                                                <a href="#">Attachment</a>
                                                                <a href="#">Make a Copy</a>
                                                                <a href="#">Edit</a>
                                                                <a href="#">Email</a>
                                                                <a href="#">HTML</a>
                                                                <a href="#">PDF</a>
                                                              </div>
                                                            </div>
                                                        </td>
                                                        
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input ng-model="sub.selected" ng-checked="selectAll" ng-click="selectedSubscription(sub)" id="subscription_83AE0045-E0A6-49A5-B123-1652FFE1D9F1" class="ng-pristine ng-valid" type="checkbox">
                                                        </td>
                                                        <td>
                                                            <p>INV0003365</p>
                                                        </td>
                                                        <td>
                                                            <p>Gold Subscription</p>
                                                        </td>
                                                        <td>
                                                            <p>Monthly Retainer</p>
                                                        </td>
                                                        <td>
                                                            <p>15 Feb 17</p>
                                                        </td>
                                                        <td>
                                                            <p>15 July 17</p>
                                                        </td>
                                                        <td>
                                                            <p>8842.36</p>
                                                        </td>
                                                        <td>
                                                            <p>unpaid</p>
                                                        </td>
                                                        <td>
                                                            <img src="{$img_dir}images/envelope.png" class="envelope-img">
                                                            <img src="{$img_dir}images/cloud.png" class="cloud-img">

                                                        </td>
                                                        <td>
                                                            <div class="dropdown">
                                                              Manage<span class="caret">
                                                              <div class="dropdown-content">
                                                                <a href="#">Attachment</a>
                                                                <a href="#">Make a Copy</a>
                                                                <a href="#">Edit</a>
                                                                <a href="#">Email</a>
                                                                <a href="#">HTML</a>
                                                                <a href="#">PDF</a>
                                                              </div>
                                                            </div>
                                                        </td>
                                                        
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input ng-model="sub.selected" ng-checked="selectAll" ng-click="selectedSubscription(sub)" id="subscription_83AE0045-E0A6-49A5-B123-1652FFE1D9F1" class="ng-pristine ng-valid" type="checkbox">
                                                        </td>
                                                        <td>
                                                            <p>INV0003625</p>
                                                        </td>
                                                        <td>
                                                            <p>Junction Sales</p>
                                                        </td>
                                                        <td>
                                                            <p>Quarterly Fixed</p>
                                                        </td>
                                                        <td>
                                                            <p>11 Feb 17</p>
                                                        </td>
                                                        <td>
                                                            <p>13 May 17</p>
                                                        </td>
                                                        <td>
                                                            <p>12426.32</p>
                                                        </td>
                                                        <td>
                                                            <p>unpaid</p>
                                                        </td>
                                                        <td>
                                                            <img src="{$img_dir}images/envelope.png" class="envelope-img">
                                                            <img src="{$img_dir}images/cloud.png" class="cloud-img">

                                                        </td>
                                                        <td>
                                                            <div class="dropdown">
                                                              Manage<span class="caret">
                                                              <div class="dropdown-content">
                                                                <a href="#">Attachment</a>
                                                                <a href="#">Make a Copy</a>
                                                                <a href="#">Edit</a>
                                                                <a href="#">Email</a>
                                                                <a href="#">HTML</a>
                                                                <a href="#">PDF</a>
                                                              </div>
                                                            </div>
                                                        </td>
                                                        
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input ng-model="sub.selected" ng-checked="selectAll" ng-click="selectedSubscription(sub)" id="subscription_83AE0045-E0A6-49A5-B123-1652FFE1D9F1" class="ng-pristine ng-valid" type="checkbox">
                                                        </td>
                                                        <td>
                                                            <p>INV0004222</p>
                                                        </td>
                                                        <td>
                                                            <p>NYYA Financial</p>
                                                        </td>
                                                        <td>
                                                            <p>Monthly Subscription</p>
                                                        </td>
                                                        <td>
                                                            <p>01 Feb 17</p>
                                                        </td>
                                                        <td>
                                                            <p>01 May 17</p>
                                                        </td>
                                                        <td>
                                                            <p>7856.35</p>
                                                        </td>
                                                        <td>
                                                            <p>unpaid</p>
                                                        </td>
                                                        <td>
                                                            <img src="{$img_dir}images/envelope.png" class="envelope-img">
                                                            <img src="{$img_dir}images/cloud.png" class="cloud-img">

                                                        </td>
                                                        <td>
                                                            <div class="dropdown">
                                                              Manage<span class="caret">
                                                              <div class="dropdown-content">
                                                                <a href="#">Attachment</a>
                                                                <a href="#">Make a Copy</a>
                                                                <a href="#">Edit</a>
                                                                <a href="#">Email</a>
                                                                <a href="#">HTML</a>
                                                                <a href="#">PDF</a>
                                                              </div>
                                                            </div>
                                                        </td>
                                                        
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input ng-model="sub.selected" ng-checked="selectAll" ng-click="selectedSubscription(sub)" id="subscription_83AE0045-E0A6-49A5-B123-1652FFE1D9F1" class="ng-pristine ng-valid" type="checkbox">
                                                        </td>
                                                        <td>
                                                            <p>INV0005587</p>
                                                        </td>
                                                        <td>
                                                            <p>Red Zone</p>
                                                        </td>
                                                        <td>
                                                            <p>Subscriber</p>
                                                        </td>
                                                        <td>
                                                            <p>01 Feb 17</p>
                                                        </td>
                                                        <td>
                                                            <p>01 March 17</p>
                                                        </td>
                                                        <td>
                                                            <p>99.99</p>
                                                        </td>
                                                        <td>
                                                            <p>unpaid</p>
                                                        </td>
                                                        <td>
                                                            <img src="{$img_dir}images/envelope.png" class="envelope-img">
                                                            <img src="{$img_dir}images/cloud.png" class="cloud-img">

                                                        </td>
                                                        <td>
                                                            <div class="dropdown">
                                                              Manage<span class="caret">
                                                              <div class="dropdown-content">
                                                                <a href="#">Attachment</a>
                                                                <a href="#">Make a Copy</a>
                                                                <a href="#">Edit</a>
                                                                <a href="#">Email</a>
                                                                <a href="#">HTML</a>
                                                                <a href="#">PDF</a>
                                                              </div>
                                                            </div>
                                                        </td>
                                                        
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input ng-model="sub.selected" ng-checked="selectAll" ng-click="selectedSubscription(sub)" id="subscription_83AE0045-E0A6-49A5-B123-1652FFE1D9F1" class="ng-pristine ng-valid" type="checkbox">
                                                        </td>
                                                        <td>
                                                            <p>INV006653</p>
                                                        </td>
                                                        <td>
                                                            <p>Salvage Sales</p>
                                                        </td>
                                                        <td>
                                                            <p>Implementation</p>
                                                        </td>
                                                        <td>
                                                            <p>12 May 17</p>
                                                        </td>
                                                        <td>
                                                            <p>22 Nov 17</p>
                                                        </td>
                                                        <td>
                                                            <p>4853.36</p>
                                                        </td>
                                                        <td>
                                                            <p>unpaid</p>
                                                        </td>
                                                        <td>
                                                            <img src="{$img_dir}images/envelope.png" class="envelope-img">
                                                            <img src="{$img_dir}images/cloud.png" class="cloud-img">

                                                        </td>
                                                        <td>
                                                            <div class="dropdown">
                                                              Manage<span class="caret">
                                                              <div class="dropdown-content">
                                                                <a href="#">Attachment</a>
                                                                <a href="#">Make a Copy</a>
                                                                <a href="#">Edit</a>
                                                                <a href="#">Email</a>
                                                                <a href="#">HTML</a>
                                                                <a href="#">PDF</a>
                                                              </div>
                                                            </div>
                                                        </td>
                                                        
                                                    
                                                        
                                                    </tr>
                                                    
													
													
													
													
													
                                        </tbody>
                                        </table>
                                    </div>
                                </div>
								
                        
					   </div>
                        </section>
                    
                </div>
        </div>
        <!---- First Tab Ends ---->
        <!---- Second Tab Starts ---->
        <div id="tab-2" class="tab-content">
            <div class="col-xsm-12 col-lg-12">
                
                    
                        <div class="listing_user" id="inner-tab-1" >
                           
                            <div class="row">
                                <div class="panel-body" >
                                    <div class="table-responsive dashboard-table col-md-12">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <input ng-model="selectAll" ng-click="oval.isAll()" id="checkedAllSubscription" class="ng-pristine ng-valid" type="checkbox">
                                                    </th>
                                                        <th>Invoice #</th>
                                                        <th style="">To</th>
                                                        <th style=" width: 0.5%">Summary</th>
                                                        <th style="">Date</th>
                                                        <th style="">Date Due</th>
                                                        <th style="">Origional</th>
                                                        <th style="width: 1%;">Type</th>
                                                        <th style="">Status</th>
                                                        <th style="width: 5%;">Options</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <input ng-model="sub.selected" ng-checked="selectAll" ng-click="selectedSubscription(sub)" id="subscription_83AE0045-E0A6-49A5-B123-1652FFE1D9F1" class="ng-pristine ng-valid" type="checkbox">
                                                        </td>
                                                        <td>
                                                            <p>INV0001452</p>
                                                        </td>
                                                        <td>
                                                            <p>Easy Subscription</p>
                                                        </td>
                                                        <td>
                                                            <p>Interest Expense</p>
                                                        </td>
                                                        <td>
                                                            <p>14 Feb 17</p>
                                                        </td>
                                                        <td>
                                                            <p>30 May 17</p>
                                                        </td>
                                                        <td>
                                                            <p>45871.68</p>
                                                        </td>
                                                        <td>
                                                            <p>unpaid</p>
                                                        </td>
                                                        <td>
                                                            <img src="{$img_dir}images/envelope.png" class="envelope-img">
                                                            <img src="{$img_dir}images/cloud.png" class="cloud-img">

                                                        </td>
                                                        <td>
                                                            <div class="dropdown">
                                                              Manage<span class="caret">
                                                              <div class="dropdown-content">
                                                                <a href="#">Attachment</a>
                                                                <a href="#">Make a Copy</a>
                                                                <a href="#">Edit</a>
                                                                <a href="#">Email</a>
                                                                <a href="#">HTML</a>
                                                                <a href="#">PDF</a>
                                                              </div>
                                                            </div>
                                                        </td>
                                                        
                                                    </tr>
										            <tr>
                                                        <td>
                                                            <input ng-model="sub.selected" ng-checked="selectAll" ng-click="selectedSubscription(sub)" id="subscription_83AE0045-E0A6-49A5-B123-1652FFE1D9F1" class="ng-pristine ng-valid" type="checkbox">
                                                        </td>
                                                        <td>
                                                            <p>INV0004112</p>
                                                        </td>
                                                        <td>
                                                            <p>Alpine Unlimited</p>
                                                        </td>
                                                        <td>
                                                            <p>Monthly Features</p>
                                                        </td>
                                                        <td>
                                                            <p>06 March 17</p>
                                                        </td>
                                                        <td>
                                                            <p>11 June 17</p>
                                                        </td>
                                                        <td>
                                                            <p>78551.68</p>
                                                        </td>
                                                        <td>
                                                            <p>unpaid</p>
                                                        </td>
                                                        <td>
                                                            <img src="{$img_dir}images/envelope.png" class="envelope-img">
                                                            <img src="{$img_dir}images/cloud.png" class="cloud-img">

                                                        </td>
                                                        <td>
                                                            <div class="dropdown">
                                                              Manage<span class="caret">
                                                              <div class="dropdown-content">
                                                                <a href="#">Attachment</a>
                                                                <a href="#">Make a Copy</a>
                                                                <a href="#">Edit</a>
                                                                <a href="#">Email</a>
                                                                <a href="#">HTML</a>
                                                                <a href="#">PDF</a>
                                                              </div>
                                                            </div>
                                                        </td>
                                                        
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input ng-model="sub.selected" ng-checked="selectAll" ng-click="selectedSubscription(sub)" id="subscription_83AE0045-E0A6-49A5-B123-1652FFE1D9F1" class="ng-pristine ng-valid" type="checkbox">
                                                        </td>
                                                        <td>
                                                            <p>INV0003365</p>
                                                        </td>
                                                        <td>
                                                            <p>Gold Subscription</p>
                                                        </td>
                                                        <td>
                                                            <p>Monthly Retainer</p>
                                                        </td>
                                                        <td>
                                                            <p>15 Feb 17</p>
                                                        </td>
                                                        <td>
                                                            <p>15 July 17</p>
                                                        </td>
                                                        <td>
                                                            <p>8842.36</p>
                                                        </td>
                                                        <td>
                                                            <p>unpaid</p>
                                                        </td>
                                                        <td>
                                                            <img src="{$img_dir}images/envelope.png" class="envelope-img">
                                                            <img src="{$img_dir}images/cloud.png" class="cloud-img">

                                                        </td>
                                                        <td>
                                                            <div class="dropdown">
                                                              Manage<span class="caret">
                                                              <div class="dropdown-content">
                                                                <a href="#">Attachment</a>
                                                                <a href="#">Make a Copy</a>
                                                                <a href="#">Edit</a>
                                                                <a href="#">Email</a>
                                                                <a href="#">HTML</a>
                                                                <a href="#">PDF</a>
                                                              </div>
                                                            </div>
                                                        </td>
                                                        
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input ng-model="sub.selected" ng-checked="selectAll" ng-click="selectedSubscription(sub)" id="subscription_83AE0045-E0A6-49A5-B123-1652FFE1D9F1" class="ng-pristine ng-valid" type="checkbox">
                                                        </td>
                                                        <td>
                                                            <p>INV0003625</p>
                                                        </td>
                                                        <td>
                                                            <p>Junction Sales</p>
                                                        </td>
                                                        <td>
                                                            <p>Quarterly Fixed</p>
                                                        </td>
                                                        <td>
                                                            <p>11 Feb 17</p>
                                                        </td>
                                                        <td>
                                                            <p>13 May 17</p>
                                                        </td>
                                                        <td>
                                                            <p>12426.32</p>
                                                        </td>
                                                        <td>
                                                            <p>unpaid</p>
                                                        </td>
                                                        <td>
                                                            <img src="{$img_dir}images/envelope.png" class="envelope-img">
                                                            <img src="{$img_dir}images/cloud.png" class="cloud-img">

                                                        </td>
                                                        <td>
                                                            <div class="dropdown">
                                                              Manage<span class="caret">
                                                              <div class="dropdown-content">
                                                                <a href="#">Attachment</a>
                                                                <a href="#">Make a Copy</a>
                                                                <a href="#">Edit</a>
                                                                <a href="#">Email</a>
                                                                <a href="#">HTML</a>
                                                                <a href="#">PDF</a>
                                                              </div>
                                                            </div>
                                                        </td>
                                                        
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input ng-model="sub.selected" ng-checked="selectAll" ng-click="selectedSubscription(sub)" id="subscription_83AE0045-E0A6-49A5-B123-1652FFE1D9F1" class="ng-pristine ng-valid" type="checkbox">
                                                        </td>
                                                        <td>
                                                            <p>INV0004222</p>
                                                        </td>
                                                        <td>
                                                            <p>NYYA Financial</p>
                                                        </td>
                                                        <td>
                                                            <p>Monthly Subscription</p>
                                                        </td>
                                                        <td>
                                                            <p>01 Feb 17</p>
                                                        </td>
                                                        <td>
                                                            <p>01 May 17</p>
                                                        </td>
                                                        <td>
                                                            <p>7856.35</p>
                                                        </td>
                                                        <td>
                                                            <p>unpaid</p>
                                                        </td>
                                                        <td>
                                                            <img src="{$img_dir}images/envelope.png" class="envelope-img">
                                                            <img src="{$img_dir}images/cloud.png" class="cloud-img">

                                                        </td>
                                                        <td>
                                                            <div class="dropdown">
                                                              Manage<span class="caret">
                                                              <div class="dropdown-content">
                                                                <a href="#">Attachment</a>
                                                                <a href="#">Make a Copy</a>
                                                                <a href="#">Edit</a>
                                                                <a href="#">Email</a>
                                                                <a href="#">HTML</a>
                                                                <a href="#">PDF</a>
                                                              </div>
                                                            </div>
                                                        </td>
                                                        
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input ng-model="sub.selected" ng-checked="selectAll" ng-click="selectedSubscription(sub)" id="subscription_83AE0045-E0A6-49A5-B123-1652FFE1D9F1" class="ng-pristine ng-valid" type="checkbox">
                                                        </td>
                                                        <td>
                                                            <p>INV0005587</p>
                                                        </td>
                                                        <td>
                                                            <p>Red Zone</p>
                                                        </td>
                                                        <td>
                                                            <p>Subscriber</p>
                                                        </td>
                                                        <td>
                                                            <p>01 Feb 17</p>
                                                        </td>
                                                        <td>
                                                            <p>01 March 17</p>
                                                        </td>
                                                        <td>
                                                            <p>99.99</p>
                                                        </td>
                                                        <td>
                                                            <p>unpaid</p>
                                                        </td>
                                                        <td>
                                                            <img src="{$img_dir}images/envelope.png" class="envelope-img">
                                                            <img src="{$img_dir}images/cloud.png" class="cloud-img">

                                                        </td>
                                                        <td>
                                                            <div class="dropdown">
                                                              Manage<span class="caret">
                                                              <div class="dropdown-content">
                                                                <a href="#">Attachment</a>
                                                                <a href="#">Make a Copy</a>
                                                                <a href="#">Edit</a>
                                                                <a href="#">Email</a>
                                                                <a href="#">HTML</a>
                                                                <a href="#">PDF</a>
                                                              </div>
                                                            </div>
                                                        </td>
                                                        
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input ng-model="sub.selected" ng-checked="selectAll" ng-click="selectedSubscription(sub)" id="subscription_83AE0045-E0A6-49A5-B123-1652FFE1D9F1" class="ng-pristine ng-valid" type="checkbox">
                                                        </td>
                                                        <td>
                                                            <p>INV006653</p>
                                                        </td>
                                                        <td>
                                                            <p>Salvage Sales</p>
                                                        </td>
                                                        <td>
                                                            <p>Implementation</p>
                                                        </td>
                                                        <td>
                                                            <p>12 May 17</p>
                                                        </td>
                                                        <td>
                                                            <p>22 Nov 17</p>
                                                        </td>
                                                        <td>
                                                            <p>4853.36</p>
                                                        </td>
                                                        <td>
                                                            <p>unpaid</p>
                                                        </td>
                                                        <td>
                                                            <img src="{$img_dir}images/envelope.png" class="envelope-img">
                                                            <img src="{$img_dir}images/cloud.png" class="cloud-img">

                                                        </td>
                                                        <td>
                                                            <div class="dropdown">
                                                              Manage<span class="caret">
                                                              <div class="dropdown-content">
                                                                <a href="#">Attachment</a>
                                                                <a href="#">Make a Copy</a>
                                                                <a href="#">Edit</a>
                                                                <a href="#">Email</a>
                                                                <a href="#">HTML</a>
                                                                <a href="#">PDF</a>
                                                              </div>
                                                            </div>
                                                        </td>
                                                        
                                                    </tr>
                                                    
													
													
													
													
													
                                        </tbody>
                                        </table>
                                    </div>
                                </div>
								
                        
					   </div>
                        </div>
                        
                        
						
   
                    
               
            </div>
        </div>
        <!---- Second Tab Starts ---->
        <!---- Third Tab Starts ---->
        <div id="tab-3" class="tab-content">
            <div class="col-xsm-12 col-lg-12">
                
                    <div class="row">
                                <div class="panel-body" >
                                    <div class="table-responsive dashboard-table col-md-12">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <input ng-model="selectAll" ng-click="oval.isAll()" id="checkedAllSubscription" class="ng-pristine ng-valid" type="checkbox">
                                                    </th>
                                                        <th>Invoice #</th>
                                                        <th style="">To</th>
                                                        <th style=" width: 0.5%">Summary</th>
                                                        <th style="">Date</th>
                                                        <th style="">Date Due</th>
                                                        <th style="">Origional</th>
                                                        <th style="width: 1%;">Type</th>
                                                        <th style="">Status</th>
                                                        <th style="width: 5%;">Options</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <input ng-model="sub.selected" ng-checked="selectAll" ng-click="selectedSubscription(sub)" id="subscription_83AE0045-E0A6-49A5-B123-1652FFE1D9F1" class="ng-pristine ng-valid" type="checkbox">
                                                        </td>
                                                        <td>
                                                            <p>INV0000362</p>
                                                        </td>
                                                        <td>
                                                            <p>Barclays</p>
                                                        </td>
                                                        <td>
                                                            <p>Interest Income</p>
                                                        </td>
                                                        <td>
                                                            <p>06 Feb 17</p>
                                                        </td>
                                                        <td>
                                                            <p>06 May 17</p>
                                                        </td>
                                                        <td>
                                                            <p>161.68</p>
                                                        </td>
                                                        <td>
                                                            <p>paid</p>
                                                        </td>
                                                        <td>
                                                            <img src="{$img_dir}images/envelope.png" class="envelope-img">
                                                            <img src="{$img_dir}images/cloud.png" class="cloud-img">

                                                        </td>
                                                        <td>
                                                            <div class="dropdown">
                                                              Manage<span class="caret">
                                                              <div class="dropdown-content">
                                                                <a href="#">Attachment</a>
                                                                <a href="#">Make a Copy</a>
                                                                <a href="#">Edit</a>
                                                                <a href="#">Email</a>
                                                                <a href="#">HTML</a>
                                                                <a href="#">PDF</a>
                                                              </div>
                                                            </div>
                                                        </td>
                                                        
                                                    </tr>
										            <tr>
                                                        <td>
                                                            <input ng-model="sub.selected" ng-checked="selectAll" ng-click="selectedSubscription(sub)" id="subscription_83AE0045-E0A6-49A5-B123-1652FFE1D9F1" class="ng-pristine ng-valid" type="checkbox">
                                                        </td>
                                                        <td>
                                                            <p>INV0004582</p>
                                                        </td>
                                                        <td>
                                                            <p>Commision 4u limited</p>
                                                        </td>
                                                        <td>
                                                            <p>May Commision</p>
                                                        </td>
                                                        <td>
                                                            <p>09 MArch 17</p>
                                                        </td>
                                                        <td>
                                                            <p>22 June 17</p>
                                                        </td>
                                                        <td>
                                                            <p>7951.68</p>
                                                        </td>
                                                        <td>
                                                            <p>paid</p>
                                                        </td>
                                                        <td>
                                                            <img src="{$img_dir}images/envelope.png" class="envelope-img">
                                                            <img src="{$img_dir}images/cloud.png" class="cloud-img">

                                                        </td>
                                                        <td>
                                                            <div class="dropdown">
                                                              Manage<span class="caret">
                                                              <div class="dropdown-content">
                                                                <a href="#">Attachment</a>
                                                                <a href="#">Make a Copy</a>
                                                                <a href="#">Edit</a>
                                                                <a href="#">Email</a>
                                                                <a href="#">HTML</a>
                                                                <a href="#">PDF</a>
                                                              </div>
                                                            </div>
                                                        </td>
                                                        
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input ng-model="sub.selected" ng-checked="selectAll" ng-click="selectedSubscription(sub)" id="subscription_83AE0045-E0A6-49A5-B123-1652FFE1D9F1" class="ng-pristine ng-valid" type="checkbox">
                                                        </td>
                                                        <td>
                                                            <p>INV0007945</p>
                                                        </td>
                                                        <td>
                                                            <p>Subscriptions R Us</p>
                                                        </td>
                                                        <td>
                                                            <p>Monthly Retainer</p>
                                                        </td>
                                                        <td>
                                                            <p>15 Feb 17</p>
                                                        </td>
                                                        <td>
                                                            <p>08 July 17</p>
                                                        </td>
                                                        <td>
                                                            <p>1542.36</p>
                                                        </td>
                                                        <td>
                                                            <p>paid</p>
                                                        </td>
                                                        <td>
                                                            <img src="{$img_dir}images/envelope.png" class="envelope-img">
                                                            <img src="{$img_dir}images/cloud.png" class="cloud-img">

                                                        </td>
                                                        <td>
                                                            <div class="dropdown">
                                                              Manage<span class="caret">
                                                              <div class="dropdown-content">
                                                                <a href="#">Attachment</a>
                                                                <a href="#">Make a Copy</a>
                                                                <a href="#">Edit</a>
                                                                <a href="#">Email</a>
                                                                <a href="#">HTML</a>
                                                                <a href="#">PDF</a>
                                                              </div>
                                                            </div>
                                                        </td>
                                                        
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input ng-model="sub.selected" ng-checked="selectAll" ng-click="selectedSubscription(sub)" id="subscription_83AE0045-E0A6-49A5-B123-1652FFE1D9F1" class="ng-pristine ng-valid" type="checkbox">
                                                        </td>
                                                        <td>
                                                            <p>INV0003625</p>
                                                        </td>
                                                        <td>
                                                            <p>Advertising Junction</p>
                                                        </td>
                                                        <td>
                                                            <p>Quarterly Fixed</p>
                                                        </td>
                                                        <td>
                                                            <p>11 Feb 17</p>
                                                        </td>
                                                        <td>
                                                            <p>13 May 17</p>
                                                        </td>
                                                        <td>
                                                            <p>1246.32</p>
                                                        </td>
                                                        <td>
                                                            <p>paid</p>
                                                        </td>
                                                        <td>
                                                            <img src="{$img_dir}images/envelope.png" class="envelope-img">
                                                            <img src="{$img_dir}images/cloud.png" class="cloud-img">

                                                        </td>
                                                        <td>
                                                            <div class="dropdown">
                                                              Manage<span class="caret">
                                                              <div class="dropdown-content">
                                                                <a href="#">Attachment</a>
                                                                <a href="#">Make a Copy</a>
                                                                <a href="#">Edit</a>
                                                                <a href="#">Email</a>
                                                                <a href="#">HTML</a>
                                                                <a href="#">PDF</a>
                                                              </div>
                                                            </div>
                                                        </td>
                                                        
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input ng-model="sub.selected" ng-checked="selectAll" ng-click="selectedSubscription(sub)" id="subscription_83AE0045-E0A6-49A5-B123-1652FFE1D9F1" class="ng-pristine ng-valid" type="checkbox">
                                                        </td>
                                                        <td>
                                                            <p>INV0004855</p>
                                                        </td>
                                                        <td>
                                                            <p>WPYS Limited</p>
                                                        </td>
                                                        <td>
                                                            <p>Monthly Subscription</p>
                                                        </td>
                                                        <td>
                                                            <p>01 Feb 17</p>
                                                        </td>
                                                        <td>
                                                            <p>01 May 17</p>
                                                        </td>
                                                        <td>
                                                            <p>699.99</p>
                                                        </td>
                                                        <td>
                                                            <p>paid</p>
                                                        </td>
                                                        <td>
                                                            <img src="{$img_dir}images/envelope.png" class="envelope-img">
                                                            <img src="{$img_dir}images/cloud.png" class="cloud-img">

                                                        </td>
                                                        <td>
                                                            <div class="dropdown">
                                                              Manage<span class="caret">
                                                              <div class="dropdown-content">
                                                                <a href="#">Attachment</a>
                                                                <a href="#">Make a Copy</a>
                                                                <a href="#">Edit</a>
                                                                <a href="#">Email</a>
                                                                <a href="#">HTML</a>
                                                                <a href="#">PDF</a>
                                                              </div>
                                                            </div>
                                                        </td>
                                                        
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input ng-model="sub.selected" ng-checked="selectAll" ng-click="selectedSubscription(sub)" id="subscription_83AE0045-E0A6-49A5-B123-1652FFE1D9F1" class="ng-pristine ng-valid" type="checkbox">
                                                        </td>
                                                        <td>
                                                            <p>INV0004466</p>
                                                        </td>
                                                        <td>
                                                            <p>Yellow Pages</p>
                                                        </td>
                                                        <td>
                                                            <p>Advertising Revenue</p>
                                                        </td>
                                                        <td>
                                                            <p>01 Feb 17</p>
                                                        </td>
                                                        <td>
                                                            <p>01 March 17</p>
                                                        </td>
                                                        <td>
                                                            <p>755.66</p>
                                                        </td>
                                                        <td>
                                                            <p>paid</p>
                                                        </td>
                                                        <td>
                                                            <img src="{$img_dir}images/envelope.png" class="envelope-img">
                                                            <img src="{$img_dir}images/cloud.png" class="cloud-img">

                                                        </td>
                                                        <td>
                                                            <div class="dropdown">
                                                              Manage<span class="caret">
                                                              <div class="dropdown-content">
                                                                <a href="#">Attachment</a>
                                                                <a href="#">Make a Copy</a>
                                                                <a href="#">Edit</a>
                                                                <a href="#">Email</a>
                                                                <a href="#">HTML</a>
                                                                <a href="#">PDF</a>
                                                              </div>
                                                            </div>
                                                        </td>
                                                        
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input ng-model="sub.selected" ng-checked="selectAll" ng-click="selectedSubscription(sub)" id="subscription_83AE0045-E0A6-49A5-B123-1652FFE1D9F1" class="ng-pristine ng-valid" type="checkbox">
                                                        </td>
                                                        <td>
                                                            <p>INV004875</p>
                                                        </td>
                                                        <td>
                                                            <p>Freedom Salvage</p>
                                                        </td>
                                                        <td>
                                                            <p>Commision</p>
                                                        </td>
                                                        <td>
                                                            <p>12 Jan 17</p>
                                                        </td>
                                                        <td>
                                                            <p>22 July 17</p>
                                                        </td>
                                                        <td>
                                                            <p>55201.36</p>
                                                        </td>
                                                        <td>
                                                            <p>paid</p>
                                                        </td>
                                                        <td>
                                                            <img src="{$img_dir}images/envelope.png" class="envelope-img">
                                                            <img src="{$img_dir}images/cloud.png" class="cloud-img">

                                                        </td>
                                                        <td>
                                                            <div class="dropdown">
                                                              Manage<span class="caret">
                                                              <div class="dropdown-content">
                                                                <a href="#">Attachment</a>
                                                                <a href="#">Make a Copy</a>
                                                                <a href="#">Edit</a>
                                                                <a href="#">Email</a>
                                                                <a href="#">HTML</a>
                                                                <a href="#">PDF</a>
                                                              </div>
                                                            </div>
                                                        </td>
                                                        
                                                    </tr>
                                                    
													
													
													
													
													
                                        </tbody>
                                        </table>
                                    </div>
                                </div>
								
                        
					   </div>
                    
                    
                
            </div>
        </div>
		

        <!---- Third Tab Starts ---->
        <!---- Fourth Tab Starts ---->
        <div id="tab-4" class="tab-content">
            <div class="col-xsm-12 col-lg-12">
                
                    
                    <div class="listing_user" id="inner-tab-1" >
                           
                            <div class="row">
                                <div class="panel-body" >
                                    <div class="table-responsive dashboard-table col-md-12">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <input ng-model="selectAll" ng-click="oval.isAll()" id="checkedAllSubscription" class="ng-pristine ng-valid" type="checkbox">
                                                    </th>
                                                        <th>Invoice #</th>
                                                        <th style="">To</th>
                                                        <th style=" width: 0.5%">Summary</th>
                                                        <th style="">Date</th>
                                                        <th style="">Date Due</th>
                                                        <th style="">Origional</th>
                                                        <th style="width: 1%;">Type</th>
                                                        <th style="">Status</th>
                                                        <th style="width: 5%;">Options</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <input ng-model="sub.selected" ng-checked="selectAll" ng-click="selectedSubscription(sub)" id="subscription_83AE0045-E0A6-49A5-B123-1652FFE1D9F1" class="ng-pristine ng-valid" type="checkbox">
                                                        </td>
                                                        <td>
                                                            <p>INV0004771</p>
                                                        </td>
                                                        <td>
                                                            <p>2nd Quarter Ad</p>
                                                        </td>
                                                        <td>
                                                            <p>Advertizing expense</p>
                                                        </td>
                                                        <td>
                                                            <p>14 May 17</p>
                                                        </td>
                                                        <td>
                                                            <p>30 July 17</p>
                                                        </td>
                                                        <td>
                                                            <p>899.99</p>
                                                        </td>
                                                        <td>
                                                            <p>draft</p>
                                                        </td>
                                                        <td>
                                                            <img src="{$img_dir}images/envelope.png" class="envelope-img">
                                                            <img src="{$img_dir}images/cloud.png" class="cloud-img">

                                                        </td>
                                                        <td>
                                                            <div class="dropdown">
                                                              Manage<span class="caret">
                                                              <div class="dropdown-content">
                                                                <a href="#">Attachment</a>
                                                                <a href="#">Make a Copy</a>
                                                                <a href="#">Edit</a>
                                                                <a href="#">Email</a>
                                                                <a href="#">HTML</a>
                                                                <a href="#">PDF</a>
                                                              </div>
                                                            </div>
                                                        </td>
                                                        
                                                    </tr>
										            <tr>
                                                        <td>
                                                            <input ng-model="sub.selected" ng-checked="selectAll" ng-click="selectedSubscription(sub)" id="subscription_83AE0045-E0A6-49A5-B123-1652FFE1D9F1" class="ng-pristine ng-valid" type="checkbox">
                                                        </td>
                                                        <td>
                                                            <p>INV0009332</p>
                                                        </td>
                                                        <td>
                                                            <p>SMP Sports</p>
                                                        </td>
                                                        <td>
                                                            <p>Monthly Features</p>
                                                        </td>
                                                        <td>
                                                            <p>15 July 17</p>
                                                        </td>
                                                        <td>
                                                            <p>15 Aug 17</p>
                                                        </td>
                                                        <td>
                                                            <p>585.63</p>
                                                        </td>
                                                        <td>
                                                            <p>draft</p>
                                                        </td>
                                                        <td>
                                                            <img src="{$img_dir}images/envelope.png" class="envelope-img">
                                                            <img src="{$img_dir}images/cloud.png" class="cloud-img">

                                                        </td>
                                                        <td>
                                                            <div class="dropdown">
                                                              Manage<span class="caret">
                                                              <div class="dropdown-content">
                                                                <a href="#">Attachment</a>
                                                                <a href="#">Make a Copy</a>
                                                                <a href="#">Edit</a>
                                                                <a href="#">Email</a>
                                                                <a href="#">HTML</a>
                                                                <a href="#">PDF</a>
                                                              </div>
                                                            </div>
                                                        </td>
                                                        
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input ng-model="sub.selected" ng-checked="selectAll" ng-click="selectedSubscription(sub)" id="subscription_83AE0045-E0A6-49A5-B123-1652FFE1D9F1" class="ng-pristine ng-valid" type="checkbox">
                                                        </td>
                                                        <td>
                                                            <p>INV0022566</p>
                                                        </td>
                                                        <td>
                                                            <p>QPWN Enterprise</p>
                                                        </td>
                                                        <td>
                                                            <p>Bi-Monthly Sales</p>
                                                        </td>
                                                        <td>
                                                            <p>15 Feb 17</p>
                                                        </td>
                                                        <td>
                                                            <p>15 April 17</p>
                                                        </td>
                                                        <td>
                                                            <p>14899.63</p>
                                                        </td>
                                                        <td>
                                                            <p>draft</p>
                                                        </td>
                                                        <td>
                                                            <img src="{$img_dir}images/envelope.png" class="envelope-img">
                                                            <img src="{$img_dir}images/cloud.png" class="cloud-img">

                                                        </td>
                                                        <td>
                                                            <div class="dropdown">
                                                              Manage<span class="caret">
                                                              <div class="dropdown-content">
                                                                <a href="#">Attachment</a>
                                                                <a href="#">Make a Copy</a>
                                                                <a href="#">Edit</a>
                                                                <a href="#">Email</a>
                                                                <a href="#">HTML</a>
                                                                <a href="#">PDF</a>
                                                              </div>
                                                            </div>
                                                        </td>
                                                        
                                                    </tr>
                                                   
													
													
													
													
													
                                         </tbody>
                                        </table>
                                    </div>
                                </div>
								
                        
					   </div>
                    
                    
                </div>
            
        </div>
		
		</div>
        
		
		
        <!---- Fourth Tab Starts ---->
        <!---- Fifth Tab Starts ---->
        <div id="tab-5" class="tab-content">
            <div class="col-xsm-12 col-lg-12">
                
                    <div class="listing_user" id="inner-tab-1" >
                           
                            <div class="row">
                                <div class="panel-body" >
                                    <div class="table-responsive dashboard-table col-md-12">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <input ng-model="selectAll" ng-click="oval.isAll()" id="checkedAllSubscription" class="ng-pristine ng-valid" type="checkbox">
                                                    </th>
                                                        <th>Invoice #</th>
                                                        <th style="">To</th>
                                                        <th style=" width: 0.5%">Summary</th>
                                                        <th style="">Date</th>
                                                        <th style="">Date Due</th>
                                                        <th style="">Origional</th>
                                                        <th style="width: 1%;">Type</th>
                                                        <th style="">Status</th>
                                                        <th style="width: 5%;">Options</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <input ng-model="sub.selected" ng-checked="selectAll" ng-click="selectedSubscription(sub)" id="subscription_83AE0045-E0A6-49A5-B123-1652FFE1D9F1" class="ng-pristine ng-valid" type="checkbox">
                                                        </td>
                                                        <td>
                                                            <p>INV0005521</p>
                                                        </td>
                                                        <td>
                                                            <p>CanAm Comm</p>
                                                        </td>
                                                        <td>
                                                            <p>telecomunications</p>
                                                        </td>
                                                        <td>
                                                            <p>15 June 17</p>
                                                        </td>
                                                        <td>
                                                            <p>29 Aug 17</p>
                                                        </td>
                                                        <td>
                                                            <p>499.95</p>
                                                        </td>
                                                        <td>
                                                            <p>credit</p>
                                                        </td>
                                                        <td>
                                                            <img src="{$img_dir}images/envelope.png" class="envelope-img">
                                                            <img src="{$img_dir}images/cloud.png" class="cloud-img">

                                                        </td>
                                                        <td>
                                                            <div class="dropdown">
                                                              Manage<span class="caret">
                                                              <div class="dropdown-content">
                                                                <a href="#">Attachment</a>
                                                                <a href="#">Make a Copy</a>
                                                                <a href="#">Edit</a>
                                                                <a href="#">Email</a>
                                                                <a href="#">HTML</a>
                                                                <a href="#">PDF</a>
                                                              </div>
                                                            </div>
                                                        </td>
                                                        
                                                    </tr>
										            <tr>
                                                        <td>
                                                            <input ng-model="sub.selected" ng-checked="selectAll" ng-click="selectedSubscription(sub)" id="subscription_83AE0045-E0A6-49A5-B123-1652FFE1D9F1" class="ng-pristine ng-valid" type="checkbox">
                                                        </td>
                                                        <td>
                                                            <p>INV0001254</p>
                                                        </td>
                                                        <td>
                                                            <p>MMR Realty Inc</p>
                                                        </td>
                                                        <td>
                                                            <p>Ad Space</p>
                                                        </td>
                                                        <td>
                                                            <p>15 May 17</p>
                                                        </td>
                                                        <td>
                                                            <p>15 Oct 17</p>
                                                        </td>
                                                        <td>
                                                            <p>852.36</p>
                                                        </td>
                                                        <td>
                                                            <p>credit</p>
                                                        </td>
                                                        <td>
                                                            <img src="{$img_dir}images/envelope.png" class="envelope-img">
                                                            <img src="{$img_dir}images/cloud.png" class="cloud-img">

                                                        </td>
                                                        <td>
                                                            <div class="dropdown">
                                                              Manage<span class="caret">
                                                              <div class="dropdown-content">
                                                                <a href="#">Attachment</a>
                                                                <a href="#">Make a Copy</a>
                                                                <a href="#">Edit</a>
                                                                <a href="#">Email</a>
                                                                <a href="#">HTML</a>
                                                                <a href="#">PDF</a>
                                                              </div>
                                                            </div>
                                                        </td>
                                                        
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input ng-model="sub.selected" ng-checked="selectAll" ng-click="selectedSubscription(sub)" id="subscription_83AE0045-E0A6-49A5-B123-1652FFE1D9F1" class="ng-pristine ng-valid" type="checkbox">
                                                        </td>
                                                        <td>
                                                            <p>INV0022555</p>
                                                        </td>
                                                        <td>
                                                            <p>KML Catering</p>
                                                        </td>
                                                        <td>
                                                            <p>Monthly Credit</p>
                                                        </td>
                                                        <td>
                                                            <p>15 Feb 17</p>
                                                        </td>
                                                        <td>
                                                            <p>15 April 17</p>
                                                        </td>
                                                        <td>
                                                            <p>99.99</p>
                                                        </td>
                                                        <td>
                                                            <p>credit</p>
                                                        </td>
                                                        <td>
                                                            <img src="{$img_dir}images/envelope.png" class="envelope-img">
                                                            <img src="{$img_dir}images/cloud.png" class="cloud-img">

                                                        </td>
                                                        <td>
                                                            <div class="dropdown">
                                                              Manage<span class="caret">
                                                              <div class="dropdown-content">
                                                                <a href="#">Attachment</a>
                                                                <a href="#">Make a Copy</a>
                                                                <a href="#">Edit</a>
                                                                <a href="#">Email</a>
                                                                <a href="#">HTML</a>
                                                                <a href="#">PDF</a>
                                                              </div>
                                                            </div>
                                                        </td>
                                                        
                                                    </tr>
                                                   
													
													
													
													
													
                                         </tbody>
                                        </table>
                                    </div>
                                </div>
								
                        
					   </div>
                    
                    
                </div>
                    
                    
               
            </div>
        </div>
		
		</div>
        <!---- Fifth Tab Starts ---->
        </section>
    </div>
</div>
	
	
	
	
	
	
	
	
  </div>
		
	
	</div>
	
	
	</div>
	</div>
	
	
	
	
  