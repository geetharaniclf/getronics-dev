<div id="columns" class="container">
	
<div class="row">
             
<div id="left_column" class="column col-xs-12 col-sm-3">
	{include file="{$one_var}control_panel/sidebar.tpl"}
</div>

<div id="center_column" class="center_column col-xs-12 col-sm-9">
		
		
    <div class="row">
        
        <div class="col-xs-12 col-lg-12 themed-nav">
						 <div class="box-title">
                              <p>Monitoring</p>
                            </div>
                          <div class="mini-box">

                            <div class="panel-header">
                             <aside data-ng-include=" 'views/virtual_machines/virtual_machine_nav.html' " id="nav-container" class="nav-container ng-scope nav-fixed nav-horizontal nav-vertical" data-ng-class="">

                            <div class="nav-wrapper">
                           
                                {*<ul id="nav" class="nav ng-scope" data-ng-controller="NavCtrl" data-collapse-nav="" data-highlight-active="">
                                    <li class="">

                                        <a href="#"><i class="fa fa-plus"></i><span>New</span></a>
                                    </li>

                                    <li>
                                        <a href="#" ><i class="fa fa-trash-o"></i><span>Delete</span></a>


                                    </li>

                                    <li>
                                        <a href="#" >
                                            <i class="fa fa-file-excel-o" aria-hidden="true"></i>

                                            <span data-i18n="Generate Excel">Generate Excel</span> 
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" >
                                            <i class="fa fa-file-pdf-o" aria-hidden="true"></i>

                                            <span data-i18n="Generate PDF">Generate PDF</span> 
                                        </a>
                                    </li>
                                </ul>*}
                           
                           
                            </div>    



                      </aside>

                            </div>
                            <div class="divider divider"></div>
                            
                       <div class="table-filters" >
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 ">
                                    <div class="col-sm-3 col-sm-offset-9 col-xs-4 col-xs-offset-8">
                                        <form class="ng-pristine ng-valid">
                                            <input placeholder="Search..." class="form-control ng-pristine ng-valid search_field" onkeyup="search($(this));" type="text">
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>   
                              
                        <div class="mini-box">
                            <div class="row">
                                <div class="panel-body body_table" >
                                    <div id="exportable" class="table-responsive dashboard-table col-md-12">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                   
                                                        <th>Name</th>
                                                        <th style="">Status</th>
                                                        <th style="">Provider</th>
                                                        
                                                        {foreach from=$mssqlData item=colname}
                                                           <th style="">{$colname.monitor_name}( {$colname.monitor_unit} )</th>
                                                        {/foreach}
                                                        
                                                    </tr>
                                                </thead>
                                                <tbody>
													
												  
													{foreach from=$vertualMachines key=key item=vmdata}
													    <tr>
													       <td><p> {$vmdata.name}</th>
													       <td><p> {$vmdata.state}</th>
													       <td><p> {$vmdata.provider}</th>
													       {foreach from=$mssqlData item=colname}
                                                              <th style="">
																  {$keyname="monitor_`$colname.monitor_id`"}
																  {if "$keyname"|array_key_exists:$vmdata}
																    {$vmdata.$keyname}
																  {/if}
															  </th>
                                                           {/foreach}
													    </tr>  
                                                    {/foreach}
													
												
                                        </tbody>
                                        </table>
                                    </div>
                                    
 

<div class="col-lg-12 text-right">
	   {include file="{$one_var}control_panel/pagination.tpl"}
</div>
	
	
 
                                    
                                    
                                </div>
								
                        
					   </div>
                        
                    </div>
                </div>
        
        
    


	
	
	
	
	
	
	
	
  </div>
		
	
	</div>
	
	
	</div>
	</div>
	
<script>
  
  
    $(document).ready(function () {
        $(document).on('click', '.page_update li a', function () {

            var sval = $('.search_field').val();
            var url = $(this).attr('href');
            if (url != '') {
                var res = url.replace("?", "&");
            }
            if (typeof res === 'undefined') {
                res = '';
            }
            change_data(sval, res);
            return false;

        });
        call_update();
    });	
    
    
    
     function call_update() {

			if ($('.row_tbl').length)
			{
				$('.state').each(function () {
					var guid = $(this).attr('id')
					var state = $(this).children('j').html();
					update_state(guid, state, $(this));
				});
			}

			setTimeout(function () {
				call_update();
			}, 10000);
   
    }


    function search(search) {
        var sval = $(search).val();
        var p = '';
        change_data(sval, p);
    }


    function change_data(sval, p) {

			$.ajax({
				type: 'post',
				url: baseUri + 'index.php?controller=cpajax',
				data: 'search=' + sval + '&page=monitoring' + p,
				success: function (resp)
				{
					$('.body_table').html(resp);
				}
			});

    }

</script>
	
	
  
