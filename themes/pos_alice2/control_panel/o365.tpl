	<script type='text/javascript'>
//<![CDATA[
$(window).load(function(){
$(document).ready(function() {
    $(".tabs-menu a").click(function(event) {
        event.preventDefault();
        $(this).parent().addClass("current");
        $(this).parent().siblings().removeClass("current");
        var tab = $(this).attr("href");
        $(".tab-content").not(tab).css("display", "none");
       $(".inner-tab-content").not(tab).css("display", "none");
		$(".listing_user").css('display', 'block');
        <!-- $(".tab-content").firstChild('div.nav-wrapper').next(div).attr("display", "block"); -->
        <!-- $(this).div('.nav-wrapper').next.css("display", "block"); -->
		
        $(tab).fadeIn();
    });

    $(".inner-tabs-menu a").click(function(event) {
        event.preventDefault();
        $(this).parent().addClass("current_1");
        $(this).parent().siblings().removeClass("current_1");
        var tab = $(this).attr("href");
       $(".inner-tab-content").not(tab).css("display", "none");
        $(".listing_user").css('display', 'none');
        $(tab).fadeIn();
    });
});
});

//]]> 

</script>

    
	<div id="columns" class="container">
	<div class="row">
             
<div id="left_column" class="column col-xs-12 col-sm-3">
	{include file="{$one_var}control_panel/sidebar.tpl"}
</div>

<div id="center_column" class="center_column col-xs-12 col-sm-9">
		
		
		<div class="row">
    
	
	
	
	
	
	
	
	
	
	
	<div class="col-lg-12 o365-main-page">
        <div class="col-lg-6">
			<div class="mini-box">
            <div class="box-title">
                    <p>Overview</p>
            </div>
            <div class="col-lg-12">
                <div class="col-lg-4 title365">
                    <p>Users</p>
                </div>
                <div class="col-lg-4 data365">
                    <p class="size-h1 ng-binding" style="color:#363636" id="totalPurchaseId">
1700
                    </p>
                </div>
                <div class="col-lg-4 btn365">
                    
                </div>
            </div>
            <hr>
            <div class="col-lg-12">
                <div class="col-lg-4 title365">
                    <p>Subscriptions</p>
                </div>
                <div class="col-lg-4 data365">
                    <p class="size-h1 ng-binding" style="color:#363636" id="totalPurchaseId">
                    2300
                    </p>
                </div>
                <div class="col-lg-4 btn365">
                    <a href="/index.php?controller=category&amp;id_category=90" target="_blank" class="btn btn-w-md btn-gap-v btn-black">New</a>
                </div>
            </div>
            <hr>
            <div class="col-lg-12">
                <div class="col-lg-4 title365">
                    <p>License</p>
                </div>
                <div class="col-lg-4 data365">
                    <p class="size-h1 ng-binding" style="color:#363636" id="totalPurchaseId">
                   
                    1545
                    </p>
                </div>
                <div class="col-lg-4 btn365">
                    <a href="/index.php?controller=category&amp;id_category=90" target="_blank" class="btn btn-w-md btn-gap-v btn-black">New</a>
                </div>
            </div>
			</div>
		</div>
        <div class="col-lg-6 billing_o365" id="billingWidget">            
            <div class="mini-box" id="billingDetailsId">
                <div class="box-title">
                <p>Billing</p>
            </div>
                <div class="row" id="totalAmount">
                    <div class="col-lg-12 col-centered o365pg">
                        <div class="box-info o365-total-three-main">
                            
							<p class="size-h1 text-center" id="total_price_amount" style="display:block;"><span>Getronics</span> <span class="ng-binding"> $12,450.00</span></p>
							<p class="size-h1 text-center" id="total_price_amount" style="display:block;"><span>Azure</span> <span class="ng-binding">  $7,800.00</span></p>
							<p class="size-h1 text-center" id="total_price_amount" style="display:block;"><span>O365</span> <span class="ng-binding">  $32,450.00</span></p>
							
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="billingDetailsTable" style="display:none;">
                        <div class="table-filters">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 ">
                                    <div class="col-sm-3 col-sm-offset-9 col-xs-4 col-xs-offset-8">
                                        <form class="ng-pristine ng-valid">
                                            <input placeholder="search" class="form-control ng-pristine ng-valid" data-ng-model="searchKeywords" data-ng-keyup="search()" type="text">
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive dashboard-table col-md-12" style="padding-top: 10px; padding-left: 22px; padding-right: 22px;">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th style="width:20%">Company</th>
                                        <th style="width:25%">Product Name/Service Name</th>
                                        <th>Payment Method</th>
                                        <th>Quantity</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!-- ngRepeat: billingData in allBillingDetails -->
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-6 col-md-offset-6 text-right pagination-container">
                            
							
                        </div>
                    </div>
                </div>
                
                <div class="col-lg-12 col-centered text-centre"> <a href="#/billing/billing" class="btn btn-w-md btn-blue">Details</a>
                        
                    </div>
            </div>
        </div>
         </div>
	
	
	
	
	
	
	
	<div class="sub-user-lice">
        <div class="col-lg-12">
            <section class="tabs-container-main">
				<div id="tabs-container">
				 <ul class="tabs-menu nav-pills menu_class">
  <li class="current"><a href="#tab-1" >SUBSCRIPTIONS</a> </li>
  <li><a href="#tab-2" >USERS</a></li>
  <li><a href="#tab-3" >LICENSES</a></li>
</ul>
               
				
				
                <!---- First Tab Starts ---->
                <div id="tab-1" class="tab-content">
                    <div class="col-xsm-12 col-lg-12">
                        <div class="mini-box">
                            <section>
                                
                                
                                <div class="nav-wrapper nav_custom">
                                    <ul class="nav">
                                        <li > <a class="" href="/index.php?controller=category&amp;id_category=90"><i class="fa fa-plus"></i> <span data-i18n="Add">Add</span> </a> </li>
                                        <li > <a class="" href=""><i class="fa fa-power-off"></i> <span data-i18n="Activate">Activate</span> </a> </li>
                                        <li > <a class="" href=""><i class="fa fa-ban"></i> <span data-i18n="Suspend">Suspend</span> </a> </li>
                                        <li > <a class="" href="#"><i class="fa fa-cubes"></i> <span data-i18n="Manage Add-Ons">Manage Add-Ons</span> </a> </li>
                                        <li > <a class="" href="#"><i class="fa fa-retweet"></i> <span data-i18n="Sync">Sync</span> </a> </li>
                                        <li > <a class="" href="#"><i class="fa fa-refresh" aria-hidden="true"></i> <span data-i18n="Refresh">Refresh</span> </a> </li>
                                    </ul>
                                </div>
                                <div class="row">
                                    <div class="panel-body" >
                                        <div class="table-responsive dashboard-table col-md-12">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <input ng-model="selectAll" ng-click="oval.isAll()" id="checkedAllSubscription" class="ng-pristine ng-valid" type="checkbox">
                                                        </th>
                                                        <th>Subscription</th>
                                                        <th style="text-align: center;">Status</th>
                                                        <th style="text-align: center; width: 0.5%">Quantity</th>
                                                        <th style="text-align: center;">Type</th>
                                                        <th style="text-align: center;">Cost</th>
                                                        <th style="text-align: center;">Term End Date</th>
                                                        <th style="width: 1%;">Edit</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    
													
													
													<tr >
                                                        <td>
                                                            <input ng-model="sub.selected" ng-checked="selectAll" ng-click="selectedSubscription(sub)" id="subscription_83AE0045-E0A6-49A5-B123-1652FFE1D9F1" class="ng-pristine ng-valid" type="checkbox">
                                                        </td>
                                                        <td><a href="#/o365/83AE0045-E0A6-49A5-B123-1652FFE1D9F1"><span class="ng-binding"> Office 365 Enterprise E1 </span></a>
                                                        </td>
                                                        <td style="text-align: center;">
                                              <span id="spinnerState_83AE0045-E0A6-49A5-B123-1652FFE1D9F1" style="text-transform: uppercase" class="ng-binding">
                suspended
            </span>
                                                        </td>
                                                        <td style="text-align: center;"><span editable-text="sub.quantity" e-name="quantity" e-form="rowform" e-class="edit-office365" class="ng-scope ng-binding editable">2 </span>
                                                        </td>
                                                        <td style="text-align: center;"><span class="ng-binding">Licenses</span>
                                                        </td>
                                                        <td style="text-align: center;"><span class="ng-binding">$&nbsp;&nbsp;User/Month </span>
                                                        </td>
                                                        <td style="text-align: center;"><span class="ng-binding">10 April 2018 +0530</span>
                                                        </td>
                                                        <td style="white-space: nowrap">
                                                            
                                        <div class="buttons ng-hide" >
                                            <a href="#" >
                                                <i class="fa fa-pencil-square-o"></i>
                                            </a>
                                        </div>
                                        </td>
                                        </tr>
										<tr >
                                                        <td>
                                                            <input ng-model="sub.selected" ng-checked="selectAll" ng-click="selectedSubscription(sub)" id="subscription_83AE0045-E0A6-49A5-B123-1652FFE1D9F1" class="ng-pristine ng-valid" type="checkbox">
                                                        </td>
                                                        <td><a href="#/o365/83AE0045-E0A6-49A5-B123-1652FFE1D9F1"><span class="ng-binding"> Office 365 Enterprise E1 </span></a>
                                                        </td>
                                                        <td style="text-align: center;">
                                              <span id="spinnerState_83AE0045-E0A6-49A5-B123-1652FFE1D9F1" style="text-transform: uppercase" class="ng-binding">
                suspended
            </span>
                                                        </td>
                                                        <td style="text-align: center;"><span editable-text="sub.quantity" e-name="quantity" e-form="rowform" e-class="edit-office365" class="ng-scope ng-binding editable">2 </span>
                                                        </td>
                                                        <td style="text-align: center;"><span class="ng-binding">Licenses</span>
                                                        </td>
                                                        <td style="text-align: center;"><span class="ng-binding">$&nbsp;&nbsp;User/Month </span>
                                                        </td>
                                                        <td style="text-align: center;"><span class="ng-binding">10 April 2018 +0530</span>
                                                        </td>
                                                        <td style="white-space: nowrap">
                                                            
                                        <div class="buttons ng-hide" >
                                            <a href="#" >
                                                <i class="fa fa-pencil-square-o"></i>
                                            </a>
                                        </div>
                                        </td>
                                        </tr>
<tr >
                                                        <td>
                                                            <input ng-model="sub.selected" ng-checked="selectAll" ng-click="selectedSubscription(sub)" id="subscription_83AE0045-E0A6-49A5-B123-1652FFE1D9F1" class="ng-pristine ng-valid" type="checkbox">
                                                        </td>
                                                        <td><a href="#/o365/83AE0045-E0A6-49A5-B123-1652FFE1D9F1"><span class="ng-binding"> Office 365 Enterprise E1 </span></a>
                                                        </td>
                                                        <td style="text-align: center;">
                                              <span id="spinnerState_83AE0045-E0A6-49A5-B123-1652FFE1D9F1" style="text-transform: uppercase" class="ng-binding">
                suspended
            </span>
                                                        </td>
                                                        <td style="text-align: center;"><span editable-text="sub.quantity" e-name="quantity" e-form="rowform" e-class="edit-office365" class="ng-scope ng-binding editable">2 </span>
                                                        </td>
                                                        <td style="text-align: center;"><span class="ng-binding">Licenses</span>
                                                        </td>
                                                        <td style="text-align: center;"><span class="ng-binding">$&nbsp;&nbsp;User/Month </span>
                                                        </td>
                                                        <td style="text-align: center;"><span class="ng-binding">10 April 2018 +0530</span>
                                                        </td>
                                                        <td style="white-space: nowrap">
                                                            
                                        <div class="buttons ng-hide" >
                                            <a href="#" >
                                                <i class="fa fa-pencil-square-o"></i>
                                            </a>
                                        </div>
                                        </td>
                                        </tr>										
													
													
													
													
													
													
													
                                        </tbody>
                                        </table>
                                    </div>
                                </div>
								
                        
					   </div>
                        </section>
                    </div>
                </div>
        </div>
        <!---- First Tab Ends ---->
        <!---- Second Tab Starts ---->
        <div id="tab-2" class="tab-content">
            <div class="col-xsm-12 col-lg-12">
                <div class="mini-box">
                    
                    <section >
                        <div class="nav-wrapper nav_custom">
                            <ul class="nav inner-tabs-menu">
                                <li >
                                    <a href="#inner-tab-2" ><i class="fa fa-plus"></i>
                                    <span data-i18n="Add">Add</span>
                                </a>
                                </li>
                                <li>
                                    <a href="#" ><i class="fa fa-trash-o"></i>
                                    <span data-i18n="Delete">Delete</span>
                                </a>
                                </li>
                                <li >
                                    <a href="#inner-tab-3" ><i class="fa fa-refresh"></i>
                                    <span data-i18n="Reset Password">Reset Password</span>
                                </a>
                                </li>
                                <li >
                                    <a href="#inner-tab-4" ><i class="fa fa-pencil-square-o"></i>
                                    <span data-i18n="Edit User Role">Edit User Role</span>
                                </a>
                                </li>
                                <li >
                                    <a href="#inner-tab-5" ><i class="fa fa-pencil"></i>
                                    <span data-i18n="Edit">Edit</span>
                                </a>
                                </li>
                                <li >
                                    <a href="#" ><i class="fa fa-file-text"></i>
                                    <span data-i18n="Assign License">Assign License</span>
                                </a>
                                </li>
                                <li >
                                    <a href="#" ><i class="fa fa-file-excel-o"></i>
                                    <span data-i18n="Remove License">Remove License</span>
                                </a>
                                </li>
                                <li > <a href="#" ><i class="fa fa-retweet"></i> <span data-i18n="Sync">Sync</span> </a> </li>
                                <li > <a href="#" ><i class="fa fa-refresh" aria-hidden="true"></i> <span data-i18n="Refresh">Refresh</span> </a>
                                </li>
                            </ul>
                        </div>
                        <div class="listing_user" id="inner-tab-1" >
                           
                            <div class="row">
                                <div class="panel-body">
                                    <div class="table-responsive dashboard-table col-md-12">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <input ng-model="selectAll" ng-click="oval.checkedAllData()" id="checkedAllUsers" class="ng-pristine ng-valid" type="checkbox">
                                                    </th>
                                                    <th style="text-align: left;">First Name</th>
                                                    <th style="text-align: left;">Last Name</th>
                                                    <th style="text-align: left;">Email Address</th>
                                                    <th style="text-align: center;">Location</th>
                                                    <th style="text-align: center;">Roles</th>
                                                    <th style="text-align: center;">Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr >
                                                    <td>
                                                        <input ng-model="individual_user.checked" ng-checked="selectAll" ng-click="selectedCustomerUserAccount($index, userAccounts)" id="user_54b46c14-a1f9-49a7-b299-19fcd5357689" class="ng-pristine ng-valid" type="checkbox">
                                                    </td>
                                                    <td style="text-align: left;">
                                                        <span class="ng-binding">
                                                SD
                                            </span>
                                                    </td>
                                                    <td style="text-align: left;">
                                                        <span class="ng-binding">
                                                Test
                                            </span>
                                                    </td>
                                                    <td style="text-align: left;">
                                                        <span class="ng-binding">
                                                admin@sdtest01234.onmicrosoft.com
                                            </span>
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <span class="ng-binding">
                                                -
                                            </span>
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <span class="ng-binding">
                                                Company Administrator
                                            </span>
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <span style="text-transform: uppercase;" class="ng-binding">
                                                 
                                             active
                                            </span>
                                                    </td>
                                                </tr>
												<tr >
                                                    <td>
                                                        <input ng-model="individual_user.checked" ng-checked="selectAll" ng-click="selectedCustomerUserAccount($index, userAccounts)" id="user_54b46c14-a1f9-49a7-b299-19fcd5357689" class="ng-pristine ng-valid" type="checkbox">
                                                    </td>
                                                    <td style="text-align: left;">
                                                        <span class="ng-binding">
                                                SD
                                            </span>
                                                    </td>
                                                    <td style="text-align: left;">
                                                        <span class="ng-binding">
                                                Test
                                            </span>
                                                    </td>
                                                    <td style="text-align: left;">
                                                        <span class="ng-binding">
                                                admin@sdtest01234.onmicrosoft.com
                                            </span>
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <span class="ng-binding">
                                                -
                                            </span>
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <span class="ng-binding">
                                                Company Administrator
                                            </span>
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <span style="text-transform: uppercase;" class="ng-binding">
                                                 
                                             active
                                            </span>
                                                    </td>
                                                </tr>
												<tr >
                                                    <td>
                                                        <input ng-model="individual_user.checked" ng-checked="selectAll" ng-click="selectedCustomerUserAccount($index, userAccounts)" id="user_54b46c14-a1f9-49a7-b299-19fcd5357689" class="ng-pristine ng-valid" type="checkbox">
                                                    </td>
                                                    <td style="text-align: left;">
                                                        <span class="ng-binding">
                                                SD
                                            </span>
                                                    </td>
                                                    <td style="text-align: left;">
                                                        <span class="ng-binding">
                                                Test
                                            </span>
                                                    </td>
                                                    <td style="text-align: left;">
                                                        <span class="ng-binding">
                                                admin@sdtest01234.onmicrosoft.com
                                            </span>
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <span class="ng-binding">
                                                -
                                            </span>
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <span class="ng-binding">
                                                Company Administrator
                                            </span>
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <span style="text-transform: uppercase;" class="ng-binding">
                                                 
                                             active
                                            </span>
                                                    </td>
                                                </tr>
												<tr >
                                                    <td>
                                                        <input ng-model="individual_user.checked" ng-checked="selectAll" ng-click="selectedCustomerUserAccount($index, userAccounts)" id="user_54b46c14-a1f9-49a7-b299-19fcd5357689" class="ng-pristine ng-valid" type="checkbox">
                                                    </td>
                                                    <td style="text-align: left;">
                                                        <span class="ng-binding">
                                                SD
                                            </span>
                                                    </td>
                                                    <td style="text-align: left;">
                                                        <span class="ng-binding">
                                                Test
                                            </span>
                                                    </td>
                                                    <td style="text-align: left;">
                                                        <span class="ng-binding">
                                                admin@sdtest01234.onmicrosoft.com
                                            </span>
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <span class="ng-binding">
                                                -
                                            </span>
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <span class="ng-binding">
                                                Company Administrator
                                            </span>
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <span style="text-transform: uppercase;" class="ng-binding">
                                                 
                                             active
                                            </span>
                                                    </td>
                                                </tr>
												<tr >
                                                    <td>
                                                        <input ng-model="individual_user.checked" ng-checked="selectAll" ng-click="selectedCustomerUserAccount($index, userAccounts)" id="user_54b46c14-a1f9-49a7-b299-19fcd5357689" class="ng-pristine ng-valid" type="checkbox">
                                                    </td>
                                                    <td style="text-align: left;">
                                                        <span class="ng-binding">
                                                SD
                                            </span>
                                                    </td>
                                                    <td style="text-align: left;">
                                                        <span class="ng-binding">
                                                Test
                                            </span>
                                                    </td>
                                                    <td style="text-align: left;">
                                                        <span class="ng-binding">
                                                admin@sdtest01234.onmicrosoft.com
                                            </span>
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <span class="ng-binding">
                                                -
                                            </span>
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <span class="ng-binding">
                                                Company Administrator
                                            </span>
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <span style="text-transform: uppercase;" class="ng-binding">
                                                 
                                             active
                                            </span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="add_user_account add_user_account form-container inner-tab-content" id="inner-tab-2">
                            <div class="form_heading">
                                <span class="boxhead">CREATE NEW USER ACCOUNT</span>
                                <hr class="boxline">
                            </div>
                           <form  class="form-horizontal dashboard-form form_subs_user " name="userCreationForm" >
                                                 
                                 <div class="form-group">
                                    <label class="col-sm-2 control-label">First Name <sup>*</sup> :</label>
                                    <div class="col-sm-4">
                                        
 <input class="form-control ng-invalid ng-invalid-required ng-valid-pattern ng-animate ng-dirty-remove ng-dirty-remove-active ng-pristine" validate-on="dirty" name="office365_first_name" ng-model="oval.individual_user.office365_first_name" invalid-message="'Invalid format'" required-message="'required field'" required="" style="" type="text">
                                        
                                         

                                    </div>
                                    <label class="col-sm-2 control-label">Last Name <sup>*</sup> :</label>
                                    <div class="col-sm-4">
 <input name="office365_last_name" validate-on="dirty" ng-model="oval.individual_user.office365_last_name" invalid-message="'Invalid format'" required-message="'required field'" required="" class="form-control ng-invalid ng-invalid-required ng-valid-pattern ng-animate ng-dirty-remove ng-dirty-remove-active ng-pristine" style="" type="text">
                                    </div>
                                </div>
                                 <div class="form-group">
                                    <label class="col-sm-2 control-label">Display Name <sup>*</sup> :</label>
                                    <div class="col-sm-4">
<input name="office365_display_name" validate-on="dirty" invalid-message="'Invalid format'" ng-model="oval.individual_user.office365_display_name" required-message="'required field'" required="" class="form-control ng-invalid ng-invalid-required ng-animate ng-dirty-remove ng-dirty-remove-active ng-pristine" style="" type="text">
                                    </div>
                                    <label class="col-sm-2 control-label">User Name <sup>*</sup> :</label>
                                    <div class="col-sm-4">
<input name="office365_user_name" validate-on="dirty" invalid-message="'Invalid format'" onkeypress="javascript:return isNumberKey(this,event)" ng-model="oval.individual_user.office365_user_name" required-message="'required field'" required="" class="form-control ng-invalid ng-invalid-required ng-animate ng-dirty-remove ng-dirty-remove-active ng-pristine" style="" type="text">
									<span class="customer-domain ng-binding">@</span>
                                    </div>
                                    
                                </div>
                                 <div class="form-group">
                                    <label class="col-sm-2 control-label">Password <sup>*</sup> :</label>
                                    <div class="col-sm-4">
<input name="office365_password" validate-on="dirty" invalid-message="'Password must be at least 8 alphanumeric characters'" required="" ng-model="oval.individual_user.office365_password" required-message="'required field'" class="form-control ng-invalid ng-invalid-required ng-valid-pattern ng-animate ng-dirty-remove ng-dirty-remove-active ng-pristine" style="" type="password">
 <!--<small style="color:red;" class="errorMessage" ng-show="(userCreationForm.office365_password.$touched && userCreationForm.office365_password.$invalid) || userCreationForm.office365_password.$error.pattern">Password must be at least 8 alphanumeric characters</small>-->
      
                                    </div>
                                    <label class="col-sm-2 control-label">Location <sup>*</sup> : </label>
                                    <div class="col-sm-4">
             

  <select class="form-control ng-scope ng-invalid ng-invalid-required ng-animate ng-dirty-remove ng-dirty-remove-active ng-pristine" validate-on="dirty" name="office365_location" invalid-message="''" required-message="'required field'" ng-model="oval.individual_user.selected_country" pvp-country-picker="" required="" ng-options="country.alpha2 as country.name for country in countries" style=""><option value="?" selected="selected" label=""></option><option value="0" label="Afghanistan">Afghanistan</option><option value="1" label="Åland Islands">Åland Islands</option><option value="2" label="Albania">Albania</option><option value="3" label="Algeria">Algeria</option><option value="4" label="American Samoa">American Samoa</option><option value="5" label="Andorra">Andorra</option><option value="6" label="Angola">Angola</option><option value="7" label="Anguilla">Anguilla</option><option value="8" label="Antarctica">Antarctica</option><option value="9" label="Antigua and Barbuda">Antigua and Barbuda</option><option value="10" label="Argentina">Argentina</option><option value="11" label="Armenia">Armenia</option><option value="12" label="Aruba">Aruba</option><option value="13" label="Australia">Australia</option><option value="14" label="Austria">Austria</option><option value="15" label="Azerbaijan">Azerbaijan</option><option value="16" label="Bahamas (the)">Bahamas (the)</option><option value="17" label="Bahrain">Bahrain</option><option value="18" label="Bangladesh">Bangladesh</option><option value="19" label="Barbados">Barbados</option><option value="20" label="Belarus">Belarus</option><option value="21" label="Belgium">Belgium</option><option value="22" label="Belize">Belize</option><option value="23" label="Benin">Benin</option><option value="24" label="Bermuda">Bermuda</option><option value="25" label="Bhutan">Bhutan</option><option value="26" label="Bolivia (Plurinational State of)">Bolivia (Plurinational State of)</option><option value="27" label="Bonaire, Sint Eustatius and Saba">Bonaire, Sint Eustatius and Saba</option><option value="28" label="Bosnia and Herzegovina">Bosnia and Herzegovina</option><option value="29" label="Botswana">Botswana</option><option value="30" label="Bouvet Island">Bouvet Island</option><option value="31" label="Brazil">Brazil</option><option value="32" label="British Indian Ocean Territory (the)">British Indian Ocean Territory (the)</option><option value="33" label="Brunei Darussalam">Brunei Darussalam</option><option value="34" label="Bulgaria">Bulgaria</option><option value="35" label="Burkina Faso">Burkina Faso</option><option value="36" label="Burundi">Burundi</option><option value="37" label="Cabo Verde">Cabo Verde</option><option value="38" label="Cambodia">Cambodia</option><option value="39" label="Cameroon">Cameroon</option><option value="40" label="Canada">Canada</option><option value="41" label="Cayman Islands (the)">Cayman Islands (the)</option><option value="42" label="Central African Republic (the)">Central African Republic (the)</option><option value="43" label="Chad">Chad</option><option value="44" label="Chile">Chile</option><option value="45" label="China">China</option><option value="46" label="Christmas Island">Christmas Island</option><option value="47" label="Cocos (Keeling) Islands (the)">Cocos (Keeling) Islands (the)</option><option value="48" label="Colombia">Colombia</option><option value="49" label="Comoros (the)">Comoros (the)</option><option value="50" label="Congo (the)">Congo (the)</option><option value="51" label="Congo (the Democratic Republic of the)">Congo (the Democratic Republic of the)</option><option value="52" label="Cook Islands (the)">Cook Islands (the)</option><option value="53" label="Costa Rica">Costa Rica</option><option value="54" label="Côte d'Ivoire">Côte d'Ivoire</option><option value="55" label="Croatia">Croatia</option><option value="56" label="Cuba">Cuba</option><option value="57" label="Curaçao">Curaçao</option><option value="58" label="Cyprus">Cyprus</option><option value="59" label="Czech Republic (the)">Czech Republic (the)</option><option value="60" label="Denmark">Denmark</option><option value="61" label="Djibouti">Djibouti</option><option value="62" label="Dominica">Dominica</option><option value="63" label="Dominican Republic (the)">Dominican Republic (the)</option><option value="64" label="Ecuador">Ecuador</option><option value="65" label="Egypt">Egypt</option><option value="66" label="El Salvador">El Salvador</option><option value="67" label="Equatorial Guinea">Equatorial Guinea</option><option value="68" label="Eritrea">Eritrea</option><option value="69" label="Estonia">Estonia</option><option value="70" label="Ethiopia">Ethiopia</option><option value="71" label="Falkland Islands (the) [Malvinas]">Falkland Islands (the) [Malvinas]</option><option value="72" label="Faroe Islands (the)">Faroe Islands (the)</option><option value="73" label="Fiji">Fiji</option><option value="74" label="Finland">Finland</option><option value="75" label="France">France</option><option value="76" label="French Guiana">French Guiana</option><option value="77" label="French Polynesia">French Polynesia</option><option value="78" label="French Southern Territories (the)">French Southern Territories (the)</option><option value="79" label="Gabon">Gabon</option><option value="80" label="Gambia (the)">Gambia (the)</option><option value="81" label="Georgia">Georgia</option><option value="82" label="Germany">Germany</option><option value="83" label="Ghana">Ghana</option><option value="84" label="Gibraltar">Gibraltar</option><option value="85" label="Greece">Greece</option><option value="86" label="Greenland">Greenland</option><option value="87" label="Grenada">Grenada</option><option value="88" label="Guadeloupe">Guadeloupe</option><option value="89" label="Guam">Guam</option><option value="90" label="Guatemala">Guatemala</option><option value="91" label="Guernsey">Guernsey</option><option value="92" label="Guinea">Guinea</option><option value="93" label="Guinea-Bissau">Guinea-Bissau</option><option value="94" label="Guyana">Guyana</option><option value="95" label="Haiti">Haiti</option><option value="96" label="Heard Island and McDonald Islands">Heard Island and McDonald Islands</option><option value="97" label="Holy See (the)">Holy See (the)</option><option value="98" label="Honduras">Honduras</option><option value="99" label="Hong Kong">Hong Kong</option><option value="100" label="Hungary">Hungary</option><option value="101" label="Iceland">Iceland</option><option value="102" label="India">India</option><option value="103" label="Indonesia">Indonesia</option><option value="104" label="Iran (Islamic Republic of)">Iran (Islamic Republic of)</option><option value="105" label="Iraq">Iraq</option><option value="106" label="Ireland">Ireland</option><option value="107" label="Isle of Man">Isle of Man</option><option value="108" label="Israel">Israel</option><option value="109" label="Italy">Italy</option><option value="110" label="Jamaica">Jamaica</option><option value="111" label="Japan">Japan</option><option value="112" label="Jersey">Jersey</option><option value="113" label="Jordan">Jordan</option><option value="114" label="Kazakhstan">Kazakhstan</option><option value="115" label="Kenya">Kenya</option><option value="116" label="Kiribati">Kiribati</option><option value="117" label="Korea (the Democratic People's Republic of)">Korea (the Democratic People's Republic of)</option><option value="118" label="Korea (the Republic of)">Korea (the Republic of)</option><option value="119" label="Kuwait">Kuwait</option><option value="120" label="Kyrgyzstan">Kyrgyzstan</option><option value="121" label="Lao People's Democratic Republic (the)">Lao People's Democratic Republic (the)</option><option value="122" label="Latvia">Latvia</option><option value="123" label="Lebanon">Lebanon</option><option value="124" label="Lesotho">Lesotho</option><option value="125" label="Liberia">Liberia</option><option value="126" label="Libya">Libya</option><option value="127" label="Liechtenstein">Liechtenstein</option><option value="128" label="Lithuania">Lithuania</option><option value="129" label="Luxembourg">Luxembourg</option><option value="130" label="Macao">Macao</option><option value="131" label="Macedonia (the former Yugoslav Republic of)">Macedonia (the former Yugoslav Republic of)</option><option value="132" label="Madagascar">Madagascar</option><option value="133" label="Malawi">Malawi</option><option value="134" label="Malaysia">Malaysia</option><option value="135" label="Maldives">Maldives</option><option value="136" label="Mali">Mali</option><option value="137" label="Malta">Malta</option><option value="138" label="Marshall Islands (the)">Marshall Islands (the)</option><option value="139" label="Martinique">Martinique</option><option value="140" label="Mauritania">Mauritania</option><option value="141" label="Mauritius">Mauritius</option><option value="142" label="Mayotte">Mayotte</option><option value="143" label="Mexico">Mexico</option><option value="144" label="Micronesia (Federated States of)">Micronesia (Federated States of)</option><option value="145" label="Moldova (the Republic of)">Moldova (the Republic of)</option><option value="146" label="Monaco">Monaco</option><option value="147" label="Mongolia">Mongolia</option><option value="148" label="Montenegro">Montenegro</option><option value="149" label="Montserrat">Montserrat</option><option value="150" label="Morocco">Morocco</option><option value="151" label="Mozambique">Mozambique</option><option value="152" label="Myanmar">Myanmar</option><option value="153" label="Namibia">Namibia</option><option value="154" label="Nauru">Nauru</option><option value="155" label="Nepal">Nepal</option><option value="156" label="Netherlands (the)">Netherlands (the)</option><option value="157" label="New Caledonia">New Caledonia</option><option value="158" label="New Zealand">New Zealand</option><option value="159" label="Nicaragua">Nicaragua</option><option value="160" label="Niger (the)">Niger (the)</option><option value="161" label="Nigeria">Nigeria</option><option value="162" label="Niue">Niue</option><option value="163" label="Norfolk Island">Norfolk Island</option><option value="164" label="Northern Mariana Islands (the)">Northern Mariana Islands (the)</option><option value="165" label="Norway">Norway</option><option value="166" label="Oman">Oman</option><option value="167" label="Pakistan">Pakistan</option><option value="168" label="Palau">Palau</option><option value="169" label="Palestine, State of">Palestine, State of</option><option value="170" label="Panama">Panama</option><option value="171" label="Papua New Guinea">Papua New Guinea</option><option value="172" label="Paraguay">Paraguay</option><option value="173" label="Peru">Peru</option><option value="174" label="Philippines (the)">Philippines (the)</option><option value="175" label="Pitcairn">Pitcairn</option><option value="176" label="Poland">Poland</option><option value="177" label="Portugal">Portugal</option><option value="178" label="Puerto Rico">Puerto Rico</option><option value="179" label="Qatar">Qatar</option><option value="180" label="Réunion">Réunion</option><option value="181" label="Romania">Romania</option><option value="182" label="Russian Federation (the)">Russian Federation (the)</option><option value="183" label="Rwanda">Rwanda</option><option value="184" label="Saint Barthélemy">Saint Barthélemy</option><option value="185" label="Saint Helena, Ascension and Tristan da Cunha">Saint Helena, Ascension and Tristan da Cunha</option><option value="186" label="Saint Kitts and Nevis">Saint Kitts and Nevis</option><option value="187" label="Saint Lucia">Saint Lucia</option><option value="188" label="Saint Martin (French part)">Saint Martin (French part)</option><option value="189" label="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option><option value="190" label="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option><option value="191" label="Samoa">Samoa</option><option value="192" label="San Marino">San Marino</option><option value="193" label="Sao Tome and Principe">Sao Tome and Principe</option><option value="194" label="Saudi Arabia">Saudi Arabia</option><option value="195" label="Senegal">Senegal</option><option value="196" label="Serbia">Serbia</option><option value="197" label="Seychelles">Seychelles</option><option value="198" label="Sierra Leone">Sierra Leone</option><option value="199" label="Singapore">Singapore</option><option value="200" label="Sint Maarten (Dutch part)">Sint Maarten (Dutch part)</option><option value="201" label="Slovakia">Slovakia</option><option value="202" label="Slovenia">Slovenia</option><option value="203" label="Solomon Islands">Solomon Islands</option><option value="204" label="Somalia">Somalia</option><option value="205" label="South Africa">South Africa</option><option value="206" label="South Georgia and the South Sandwich Islands">South Georgia and the South Sandwich Islands</option><option value="207" label="South Sudan ">South Sudan </option><option value="208" label="Spain">Spain</option><option value="209" label="Sri Lanka">Sri Lanka</option><option value="210" label="Sudan (the)">Sudan (the)</option><option value="211" label="Suriname">Suriname</option><option value="212" label="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option><option value="213" label="Swaziland">Swaziland</option><option value="214" label="Sweden">Sweden</option><option value="215" label="Switzerland">Switzerland</option><option value="216" label="Syrian Arab Republic">Syrian Arab Republic</option><option value="217" label="Taiwan (Province of China)">Taiwan (Province of China)</option><option value="218" label="Tajikistan">Tajikistan</option><option value="219" label="Tanzania, United Republic of">Tanzania, United Republic of</option><option value="220" label="Thailand">Thailand</option><option value="221" label="Timor-Leste">Timor-Leste</option><option value="222" label="Togo">Togo</option><option value="223" label="Tokelau">Tokelau</option><option value="224" label="Tonga">Tonga</option><option value="225" label="Trinidad and Tobago">Trinidad and Tobago</option><option value="226" label="Tunisia">Tunisia</option><option value="227" label="Turkey">Turkey</option><option value="228" label="Turkmenistan">Turkmenistan</option><option value="229" label="Turks and Caicos Islands (the)">Turks and Caicos Islands (the)</option><option value="230" label="Tuvalu">Tuvalu</option><option value="231" label="Uganda">Uganda</option><option value="232" label="Ukraine">Ukraine</option><option value="233" label="United Arab Emirates (the)">United Arab Emirates (the)</option><option value="234" label="United Kingdom of Great Britain and Northern Ireland (the)">United Kingdom of Great Britain and Northern Ireland (the)</option><option value="235" label="United States Minor Outlying Islands (the)">United States Minor Outlying Islands (the)</option><option value="236" label="United States of America (the)">United States of America (the)</option><option value="237" label="Uruguay">Uruguay</option><option value="238" label="Uzbekistan">Uzbekistan</option><option value="239" label="Vanuatu">Vanuatu</option><option value="240" label="Venezuela (Bolivarian Republic of)">Venezuela (Bolivarian Republic of)</option><option value="241" label="Vietnam">Vietnam</option><option value="242" label="Virgin Islands (British)">Virgin Islands (British)</option><option value="243" label="Virgin Islands (U.S.)">Virgin Islands (U.S.)</option><option value="244" label="Wallis and Futuna">Wallis and Futuna</option><option value="245" label="Western Sahara">Western Sahara</option><option value="246" label="Yemen">Yemen</option><option value="247" label="Zambia">Zambia</option><option value="248" label="Zimbabwe">Zimbabwe</option></select>
   
  

                                    </div>
                                </div>
                        <hr class="hr_divider">
                                 <div class="form-group save-password">
                                    <div class="col-sm-12">
<input id="office365_set_password" name="show_password" ng-checked="true" ng-model="oval.individual_user.show_password" class="ng-valid ng-pristine" checked="checked" type="checkbox">

                                        <label for="" class="col-sm-8 control-label dark-color">Make this person change their password the next time they sign in.</label>
                                    </div>
                                </div>
                                   
                                <hr class="reset_hr_line reset_hr_password">
                                <div class="modal-footer form-footer a-left o365-add-user-from-btn" style="border-top:0px;">
                                    <input class="btn btn-blue" value="Cancel"  type="button">
                                    
                                     <button type="submit" class="btn btn-dark" id="button1">Create </button>
                                    
                                </div>
     </form>
                        </div>
                        
						<!--Reset Password section starts-->
                        <div class="reset_pswd_count form-container inner-tab-content" id="inner-tab-3">
                            <div class="form_heading">
                                <span class="boxhead">RESET PASSWORD</span>
                                <hr class="boxline">
                            </div>
                            <form class="form-horizontal dashboard-form form_subs_user ng-pristine ng-valid" name="resetPasswordForm" ng-submit="resetPasswordForm.$valid &amp;&amp; resetUserPassword()">
                                <div class="form-group reset_password_image">
                                    <img src="{$img_dir}images/user-big.png" alt="" class="img-circle">
                                    <div class="col-sm-4">
                                        <span class="span_reset_name ng-binding">
                                                 
                                                </span>
                                        <span class="span_reset_email ng-binding">
                                                
                                                </span>
                                    </div>
                                </div>
                                <!--<div class="form-group save-password">
                                    <div class="col-sm-8">
                                        <input type="checkbox" name="send_mail" ng-model="reset_password.send_mail" ng-false-value="false" ng-true-value="true" ng-checked="reset_password.send_mail">
                                        <label for="" class="col-sm-8 control-label dark-color">Send result in email</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-6">
                                        <input class="form-control" name="email_addresses" type="text" ng-model="reset_password.office365_recipient_email">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        Separate multiple email addresses (up to 5) with semicolon(;).
                                    </div>
                                </div>-->
                                <div class="form-group set-password">
                                    <label class="col-sm-2 control-label"><sup>*</sup>Password:</label>
                                    <div class="col-sm-4">
                                        <input name="office365_password" ng-model="reset_password_data.office365_password" class="form-control ng-pristine ng-valid" type="password">
                                    </div>
                                </div>
                                <div class="form-group save-password">
                                    <div class="col-sm-12">
                                        <input name="office365_change_password_next_login" ng-model="reset_password_data.office365_change_password_next_login" ng-checked="reset_password.office365_change_password_next_login=='true'" class="ng-pristine ng-valid" type="checkbox">
                                        <label for="" class="col-sm-8 control-label dark-color">Make this person change their password the next time they sign in.</label>
                                    </div>
                                </div>
                                <hr class="reset_hr_line reset_hr_password">
                                <div class="modal-footer a-left form-footer" style="border-top: 0px">
                                    <input class="btn btn-blue" value="Cancel" ng-click="user_tab = 1" type="button">
                                    <input class="btn btn-dark" value="Save" ng-click="user_tab = 1" type="submit">
                                </div>
                            </form>
                        </div>
                        <!--Reset Password section ends-->
                        <!--Edit User Role Section Starts-->
                        <div class="add_user_account form-container inner-tab-content" id="inner-tab-4">
                            <div class="form_heading">
                                <span class="boxhead">EDIT USER ROLE</span>
                                <hr class="boxline">
                            </div>
                            <form name="editUserRoleForm" class="form-horizontal dashboard-form edituserrole_form ng-pristine ng-valid" ng-submit="editUserRoleForm.$valid &amp;&amp; updateUserRole()">
                                <div class="form-group reset_password_image">
                                    <img src="{$img_dir}images/user-big.png" alt="" class="img-circle">
                                    <div class="col-sm-4">
                                        <span class="span_reset_name ng-binding">
                                                 
                                                </span>
                                        <span class="span_reset_email ng-binding">
                                                
                                                </span>
                                        <span class="span_reset_roles ng-binding">
                                                
                                        </span>        
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12 chooee_admin_msg">
                                        Choose the admin role that you want to asign to this user and save changes
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-8 message_info">
                                        You can't edit your own security settings, so not all settings are available here.
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <!-- ngRepeat: eval in getCustomerDirectoryRoles() --><label class="radio ng-binding ng-scope" ng-repeat="eval in getCustomerDirectoryRoles()">
                                        <input ng-change="newPrimaryRole(eval.name)" ng-model="update_user_role.role" name="evaluatorOptions" value="User" class="ng-pristine ng-valid" type="radio">User
                                         <!-- ngIf: eval.name === 'Company Administrator' --><!-- ngIf: eval.name === 'User' --><span ng-if="eval.name === 'User'" class="ng-scope">(can use licenses and services)</span><!-- end ngIf: eval.name === 'User' --></label><!-- end ngRepeat: eval in getCustomerDirectoryRoles() --><label class="radio ng-binding ng-scope" ng-repeat="eval in getCustomerDirectoryRoles()">
                                        <input ng-change="newPrimaryRole(eval.name)" ng-model="update_user_role.role" name="evaluatorOptions" value="Company Administrator" class="ng-pristine ng-valid" type="radio">Company Administrator
                                         <!-- ngIf: eval.name === 'Company Administrator' --><span ng-if="eval.name === 'Company Administrator'" class="ng-scope">(has full permissions)</span><!-- end ngIf: eval.name === 'Company Administrator' --><!-- ngIf: eval.name === 'User' --></label><!-- end ngRepeat: eval in getCustomerDirectoryRoles() --><label class="radio ng-binding ng-scope" ng-repeat="eval in getCustomerDirectoryRoles()">
                                        <input ng-change="newPrimaryRole(eval.name)" ng-model="update_user_role.role" name="evaluatorOptions" value="Custom Permissions" class="ng-pristine ng-valid" type="radio">Custom Permissions
                                         <!-- ngIf: eval.name === 'Company Administrator' --><!-- ngIf: eval.name === 'User' --></label><!-- end ngRepeat: eval in getCustomerDirectoryRoles() -->
                                    </div>
                                    <!-- ngIf: update_user_role.role === 'Custom Permissions' -->
                                </div>
                                <!--<div class="form-group grp_radio">
                                    <div class="col-sm-12">
                                        <input type="radio" name="user_role" class="radio_user_role" ng-model="edit_user_role.office365_user_role" value="user">User</label>
                                    </div>
                                </div>
                                <div class="form-group grp_radio">
                                    <div class="col-sm-12">
                                        <input type="radio" name="user_role" class="radio_user_role" ng-model="edit_user_role.office365_user_role" ">
                                        <label class="radio_label_user">Global Administrator</label>
                                    </div>
                                </div>
                                <div class="form-group grp_radio">
                                    <div class="col-sm-12">
                                        <input type="radio" name="user_role" class="radio_user_role" ng-model="edit_user_role.office365_user_role" value="limited">Limited Admin Role</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Alternate Email Address:</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="recipient_email" ng-model="edit_user_role.office365_recipient_email">
                                    </div>
                                </div>-->
                                <hr class="reset_hr_line reset_hr_password">
                                <div class="modal-footer a-left form-footer" style="border-top: 0px">
                                    <input class="btn btn-blue" value="Cancel" ng-click="user_tab = 1" type="button">
                                    <input class="btn btn-dark" value="Save" ng-click="user_tab = 1" type="submit">
                                </div>
                            </form>
                        </div>
                        <!--Reset Password section ends-->
                        <!--Edit User Role Section Starts-->
                        <div class="add_user_account form-container inner-tab-content" id="inner-tab-5">
                            <div class="form_heading">
                                <span class="boxhead">EDIT USER ROLE</span>
                                <hr class="boxline">
                            </div>
                            <form name="editUserRoleForm" class="form-horizontal dashboard-form edituserrole_form ng-pristine ng-valid" ng-submit="editUserRoleForm.$valid &amp;&amp; updateUserRole()">
                                <div class="form-group reset_password_image">
                                    <img src="{$img_dir}images/user-big.png" alt="" class="img-circle">
                                    <div class="col-sm-4">
                                        <span class="span_reset_name ng-binding">
                                                 
                                                </span>
                                        <span class="span_reset_email ng-binding">
                                                
                                                </span>
                                        <span class="span_reset_roles ng-binding">
                                                
                                        </span>        
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12 chooee_admin_msg">
                                        Choose the admin role that you want to asign to this user and save changes
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-8 message_info">
                                        You can't edit your own security settings, so not all settings are available here.
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <!-- ngRepeat: eval in getCustomerDirectoryRoles() --><label class="radio ng-binding ng-scope" ng-repeat="eval in getCustomerDirectoryRoles()">
                                        <input ng-change="newPrimaryRole(eval.name)" ng-model="update_user_role.role" name="evaluatorOptions" value="User" class="ng-pristine ng-valid" type="radio">User
                                         <!-- ngIf: eval.name === 'Company Administrator' --><!-- ngIf: eval.name === 'User' --><span ng-if="eval.name === 'User'" class="ng-scope">(can use licenses and services)</span><!-- end ngIf: eval.name === 'User' --></label><!-- end ngRepeat: eval in getCustomerDirectoryRoles() --><label class="radio ng-binding ng-scope" ng-repeat="eval in getCustomerDirectoryRoles()">
                                        <input ng-change="newPrimaryRole(eval.name)" ng-model="update_user_role.role" name="evaluatorOptions" value="Company Administrator" class="ng-pristine ng-valid" type="radio">Company Administrator
                                         <!-- ngIf: eval.name === 'Company Administrator' --><span ng-if="eval.name === 'Company Administrator'" class="ng-scope">(has full permissions)</span><!-- end ngIf: eval.name === 'Company Administrator' --><!-- ngIf: eval.name === 'User' --></label><!-- end ngRepeat: eval in getCustomerDirectoryRoles() --><label class="radio ng-binding ng-scope" ng-repeat="eval in getCustomerDirectoryRoles()">
                                        <input ng-change="newPrimaryRole(eval.name)" ng-model="update_user_role.role" name="evaluatorOptions" value="Custom Permissions" class="ng-pristine ng-valid" type="radio">Custom Permissions
                                         <!-- ngIf: eval.name === 'Company Administrator' --><!-- ngIf: eval.name === 'User' --></label><!-- end ngRepeat: eval in getCustomerDirectoryRoles() -->
                                    </div>
                                    <!-- ngIf: update_user_role.role === 'Custom Permissions' -->
                                </div>
                                <!--<div class="form-group grp_radio">
                                    <div class="col-sm-12">
                                        <input type="radio" name="user_role" class="radio_user_role" ng-model="edit_user_role.office365_user_role" value="user">User</label>
                                    </div>
                                </div>
                                <div class="form-group grp_radio">
                                    <div class="col-sm-12">
                                        <input type="radio" name="user_role" class="radio_user_role" ng-model="edit_user_role.office365_user_role">
                                        <label class="radio_label_user">Global Administrator</label>
                                    </div>
                                </div>
                                <div class="form-group grp_radio">
                                    <div class="col-sm-12">
                                        <input type="radio" name="user_role" class="radio_user_role" ng-model="edit_user_role.office365_user_role" value="limited">Limited Admin Role</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Alternate Email Address:</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="recipient_email" ng-model="edit_user_role.office365_recipient_email">
                                    </div>
                                </div>-->
                                <hr class="reset_hr_line reset_hr_password">
                                <div class="modal-footer a-left form-footer" style="border-top: 0px">
                                    <input class="btn btn-blue" value="Cancel" ng-click="user_tab = 1" type="button">
                                    <input class="btn btn-dark" value="Save" ng-click="user_tab = 1" type="submit">
                                </div>
                            </form>
                        </div>
                        <!--Edit User Role Section Ends-->
                        <!--Edit User Section Starts-->
                        <div class="add_user_account form-container" style="display:none;">
                            <div class="form_heading">
                                <span class="boxhead">EDIT</span>
                                <hr class="boxline">
                            </div>
                            <form name="editUserForm" class="form-horizontal dashboard-form edituser_form">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">First Name:</label>
                                    <div class="col-sm-4">
                                        <input placeholder="" name="first_name" class="form-control"  type="text">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Last Name:</label>
                                    <div class="col-sm-4">
                                        <input placeholder="" name="last_name" class="form-control "  type="text">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"><sup>*</sup>Display Name:</label>
                                    <div class="col-sm-4">
                                        <input placeholder="" class="form-control ng-pristine ng-valid" name="display_name"  type="text">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"><sup>*</sup>User Name:</label>
                                    <div class="col-sm-4">
                                        <input placeholder="" class="form-control ng-pristine ng-valid" name="user_name"  type="text">
                                    </div>
                                    <div class="col-sm-1 edit_splitter"><span class="ng-binding">@</span>
                                    </div>
                                    <!--<div class="col-sm-4">
                                        <select name="domain" ng-model="edit_user.office365_domain_prefix">
                                            <option ng-repeat="individual_domian in domian" >
                                                
                                            </option>
                                        </select>
                                    </div>-->
                                </div>
                                <!--<div class="form-group">
                                <label class="col-sm-2 control-label"><sup>*</sup>Location:</label>
                                    <div class="col-sm-4">
                                        <select style="width: 240px" ng-model="update_user.selected_country" pvp-country-picker required></select>
                                    </div>
                                </div>-->
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"><sup>*</sup>Location:</label>
                                    <div class="col-sm-4">
                                        <select style="width: 240px" ng-model="update_user.selected_country" pvp-country-picker="" ng-options="country.alpha2 as country.name for country in countries" class="ng-scope ng-pristine ng-valid"><option value="?" selected="selected" label=""></option><option value="0" label="Afghanistan">Afghanistan</option><option value="1" label="Åland Islands">Åland Islands</option><option value="2" label="Albania">Albania</option><option value="3" label="Algeria">Algeria</option><option value="4" label="American Samoa">American Samoa</option><option value="5" label="Andorra">Andorra</option><option value="6" label="Angola">Angola</option><option value="7" label="Anguilla">Anguilla</option><option value="8" label="Antarctica">Antarctica</option><option value="9" label="Antigua and Barbuda">Antigua and Barbuda</option><option value="10" label="Argentina">Argentina</option><option value="11" label="Armenia">Armenia</option><option value="12" label="Aruba">Aruba</option><option value="13" label="Australia">Australia</option><option value="14" label="Austria">Austria</option><option value="15" label="Azerbaijan">Azerbaijan</option><option value="16" label="Bahamas (the)">Bahamas (the)</option><option value="17" label="Bahrain">Bahrain</option><option value="18" label="Bangladesh">Bangladesh</option><option value="19" label="Barbados">Barbados</option><option value="20" label="Belarus">Belarus</option><option value="21" label="Belgium">Belgium</option><option value="22" label="Belize">Belize</option><option value="23" label="Benin">Benin</option><option value="24" label="Bermuda">Bermuda</option><option value="25" label="Bhutan">Bhutan</option><option value="26" label="Bolivia (Plurinational State of)">Bolivia (Plurinational State of)</option><option value="27" label="Bonaire, Sint Eustatius and Saba">Bonaire, Sint Eustatius and Saba</option><option value="28" label="Bosnia and Herzegovina">Bosnia and Herzegovina</option><option value="29" label="Botswana">Botswana</option><option value="30" label="Bouvet Island">Bouvet Island</option><option value="31" label="Brazil">Brazil</option><option value="32" label="British Indian Ocean Territory (the)">British Indian Ocean Territory (the)</option><option value="33" label="Brunei Darussalam">Brunei Darussalam</option><option value="34" label="Bulgaria">Bulgaria</option><option value="35" label="Burkina Faso">Burkina Faso</option><option value="36" label="Burundi">Burundi</option><option value="37" label="Cabo Verde">Cabo Verde</option><option value="38" label="Cambodia">Cambodia</option><option value="39" label="Cameroon">Cameroon</option><option value="40" label="Canada">Canada</option><option value="41" label="Cayman Islands (the)">Cayman Islands (the)</option><option value="42" label="Central African Republic (the)">Central African Republic (the)</option><option value="43" label="Chad">Chad</option><option value="44" label="Chile">Chile</option><option value="45" label="China">China</option><option value="46" label="Christmas Island">Christmas Island</option><option value="47" label="Cocos (Keeling) Islands (the)">Cocos (Keeling) Islands (the)</option><option value="48" label="Colombia">Colombia</option><option value="49" label="Comoros (the)">Comoros (the)</option><option value="50" label="Congo (the)">Congo (the)</option><option value="51" label="Congo (the Democratic Republic of the)">Congo (the Democratic Republic of the)</option><option value="52" label="Cook Islands (the)">Cook Islands (the)</option><option value="53" label="Costa Rica">Costa Rica</option><option value="54" label="Côte d'Ivoire">Côte d'Ivoire</option><option value="55" label="Croatia">Croatia</option><option value="56" label="Cuba">Cuba</option><option value="57" label="Curaçao">Curaçao</option><option value="58" label="Cyprus">Cyprus</option><option value="59" label="Czech Republic (the)">Czech Republic (the)</option><option value="60" label="Denmark">Denmark</option><option value="61" label="Djibouti">Djibouti</option><option value="62" label="Dominica">Dominica</option><option value="63" label="Dominican Republic (the)">Dominican Republic (the)</option><option value="64" label="Ecuador">Ecuador</option><option value="65" label="Egypt">Egypt</option><option value="66" label="El Salvador">El Salvador</option><option value="67" label="Equatorial Guinea">Equatorial Guinea</option><option value="68" label="Eritrea">Eritrea</option><option value="69" label="Estonia">Estonia</option><option value="70" label="Ethiopia">Ethiopia</option><option value="71" label="Falkland Islands (the) [Malvinas]">Falkland Islands (the) [Malvinas]</option><option value="72" label="Faroe Islands (the)">Faroe Islands (the)</option><option value="73" label="Fiji">Fiji</option><option value="74" label="Finland">Finland</option><option value="75" label="France">France</option><option value="76" label="French Guiana">French Guiana</option><option value="77" label="French Polynesia">French Polynesia</option><option value="78" label="French Southern Territories (the)">French Southern Territories (the)</option><option value="79" label="Gabon">Gabon</option><option value="80" label="Gambia (the)">Gambia (the)</option><option value="81" label="Georgia">Georgia</option><option value="82" label="Germany">Germany</option><option value="83" label="Ghana">Ghana</option><option value="84" label="Gibraltar">Gibraltar</option><option value="85" label="Greece">Greece</option><option value="86" label="Greenland">Greenland</option><option value="87" label="Grenada">Grenada</option><option value="88" label="Guadeloupe">Guadeloupe</option><option value="89" label="Guam">Guam</option><option value="90" label="Guatemala">Guatemala</option><option value="91" label="Guernsey">Guernsey</option><option value="92" label="Guinea">Guinea</option><option value="93" label="Guinea-Bissau">Guinea-Bissau</option><option value="94" label="Guyana">Guyana</option><option value="95" label="Haiti">Haiti</option><option value="96" label="Heard Island and McDonald Islands">Heard Island and McDonald Islands</option><option value="97" label="Holy See (the)">Holy See (the)</option><option value="98" label="Honduras">Honduras</option><option value="99" label="Hong Kong">Hong Kong</option><option value="100" label="Hungary">Hungary</option><option value="101" label="Iceland">Iceland</option><option value="102" label="India">India</option><option value="103" label="Indonesia">Indonesia</option><option value="104" label="Iran (Islamic Republic of)">Iran (Islamic Republic of)</option><option value="105" label="Iraq">Iraq</option><option value="106" label="Ireland">Ireland</option><option value="107" label="Isle of Man">Isle of Man</option><option value="108" label="Israel">Israel</option><option value="109" label="Italy">Italy</option><option value="110" label="Jamaica">Jamaica</option><option value="111" label="Japan">Japan</option><option value="112" label="Jersey">Jersey</option><option value="113" label="Jordan">Jordan</option><option value="114" label="Kazakhstan">Kazakhstan</option><option value="115" label="Kenya">Kenya</option><option value="116" label="Kiribati">Kiribati</option><option value="117" label="Korea (the Democratic People's Republic of)">Korea (the Democratic People's Republic of)</option><option value="118" label="Korea (the Republic of)">Korea (the Republic of)</option><option value="119" label="Kuwait">Kuwait</option><option value="120" label="Kyrgyzstan">Kyrgyzstan</option><option value="121" label="Lao People's Democratic Republic (the)">Lao People's Democratic Republic (the)</option><option value="122" label="Latvia">Latvia</option><option value="123" label="Lebanon">Lebanon</option><option value="124" label="Lesotho">Lesotho</option><option value="125" label="Liberia">Liberia</option><option value="126" label="Libya">Libya</option><option value="127" label="Liechtenstein">Liechtenstein</option><option value="128" label="Lithuania">Lithuania</option><option value="129" label="Luxembourg">Luxembourg</option><option value="130" label="Macao">Macao</option><option value="131" label="Macedonia (the former Yugoslav Republic of)">Macedonia (the former Yugoslav Republic of)</option><option value="132" label="Madagascar">Madagascar</option><option value="133" label="Malawi">Malawi</option><option value="134" label="Malaysia">Malaysia</option><option value="135" label="Maldives">Maldives</option><option value="136" label="Mali">Mali</option><option value="137" label="Malta">Malta</option><option value="138" label="Marshall Islands (the)">Marshall Islands (the)</option><option value="139" label="Martinique">Martinique</option><option value="140" label="Mauritania">Mauritania</option><option value="141" label="Mauritius">Mauritius</option><option value="142" label="Mayotte">Mayotte</option><option value="143" label="Mexico">Mexico</option><option value="144" label="Micronesia (Federated States of)">Micronesia (Federated States of)</option><option value="145" label="Moldova (the Republic of)">Moldova (the Republic of)</option><option value="146" label="Monaco">Monaco</option><option value="147" label="Mongolia">Mongolia</option><option value="148" label="Montenegro">Montenegro</option><option value="149" label="Montserrat">Montserrat</option><option value="150" label="Morocco">Morocco</option><option value="151" label="Mozambique">Mozambique</option><option value="152" label="Myanmar">Myanmar</option><option value="153" label="Namibia">Namibia</option><option value="154" label="Nauru">Nauru</option><option value="155" label="Nepal">Nepal</option><option value="156" label="Netherlands (the)">Netherlands (the)</option><option value="157" label="New Caledonia">New Caledonia</option><option value="158" label="New Zealand">New Zealand</option><option value="159" label="Nicaragua">Nicaragua</option><option value="160" label="Niger (the)">Niger (the)</option><option value="161" label="Nigeria">Nigeria</option><option value="162" label="Niue">Niue</option><option value="163" label="Norfolk Island">Norfolk Island</option><option value="164" label="Northern Mariana Islands (the)">Northern Mariana Islands (the)</option><option value="165" label="Norway">Norway</option><option value="166" label="Oman">Oman</option><option value="167" label="Pakistan">Pakistan</option><option value="168" label="Palau">Palau</option><option value="169" label="Palestine, State of">Palestine, State of</option><option value="170" label="Panama">Panama</option><option value="171" label="Papua New Guinea">Papua New Guinea</option><option value="172" label="Paraguay">Paraguay</option><option value="173" label="Peru">Peru</option><option value="174" label="Philippines (the)">Philippines (the)</option><option value="175" label="Pitcairn">Pitcairn</option><option value="176" label="Poland">Poland</option><option value="177" label="Portugal">Portugal</option><option value="178" label="Puerto Rico">Puerto Rico</option><option value="179" label="Qatar">Qatar</option><option value="180" label="Réunion">Réunion</option><option value="181" label="Romania">Romania</option><option value="182" label="Russian Federation (the)">Russian Federation (the)</option><option value="183" label="Rwanda">Rwanda</option><option value="184" label="Saint Barthélemy">Saint Barthélemy</option><option value="185" label="Saint Helena, Ascension and Tristan da Cunha">Saint Helena, Ascension and Tristan da Cunha</option><option value="186" label="Saint Kitts and Nevis">Saint Kitts and Nevis</option><option value="187" label="Saint Lucia">Saint Lucia</option><option value="188" label="Saint Martin (French part)">Saint Martin (French part)</option><option value="189" label="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option><option value="190" label="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option><option value="191" label="Samoa">Samoa</option><option value="192" label="San Marino">San Marino</option><option value="193" label="Sao Tome and Principe">Sao Tome and Principe</option><option value="194" label="Saudi Arabia">Saudi Arabia</option><option value="195" label="Senegal">Senegal</option><option value="196" label="Serbia">Serbia</option><option value="197" label="Seychelles">Seychelles</option><option value="198" label="Sierra Leone">Sierra Leone</option><option value="199" label="Singapore">Singapore</option><option value="200" label="Sint Maarten (Dutch part)">Sint Maarten (Dutch part)</option><option value="201" label="Slovakia">Slovakia</option><option value="202" label="Slovenia">Slovenia</option><option value="203" label="Solomon Islands">Solomon Islands</option><option value="204" label="Somalia">Somalia</option><option value="205" label="South Africa">South Africa</option><option value="206" label="South Georgia and the South Sandwich Islands">South Georgia and the South Sandwich Islands</option><option value="207" label="South Sudan ">South Sudan </option><option value="208" label="Spain">Spain</option><option value="209" label="Sri Lanka">Sri Lanka</option><option value="210" label="Sudan (the)">Sudan (the)</option><option value="211" label="Suriname">Suriname</option><option value="212" label="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option><option value="213" label="Swaziland">Swaziland</option><option value="214" label="Sweden">Sweden</option><option value="215" label="Switzerland">Switzerland</option><option value="216" label="Syrian Arab Republic">Syrian Arab Republic</option><option value="217" label="Taiwan (Province of China)">Taiwan (Province of China)</option><option value="218" label="Tajikistan">Tajikistan</option><option value="219" label="Tanzania, United Republic of">Tanzania, United Republic of</option><option value="220" label="Thailand">Thailand</option><option value="221" label="Timor-Leste">Timor-Leste</option><option value="222" label="Togo">Togo</option><option value="223" label="Tokelau">Tokelau</option><option value="224" label="Tonga">Tonga</option><option value="225" label="Trinidad and Tobago">Trinidad and Tobago</option><option value="226" label="Tunisia">Tunisia</option><option value="227" label="Turkey">Turkey</option><option value="228" label="Turkmenistan">Turkmenistan</option><option value="229" label="Turks and Caicos Islands (the)">Turks and Caicos Islands (the)</option><option value="230" label="Tuvalu">Tuvalu</option><option value="231" label="Uganda">Uganda</option><option value="232" label="Ukraine">Ukraine</option><option value="233" label="United Arab Emirates (the)">United Arab Emirates (the)</option><option value="234" label="United Kingdom of Great Britain and Northern Ireland (the)">United Kingdom of Great Britain and Northern Ireland (the)</option><option value="235" label="United States Minor Outlying Islands (the)">United States Minor Outlying Islands (the)</option><option value="236" label="United States of America (the)">United States of America (the)</option><option value="237" label="Uruguay">Uruguay</option><option value="238" label="Uzbekistan">Uzbekistan</option><option value="239" label="Vanuatu">Vanuatu</option><option value="240" label="Venezuela (Bolivarian Republic of)">Venezuela (Bolivarian Republic of)</option><option value="241" label="Vietnam">Vietnam</option><option value="242" label="Virgin Islands (British)">Virgin Islands (British)</option><option value="243" label="Virgin Islands (U.S.)">Virgin Islands (U.S.)</option><option value="244" label="Wallis and Futuna">Wallis and Futuna</option><option value="245" label="Western Sahara">Western Sahara</option><option value="246" label="Yemen">Yemen</option><option value="247" label="Zambia">Zambia</option><option value="248" label="Zimbabwe">Zimbabwe</option></select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Additional Details:</label>
                                    <div class="col-sm-4">
                                        <input class="form-control ng-pristine ng-valid" name="additional_details" ng-model="update_user.additional_details" type="text">
                                    </div>
                                </div>
                                <hr class="reset_hr_line reset_hr_password">
                                <div class="modal-footer a-left form-footer" style="border-top: 0px">
                                    <input class="btn btn-blue" value="Cancel" ng-click="user_tab = 1" type="button">
                                    <input class="btn btn-dark" value="Save" ng-click="user_tab = 1" type="submit">
                                </div>
                            </form>
                        </div>
                        <!--Edit User Section Ends-->
                        <!--Assign License Section Starts-->
                        <div class="add_user_account" style="display:none;">
                            <div class="form_heading">
                                <span class="boxhead">ASSIGN LICENSE</span>
                                <hr class="boxline">
                            </div>
                            <div class="subtitle_license">
                                Different services are available in different locations.
                            </div>
                            <div class="dropdown_view_listing">
                                <div class="col-sm-6 dropdown_o3652">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Select User Locations:</label>
                                        <div class="col-md-5">
                                            <span class="dropdown_o365_locations">
                                    <select class="form-control">
                                      <option value="0" selected="selected">Singapore</option>
                                      <option value="1">Sign-in Allowed Users</option>
                                      <option value="2">Sign-in Blocked Users</option>
                                      <option value="3">Unlicensed Users</option>
                                      <option value="4">Users With Errors</option>
                                    </select>
                                </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group dropdown_o365_licenses">
                                        <label class="control-label col-md-3">Select Licenses:</label>
                                        <div class="show_license_div col-md-8">
                                            <div class="license_selected_div" id="license_all_selected_div">All Licenses</div>
                                            <div class="selected_checkbox" id="selected_checkbox_license" style="display:none;">
                                                <ul class="selCheck">
                                                    <li>
                                                        <input type="checkbox"><span>Visio Pro for Office365</span>
                                                        <div class="select_assign_license">
                                                            <span class="info_licenses">
                                           2 of 13 licenses available
                                         </span>
                                                            <span class="buy_more">
                                            <a href="">Buy More</a>
                                         </span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <input type="checkbox"><span>Project Pro for Office365</span>
                                                        <div class="select_assign_license">
                                                            <span class="info_licenses">
                                           2 of 13 licenses available
                                         </span>
                                                            <span class="buy_more">
                                            <a href="">Buy More</a>
                                         </span>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <hr class="reset_hr_line">
                                                <div class="show_button_dropdown">
                                                    <input class="btn btn-primary" value="Cancel" ng-click="user_tab = 1" type="button">
                                                    <input class="btn btn-dark" value="Save" type="submit">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="panel-body">
                                    <table class="table table-responsive tab_assign_license">
                                        <tbody>
                                            <!-- ngRepeat: assign_license in licenses_val -->
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!--- Assign License Section Ends --->
                    </section>
                </div>
            </div>
        </div>
        <!---- Second Tab Starts ---->
        <!---- Third Tab Starts ---->
        <div id="tab-3" class="tab-content">
            <div class="col-xsm-12 col-lg-12">
                <div class="mini-box tab_licenses">
                    <div class="row">
                        <div class="panel-body">
                            <div class="table-responsive dashboard-table col-md-12">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Assigned To</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <span class="ng-binding">
                                                        Office 365 Enterprise E1
                                                    </span>
                                            </td>
                                            <td>
                                                <span class="ng-binding">
                                                 attempt3@sdtest01234.onmicrosoft.com
                                                    </span>
                                            </td>
                                        </tr>
										<tr>
                                            <td>
                                                <span class="ng-binding">
                                                        Office 365 Enterprise E1
                                                    </span>
                                            </td>
                                            <td>
                                                <span class="ng-binding">
                                                 attempt3@sdtest01234.onmicrosoft.com
                                                    </span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		
		</div>
        <!---- Third Tab Starts ---->
        </section>
    </div>
</div>
	
	
	
	
	
	
	
	
  </div>
		
	
	</div>
	
	
	</div>
	</div>
	
	
	
	
  