 {if $start!=$stop}
    <ul class="pagination page_update">
        {if $p != 1 && $p}
            {assign var='p_previous' value=$p-1}
            <li id="pagination_previous{if isset($paginationId)}_{$paginationId}{/if}" class="pagination_previous">
                <a rel="nofollow" href="{$requestPage}">
                     <b>{l s='First'}</b>
                </a>
            </li>
            <li id="pagination_previous{if isset($paginationId)}_{$paginationId}{/if}" class="pagination_previous">
                <a rel="nofollow" href="{$requestPage}&p={$prev_p}">
                    <i class="icon-chevron-left"></i> <b>{l s='Previous'}</b>
                </a>
            </li>
        {else}
            <li id="pagination_previous{if isset($paginationId)}_{$paginationId}{/if}" class="disabled pagination_previous">
                <span>
                     <b>{l s='First'}</b>
                </span>
            </li>
            <li id="pagination_previous{if isset($paginationId)}_{$paginationId}{/if}" class="disabled pagination_previous">
                <span>
                    <i class="icon-chevron-left"></i> <b>{l s='Previous'}</b>
                </span>
            </li>
        {/if}
        {if $start==3}
            <li>
                <a rel="nofollow"  href="{$link->goPage($requestPage, 1)}">
                    <span>1</span>
                </a>
            </li>
            <li>
                <a rel="nofollow"  href="{$link->goPage($requestPage, 2)}">
                    <span>2</span>
                </a>
            </li>
        {/if}
        {if $start==2}
            <li>
                <a rel="nofollow"  href="{$link->goPage($requestPage, 1)}">
                    <span>1</span>
                </a>
            </li>
        {/if}
        {if $start>3}
            <li>
                <a rel="nofollow"  href="{$link->goPage($requestPage, 1)}">
                    <span>1</span>
                </a>
            </li>
            <li class="truncate">
                <span>
                    <span>...</span>
                </span>
            </li>
        {/if}
        {section name=pagination start=$start loop=$stop+1 step=1}
            {if $p == $smarty.section.pagination.index}
                <li class="active current">
                    <span>
                        <span>{$p|escape:'html':'UTF-8'}</span>
                    </span>
                </li>
            {else}
                <li>
                    <a rel="nofollow" href="{$link->goPage($requestPage, $smarty.section.pagination.index)}">
                        <span>{$smarty.section.pagination.index|escape:'html':'UTF-8'}</span>
                    </a>
                </li>
            {/if}
        {/section}
        {if $pages_nb>$stop+2}
            <li class="truncate">
                <span>
                    <span>...</span>
                </span>
            </li>
            <li>
                <a href="{$link->goPage($requestPage, $pages_nb)}">
                    <span>{$pages_nb|intval}</span>
                </a>
            </li>
        {/if}
        {if $pages_nb==$stop+1}
            <li>
                <a href="{$link->goPage($requestPage, $pages_nb)}">
                    <span>{$pages_nb|intval}</span>
                </a>
            </li>
        {/if}
        {if $pages_nb==$stop+2}
            <li>
                <a href="{$link->goPage($requestPage, $pages_nb-1)}">
                    <span>{$pages_nb-1|intval}</span>
                </a>
            </li>
            <li>
                <a href="{$link->goPage($requestPage, $pages_nb)}">
                    <span>{$pages_nb|intval}</span>
                </a>
            </li>
        {/if}
        {if $pages_nb > 1 AND $p != $pages_nb}
            {assign var='p_next' value=$p+1}
            <li id="pagination_next{if isset($paginationId)}_{$paginationId}{/if}" class="pagination_next">
                <a rel="nofollow" href="{$requestPage}&p={$next_p}">
                    <b>{l s='Next'}</b> <i class="icon-chevron-right"></i>
                </a>
            </li>
            <li id="pagination_next{if isset($paginationId)}_{$paginationId}{/if}" class="pagination_next">
                <a rel="nofollow" href="{$requestPage}&p={$pages_nb}">
                    <b>{l s='Last'}</b>
                </a>
            </li>
        {else}
            <li id="pagination_next{if isset($paginationId)}_{$paginationId}{/if}" class="disabled pagination_next">
                <span>
                    <b>{l s='Next'}</b> <i class="icon-chevron-right"></i>
                </span>
            </li>
            <li id="pagination_next{if isset($paginationId)}_{$paginationId}{/if}" class="disabled pagination_next">
                <span>
                    <b>{l s='Last'}</b>
                </span>
            </li>
        {/if}
    </ul>
{/if}