
	<script type='text/javascript'>
//<![CDATA[
$(window).load(function(){
$(document).ready(function() {
    $(".tabs-menu a").click(function(event) {
        event.preventDefault();
        $(this).parent().addClass("current");
        $(this).parent().siblings().removeClass("current");
        var tab = $(this).attr("href");
        $(".tab-content").not(tab).css("display", "none");
       $(".inner-tab-content").not(tab).css("display", "none");
		$(".listing_user").css('display', 'block');
        <!-- $(".tab-content").firstChild('div.nav-wrapper').next(div).attr("display", "block"); -->
        <!-- $(this).div('.nav-wrapper').next.css("display", "block"); -->
		
        $(tab).fadeIn();
    });

    $(".inner-tabs-menu a").click(function(event) {
        event.preventDefault();
        $(this).parent().addClass("current_1");
        $(this).parent().siblings().removeClass("current_1");
        var tab = $(this).attr("href");
       $(".inner-tab-content").not(tab).css("display", "none");
        $(".listing_user").css('display', 'none');
        $(tab).fadeIn();
    });
});
});

//]]> 

</script>
	
    
	<div id="columns" class="container">
	<div class="row">
             
<div id="left_column" class="column col-xs-12 col-sm-3">
	{include file="{$one_var}control_panel/sidebar.tpl"}
</div>

<div id="center_column" class="center_column col-xs-12 col-sm-9">
		
		
    <div class="row">         
                    <div class="col-xs-12 col-lg-12 themed-nav">
						 <div class="box-title">
          <p>Service Request</p>
        </div>
      <div class="mini-box">
        
        <div class="panel-header">
         <aside data-ng-include=" 'views/virtual_machines/virtual_machine_nav.html' " id="nav-container" class="nav-container ng-scope nav-fixed nav-horizontal nav-vertical" data-ng-class="">
		
        <div class="nav-wrapper">
            <ul id="nav" class="nav ng-scope" data-ng-controller="NavCtrl" data-collapse-nav="" data-highlight-active="">
                <li class="">
                    
                    <a href="#"><i class="fa fa-plus"></i><span>New</span></a>
                </li>
                
                <li>
                    <a href="#" ><i class="fa fa-trash-o"></i><span>Delete</span></a>
                    
            
                </li>
                
                <li>
                    <a href="#" >
						<i class="fa fa-file-excel-o" aria-hidden="true"></i>

                        <span data-i18n="Generate Excel">Generate Excel</span> 
                    </a>
                </li>
                <li>
                    <a href="#" >
						<i class="fa fa-file-pdf-o" aria-hidden="true"></i>

                        <span data-i18n="Generate PDF">Generate PDF</span> 
                    </a>
                </li>
            </ul>
        </div>    
     


  </aside>

        </div>
        <div class="divider divider"></div>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        <div class="mini-box">
                            <div class="row">
                                <div class="panel-body" >
                                    <div class="table-responsive dashboard-table col-md-12">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <input ng-model="selectAll" ng-click="oval.isAll()" id="checkedAllSubscription" class="ng-pristine ng-valid" type="checkbox">
                                                    </th>
                                                        <th>Number</th>
                                                        <th style="">Caller</th>
                                                        <th style="">Category</th>
                                                        <th style="">Impact</th>
                                                        <th style="">Priority</th>
                                                        <th style="">Escalation</th>
                                                        <th style="">State</th>
                                                        <th style="">Assignment Group</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <input ng-model="sub.selected" ng-checked="selectAll" ng-click="selectedSubscription(sub)" id="subscription_83AE0045-E0A6-49A5-B123-1652FFE1D9F1" class="ng-pristine ng-valid" type="checkbox">
                                                        </td>
                                                        <td>
                                                            <p>SRM0007842</p>
                                                        </td>
                                                        <td>
                                                            <p>System Administrator</p>
                                                        </td>
                                                        <td>
                                                            <p>Inquiry/Help</p>
                                                        </td>
                                                        <td>
                                                            <p>3 - Moderate</p>
                                                        </td>
                                                        <td>
                                                            <p>4 - Low</p>
                                                        </td>
                                                        <td>
                                                            <p>Normal</p>
                                                        </td>
                                                        <td>
                                                            <p>New</p>
                                                        </td>
                                                        <td>
                                                            <p>Service Desk</p>

                                                        </td>
                                                        
                                                        
                                                    </tr>
										            <tr>
                                                        <td>
                                                            <input ng-model="sub.selected" ng-checked="selectAll" ng-click="selectedSubscription(sub)" id="subscription_83AE0045-E0A6-49A5-B123-1652FFE1D9F1" class="ng-pristine ng-valid" type="checkbox">
                                                        </td>
                                                         <td>
                                                            <p>SRM0003557</p>
                                                        </td>
                                                        <td>
                                                            <p>Lucas Bragoli</p>
                                                        </td>
                                                        <td>
                                                            <p>Inquiry/Help</p>
                                                        </td>
                                                        <td>
                                                            <p>3 - Moderate</p>
                                                        </td>
                                                        <td>
                                                            <p>4 - Low</p>
                                                        </td>
                                                        <td>
                                                            <p>Normal</p>
                                                        </td>
                                                        <td>
                                                            <p>Awaiting Problem</p>
                                                        </td>
                                                        <td>
                                                            <p>Service Desk</p>

                                                        </td>
                                                        
                                                        
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input ng-model="sub.selected" ng-checked="selectAll" ng-click="selectedSubscription(sub)" id="subscription_83AE0045-E0A6-49A5-B123-1652FFE1D9F1" class="ng-pristine ng-valid" type="checkbox">
                                                        </td>
                                                         <td>
                                                            <p>SRM0001114</p>
                                                        </td>
                                                        <td>
                                                            <p>System Administrator</p>
                                                        </td>
                                                        <td>
                                                            <p>Inquiry/Help</p>
                                                        </td>
                                                        <td>
                                                            <p>3 - Moderate</p>
                                                        </td>
                                                        <td>
                                                            <p>1 - Critical</p>
                                                        </td>
                                                        <td>
                                                            <p>High</p>
                                                        </td>
                                                        <td>
                                                            <p>New</p>
                                                        </td>
                                                        <td>
                                                            <p>Service Desk</p>

                                                        </td>
                                                        
                                                        
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input ng-model="sub.selected" ng-checked="selectAll" ng-click="selectedSubscription(sub)" id="subscription_83AE0045-E0A6-49A5-B123-1652FFE1D9F1" class="ng-pristine ng-valid" type="checkbox">
                                                        </td>
                                                         <td>
                                                            <p>SRM0009115</p>
                                                        </td>
                                                        <td>
                                                            <p>Joe Employee</p>
                                                        </td>
                                                        <td>
                                                            <p>Software</p>
                                                        </td>
                                                        <td>
                                                            <p>3 - Moderate</p>
                                                        </td>
                                                        <td>
                                                            <p>4 - Low</p>
                                                        </td>
                                                        <td>
                                                            <p>Normal</p>
                                                        </td>
                                                        <td>
                                                            <p>awaiting Problem</p>
                                                        </td>
                                                        <td>
                                                            <p>IT - Information Technology</p>

                                                        </td>
                                                        
                                                        
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input ng-model="sub.selected" ng-checked="selectAll" ng-click="selectedSubscription(sub)" id="subscription_83AE0045-E0A6-49A5-B123-1652FFE1D9F1" class="ng-pristine ng-valid" type="checkbox">
                                                        </td>
                                                         <td>
                                                            <p>SRM0003355</p>
                                                        </td>
                                                        <td>
                                                            <p>Joe Employee</p>
                                                        </td>
                                                        <td>
                                                            <p>Hardware</p>
                                                        </td>
                                                        <td>
                                                            <p>3 - Moderate</p>
                                                        </td>
                                                        <td>
                                                            <p>4 - Low</p>
                                                        </td>
                                                        <td>
                                                            <p>Moderate</p>
                                                        </td>
                                                        <td>
                                                            <p>New</p>
                                                        </td>
                                                        <td>
                                                            <p>Service Desk</p>

                                                        </td>
                                                        
                                                        
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input ng-model="sub.selected" ng-checked="selectAll" ng-click="selectedSubscription(sub)" id="subscription_83AE0045-E0A6-49A5-B123-1652FFE1D9F1" class="ng-pristine ng-valid" type="checkbox">
                                                        </td>
                                                         <td>
                                                            <p>SRM0005598</p>
                                                        </td>
                                                        <td>
                                                            <p>Annie Approver</p>
                                                        </td>
                                                        <td>
                                                            <p>Inquiry/Help</p>
                                                        </td>
                                                        <td>
                                                            <p>3 - Moderate</p>
                                                        </td>
                                                        <td>
                                                            <p>4 - Low</p>
                                                        </td>
                                                        <td>
                                                            <p>Moderate</p>
                                                        </td>
                                                        <td>
                                                            <p>New</p>
                                                        </td>
                                                        <td>
                                                            <p>Computer Support</p>

                                                        </td>
                                                        
                                                        
                                                    </tr>
                                                   
													
													
													
													
													
                                        </tbody>
                                        </table>
                                    </div>
                                </div>
								
                        
					   </div>
                        
                    </div>
                </div>
        
        
    


	
	
	
	
	
	
	
	
  </div>
		
	
	</div>
	
	
	</div>
	</div>
	
	
	
	
  