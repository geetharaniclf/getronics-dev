<section class="sidebar">

	
    <ul id="nav" class="sidebar-menu ng-scope" data-ng-controller="NavCtrl" data-collapse-nav="">
		<li class="header">MAIN NAVIGATION</li>
		
                
                <li {if $page==''}class='active'{/if} ng-class="getClass('/');" class="">
            
                <a href="ctrlpanel">
				<i class="fa fa-windows"></i> 
				<span data-i18n="O365 MANAGEMENT">{l s='DASHBOARD'}</span>
            </a>
        </li>
		
		<!-- <li {if $page=='tenant_management'}class='active'{/if} ng-class="getClass('/tenant');" class="">
            <a href="?page=tenant_management">
                <i class="fa fa-plus"></i> 
                
				<span data-i18n="TENANT MANAGEMENT">{l s='TENANT MANAGEMENT'}</span>
            </a>
        </li>-->
        
            
        
		<li {if $page=='o365'}class='active'{/if}  ng-class="getClass('/o365');">
            <a href="?page=o365">
				<i class="fa fa-windows"></i> 
				<span data-i18n="O365 MANAGEMENT">{l s='O365 MANAGEMENT'}</span>
            </a>
        </li>
        <li {if $page=='virtual_data_center'}class='active'{/if}>
            <a href="?page=virtual_data_center">
			<i class="fa fa-cloud-upload"></i>
			<span data-i18n="VIRTUAL MACHINE">{l s='VIRTUAL DATACENTER'}</span></a>
             
        </li>
		<li {if $page=='virtual_machines'}class='active'{/if}  ng-class="getClass('/virtual_machines');">
            <a href="?page=virtual_machines">
			<i class="fa fa-desktop"></i>
			<span data-i18n="VIRTUAL MACHINE">{l s='VIRTUAL MACHINE'}</span></a>
             
        </li>
		<!-- ngIf: showUsersTab --><li {if $page=='users'}class='active'{/if} ng-if="showUsersTab" ng-class="getClass('/users');" class="ng-scope">
            <a href="?page=users">
			<i class="fa fa-user"></i> 
			<span data-i18n="USER MANAGEMENT">{l s='USER MANAGEMENT'}</span></a>
            
        </li><!-- end ngIf: showUsersTab -->
		 <!-- <li {if $page=='circuit_unify'}class='active'{/if} ng-class="getClass('/circuit');">
            <a href="?page=circuit_unify">
                <i class="fa fa-line-chart"></i> 
				<span data-i18n="CIRCUIT UNIFY">{l s='CIRCUIT UNIFY'}</span>
            </a>
        </li>-->
        <!-- <li {if $page=='reseller_unify'}class='active'{/if} ng-class="getClass('/reseller');">
            <a href="?page=reseller_unify">
                
             <i class="fa fa-id-card" aria-hidden="true"></i>
				<span data-i18n="RESELLER UNIFY">{l s='RESELLER UNIFY'}</span>
            </a>
        </li> -->
        
		<li {if $page=='support'}class='active'{/if} ng-class="getClass('/billing');">
            <a href="?page=support">
			<i class="fa fa-phone-square"></i> 
			<span data-i18n="USERS">{l s='BILLING SUPPORT'}</span></a>
        </li>
        
		<li {if $page=='billing'}class='active'{/if} ng-class="getClass('/billing');">
            <a href="?page=billing">  
                <i class="fa fa-usd" aria-hidden="true"></i>
				<span data-i18n="BILLING">{l s='BILLING OVERVIEW'}</span> 
            </a>
        </li>
        
        <li {if $page=='waas_management'}class='active'{/if}>
            <a href="?page=waas_management">  
                <i class="fa fa-laptop" aria-hidden="true"></i>
				<span data-i18n="BILLING">{l s='DIGITAL WORKSPACE'}</span> 
            </a>
        </li>
        
        <li {if $page=='service_request'}class='active'{/if} >
            <a href="?page=service_request">  
                <i class="fa fa-phone-square" aria-hidden="true"></i>
				<span data-i18n="BILLING">{l s='SERVICE REQUEST'}</span> 
            </a>
        </li>
		<li {if $page=='monitoring'}class='active'{/if}>
            <a href="?page=monitoring">  
                <i class="fa fa-desktop" aria-hidden="true"></i>
				<span data-i18n="BILLING">{l s='SYSTEM MONITORING'}</span> 
            </a>
        </li>
		<li {if $page=='invoices'}class='active'{/if}>
            <a href="?page=invoices">  
                <i class="fa fa-usd" aria-hidden="true"></i>
				<span data-i18n="BILLING">{l s='ACCOUNT INVOICES'}</span> 
            </a>
        </li>   
		
        <!--<li  ng-class="getClass('/dashboard');">
            <a href="#/dashboard">  
                <i class="fa fa-dashboard"></i>
				<span data-i18n="DASHBOARD"></span> 
            </a>
        </li>
         
        <li ng-class="getClass('/virtual_data_center');">
            <a href="#/virtual_data_center/virtual_data_center">
			<i class="fa fa-cloud-upload"></i>
			<span data-i18n="VIRTUAL DATACENTER"></span></a>
            
        </li>
         
        <li ng-class="getClass('/virtual_machines');">
            <a  href="#/virtual_machines/virtual_machines">
			<i class="fa fa-desktop"></i>
			<span data-i18n="VIRTUAL MACHINE"></span></a>
             
        </li> -->
          
        
         <!--
        <li ng-class="getClass('/servicerequest');">
            <a href="#/servicerequest/servicerequest" >
                <i class="fa fa-phone-square" aria-hidden="true"></i>
				<span data-i18n="SUPPORT"></span>
            </a>
        </li> -->

        
        <!--<li ng-class="getClass('/iaas');">
            <a href="#/iaas/billing" >
                <i class="fa fa-dollar"></i> 
				<span data-i18n="IAAS BILLING"></span>
            </a>
        </li>-->
        
       
       
        
        
    </ul>
	</section>