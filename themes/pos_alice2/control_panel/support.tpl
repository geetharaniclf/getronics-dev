    
	<div id="columns" class="container">
	<div class="row">
             
<div id="left_column" class="column col-xs-12 col-sm-3">
	{include file="{$one_var}control_panel/sidebar.tpl"}
</div>

<div id="center_column" class="center_column col-xs-12 col-sm-9">
		
		
		<div class="row">
    <div class="col-lg-12 themed-nav virtual-machines-main">
      <div class="box-title">
          <p>Support</p>
        </div>
      <div class="mini-box">
        
        <div class="panel-header">
         <aside data-ng-include=" 'views/virtual_machines/virtual_machine_nav.html' " id="nav-container" class="nav-container ng-scope nav-fixed nav-horizontal nav-vertical" data-ng-class="">
		
        <div class="nav-wrapper">
            <ul id="nav" class="nav ng-scope" data-ng-controller="NavCtrl" data-collapse-nav="" data-highlight-active="">
                <li class="">
                    
                    <a href="#"><i class="fa fa-plus"></i><span>New</span></a>
                </li>
                
                <li>
                    <a href="#" ><i class="fa fa-trash-o"></i><span>Delete</span></a>
                    
            
                </li>
                
                <li>
                    <a href="#" >
						<i class="fa fa-file-excel-o" aria-hidden="true"></i>

                        <span data-i18n="Generate Excel">Generate Excel</span> 
                    </a>
                </li>
                <li>
                    <a href="#" >
						<i class="fa fa-file-pdf-o" aria-hidden="true"></i>

                        <span data-i18n="Generate PDF">Generate PDF</span> 
                    </a>
                </li>
            </ul>
        </div>    
     


  </aside>

        </div>
        <div class="divider divider"></div>


       

        <div class="panel-body">
          
		
		  <div id="exportable">
	<div class="table-responsive" >
		<table class="table"  cellpadding="0" cellspacing="0">
			<thead>
                            <tr>
                                <th width="4%"><input ng-model="selectAll" id="selectAllRecords"  class="ng-pristine ng-valid" type="checkbox">
                                </th>
                                <th width="18%">Reference No.</th>
                                <th width="18%">Subject</th>
                                <th width="20%">Summary</th>
                                <th width="10%">Status</th>
                                <th width="15%">Creation Date</th>
                                <th width="15%">Last Update</th> 
                                
                            </tr> 
                            </thead> 
			  <tbody>
                <tr > 
                                    <td>
                                        <input   id="service_request" class="ng-pristine ng-valid" type="checkbox">
                                    </td> 
                                    <td class="ng-binding">
                                        REF#1000107 
                                    </td> 
                                    
                                   <td  class="ng-scope">
                                     Technical/Order Query
                                    </td> 
                                    <td class="ng-binding">
                                        Incorrect Bill
                                    </td>
                                    <td class="ng-binding">
                                        Submitted
                                    </td>
                                    
                                    <td ng-bind="serviceRequest.created_at | date:'dd/MM/yyyy'" class="ng-binding">23/11/2016</td>
                                    <td ng-bind="serviceRequest.updated_at | date:'dd/MM/yyyy'" class="ng-binding">23/11/2016</td>
                                    
                                </tr>
								 <tr > 
                                    <td>
                                        <input   id="service_request" class="ng-pristine ng-valid" type="checkbox">
                                    </td> 
                                    <td class="ng-binding">
                                        REF#1000107 
                                    </td> 
                                    
                                   <td  class="ng-scope">
                                     Technical/Order Query
                                    </td> 
                                    <td class="ng-binding">
                                        Incorrect Bill
                                    </td>
                                    <td class="ng-binding">
                                        Submitted
                                    </td>
                                    
                                    <td ng-bind="serviceRequest.created_at | date:'dd/MM/yyyy'" class="ng-binding">23/11/2016</td>
                                    <td ng-bind="serviceRequest.updated_at | date:'dd/MM/yyyy'" class="ng-binding">23/11/2016</td>
                                    
                                </tr>
								 <tr > 
                                    <td>
                                        <input   id="service_request" class="ng-pristine ng-valid" type="checkbox">
                                    </td> 
                                    <td class="ng-binding">
                                        REF#1000107 
                                    </td> 
                                    
                                   <td  class="ng-scope">
                                     Technical/Order Query
                                    </td> 
                                    <td class="ng-binding">
                                        Incorrect Bill
                                    </td>
                                    <td class="ng-binding">
                                        Submitted
                                    </td>
                                    
                                    <td ng-bind="serviceRequest.created_at | date:'dd/MM/yyyy'" class="ng-binding">23/11/2016</td>
                                    <td ng-bind="serviceRequest.updated_at | date:'dd/MM/yyyy'" class="ng-binding">23/11/2016</td>
                                    
                                </tr>
								 <tr > 
                                    <td>
                                        <input   id="service_request" class="ng-pristine ng-valid" type="checkbox">
                                    </td> 
                                    <td class="ng-binding">
                                        REF#1000107 
                                    </td> 
                                    
                                   <td  class="ng-scope">
                                     Technical/Order Query
                                    </td> 
                                    <td class="ng-binding">
                                        Incorrect Bill
                                    </td>
                                    <td class="ng-binding">
                                        Submitted
                                    </td>
                                    
                                    <td ng-bind="serviceRequest.created_at | date:'dd/MM/yyyy'" class="ng-binding">23/11/2016</td>
                                    <td ng-bind="serviceRequest.updated_at | date:'dd/MM/yyyy'" class="ng-binding">23/11/2016</td>
                                    
                                </tr>
              </tbody>
 
 				  
		</table>
	</div>

	
	<div class="col-lg-12 text-right">
	
	
	<ul class="pagination-sm pagination " >
  <li class=""><a href="" class="ng-binding">First</a></li>
  <li><a href="">Previous</a></li>
 <li class="active"><a href="">1</a></li>
 <li class=""><a href="">2</a></li>
 <li class=""><a href="">3</a></li>
  <li><a href="">Next</a></li>
<li><a href="">Last</a></li>
</ul>
	
	
	</div>
	
	</div>
		  
		  
		  
		  
		  
		 
        </div>
      </div>
	  
    </div>
  </div>
		
	
	</div>
	
	
	</div>
	</div>
	
	
	
	
	
