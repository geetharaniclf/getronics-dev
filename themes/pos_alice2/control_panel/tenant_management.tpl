
    
	<div id="columns" class="container">
	<div class="row">
             
<div id="left_column" class="column col-xs-12 col-sm-3">
    
	{include file="{$one_var}control_panel/sidebar.tpl"}
</div>

<div id="center_column" class="center_column col-xs-12 col-sm-9">

    <div class="row dashboard-top-panel">

       <div class="col-lg-6">
			<div class="mini-box-bg">
            <div class="box-title">
                <p>At a Glance</p>
            </div>

            <div class="col-lg-6" id="providerWidget">
                <div class="row db-boxes head-text">
                    <div class="head-block"><span style="color:#000"><b>Providers</b></span></div>
                    
                </div>
				
				<img src="{$img_dir}images/dashboard-icons/providers.jpg">

            </div>

            <div class="col-lg-6" id="billingWidget" ng-show="showBillingWidget">
                <div class="mini-box-test" style="background-color: #fff; border-radius: 0px;min-height:228px" id="billingDetailsId">
                    <div class="col-lg-12 db-boxes">
                        <div class="col-lg-6 head-text col-centered">
                            <div class="head-block">
                                <span style="color:#000"><b>Billing</b></span>                              
                            </div>                                 
                            
                        </div>                      

                </div>
                <div class="row" id="totalAmount">
                        <div class="col-lg-12">
                            <div class="box-info">
                                <p class="size-h1 text-center ng-binding" id="total_price_amount" style="display:block;color:#000">$0</p>
                            </div>
                        </div>
                    </div>
                    

                                                                <div class="row">
                                                                    <div class="col-lg-12 ">
                                                                        <div class="box-info">
                                                                            <p class="size-h6 text-center" style="color:#000"><span><b>Current billing period</b></span></p>
                                                                        </div>
                                                                        <div class="box-info text-center">
                                                                            <p class="text-muted" style="color:#000"><span class="ng-binding"> - </span>
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="clearfix">&nbsp;</div>
                                                                    <div class="col-lg-12 text-center">
                                                                        <a href="#" id="billingDetailsBtn" class="btn btn-w-md btn-gap-v btn-dark btn-blue">Details</a>
                                                                    </div>
                                                                </div>
                                </div>
                            </div>
				 </div>
       </div>
	   <div class="col-lg-6 my-account-dashboard">
		<div class="mini-box-bg">
                            <div class="box-title">
                                <p>Account</p>
                            </div>
                            <div class="col-xs-4">
                                <div class="icon-container">
                                    <a href="/ui/index#/users/users">
									<i class="fa fa-plus"></i></a>
                                    <p>Create New User</p>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="icon-container">
                                    <a href="#"><img src="{$img_dir}images/dashboard-icons/new-blueprint.jpg"></a>
                                    <p>Virtual Machine</p>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="icon-container">
                                    <a href="#"><img src="{$img_dir}images/dashboard-icons/new-data-center.jpg"></a>
                                    <p>New Data Center</p>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="icon-container">
                                    <a href="/ui/index#/servicerequest/servicerequest"><img src="{$img_dir}images/dashboard-icons/new-support-item.jpg"></a>
                                    <p>New Support Item</p>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="icon-container">
                                    <a href="/ui/index#/o365/o365"><img src="{$img_dir}images/dashboard-icons/new-365.jpg"></a>
                                    <p>New O365 Subscription</p>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="icon-container">
                                    <a href="/en/identity"><img src="{$img_dir}images/dashboard-icons/account-settings.jpg"></a>
                                    <p>Account Settings</p>
                                </div>
                            </div>
		</div>
        </div>
	
	
	</div>
	
	<div class="row">
                        <div class="col-lg-4">
							<div class="mini-box-bg-2">
                            <div class="box-title">
                                <p>Virtual Machines &amp; Cloud Desktop</p>
                            </div>

                            <div class="row">
                                <div class=" col-sm-12 col-lg-12">
                                    <p class="blue-heding p-margin"><span><b>Virtual Machine Status</b></span></p>
                                </div>

                                <div class=" col-sm-12 col-lg-12">
                                    <div class="row">
                                        <div class="col-xs-4 dash-logo">
                                            <img src="{$img_dir}images/dashboard-icons/Virtual-Machines.jpg">
                                        </div>
                                        <div class="col-sm-4 col-lg-4 text-center paddingright-8">
                                            <div class="box-dasbhboard">
                                                <h3 class="box-dasbhboard-h3 badge badge-success ng-binding">0</h3>
                                                <p class="box-dasbhboard-text">Running</p>
                                            </div>
                                        </div>
                                        <div class=" col-sm-4 col-lg-4 text-center paddingleft-8">
                                            <div class="box-dasbhboard">
                                                <h3 class="box-dasbhboard-h3-red badge badge-danger ng-binding">0</h3>
                                                <p class="box-dasbhboard-text-red">Stopped</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-12 col-lg-12">
                                    <p class="blue-heding p-margin"><span><b>Cloud Desktop Status</b></span></p>

                                </div>
                                <div class=" col-sm-12 col-lg-12">
                                    <div class="row">
                                        <div class="col-xs-4 dash-logo">
                                            <img src="{$img_dir}images/dashboard-icons/cloud-desktops.jpg">
                                        </div>
                                        <div class=" col-sm-4 col-lg-4 text-center paddingright-8">
                                            <div class="box-dasbhboard">
                                                <h3 class="box-dasbhboard-h3 badge badge-success ng-binding">0</h3>
                                                <p class="box-dasbhboard-text">Running</p>
                                            </div>
                                        </div>
                                        <div class=" col-sm-4 col-lg-4 text-center paddingleft-8">
                                            <div class="box-dasbhboard">
                                                <h3 class="box-dasbhboard-h3-red badge badge-danger ng-binding">0</h3>
                                                <p class="box-dasbhboard-text-red">Stopped</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">							
                                <div class="col-xs-8 col-centered virtual-btn">
                                    <a href="#" class="btn btn-w-md btn-gap-v btn-dark btn-blue">New Virtual Machine</a>
                                    <a href="#" class="btn btn-w-md btn-gap-v btn-dark btn-blue">View All</a>
                                </div>

                            </div>
						  </div>
						</div>
                        <div class="col-lg-4">
							<div class="mini-box-bg-2">
                            <div class="box-title">
                                <p>Networks, Firewalls &amp; Load Balancer</p>
                            </div>

                            <div class="row">
                                <div class=" col-sm-12 col-lg-12">
                                    <p class="blue-heding p-margin"><span><b>Network Status</b></span></p>

                                </div>
                                <div class=" col-sm-12 col-lg-12">
                                    <div class="row">
                                        <div class="col-xs-4 dash-logo">
                                            <img src="{$img_dir}images/dashboard-icons/network-status.jpg">
                                        </div>
                                        <div class=" col-sm-4 col-lg-4 text-center paddingright-8">
                                            <div class="box-dasbhboard">
                                                <h3 class="box-dasbhboard-h3 badge badge-info ng-binding">52</h3>
                                                <p class="box-dasbhboard-text">Private IP's</p>
                                            </div>
                                        </div>
                                        <div class=" col-sm-4 col-lg-4 text-center paddingleft-8">
                                            <div class="box-dasbhboard">
                                                <h3 class="box-dasbhboard-h3 badge badge-info ng-binding">17</h3>
                                                <p class="box-dasbhboard-text">Public IP's</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-12 col-lg-12">
                                    <p class="blue-heding p-margin"><span><b>Firewall Status</b></span></p>

                                </div>
                                <div class=" col-sm-12 col-lg-12">
                                    <div class="row">
                                        <div class="col-xs-4 dash-logo">
                                            <img src="{$img_dir}images/dashboard-icons/firewall-status.jpg">
                                        </div>
                                        <div class=" col-sm-4 col-lg-4 text-center paddingright-8">
                                            <div class="box-dasbhboard">
                                                <h3 class="box-dasbhboard-h3 badge badge-warning ng-binding">0</h3>
                                                <p class="box-dasbhboard-text">Firewalls</p>
                                            </div>
                                        </div>
                                        <div class=" col-sm-4 col-lg-4 text-center paddingleft-8">
                                            <div class="box-dasbhboard">
                                                <h3 class="box-dasbhboard-h3 badge badge-warning ng-binding">0</h3>
                                                <p class="box-dasbhboard-text">Rules</p>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        
                            <hr>
                            <div class="row">
                                <div class="col-sm-12 col-lg-12">
                                    <p class="blue-heding p-margin"><span><b>Load Balancer Status</b></span></p>


                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-12 col-lg-12">
                                        <p class="blue-heding p-margin"><span><b>Load Balencer Status</b></span></p>

                                    </div>
                                    <div class=" col-sm-12 col-lg-12">
                                        <div class="row">
                                            <div class="col-xs-4 dash-logo">
                                                <img src="{$img_dir}images/dashboard-icons/load-balancer.jpg">
                                            </div>
                                            <div class=" col-sm-4 col-lg-4 text-center paddingright-8">
                                                <div class="box-dasbhboard">
                                                    <h3 class="box-dasbhboard-h3 badge badge-warning ng-binding">0</h3>
                                                    <p class="box-dasbhboard-text">Load Balancers</p>
                                                </div>
                                            </div>
                                            <div class=" col-sm-4 col-lg-4 text-center paddingleft-8">
                                                <div class="box-dasbhboard">
                                                    <h3 class="box-dasbhboard-h3 badge badge-warning ng-binding">0</h3>
                                                    <p class="box-dasbhboard-text">VIP Pools</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
						</div>
                        <div class="col-lg-4">
						<div class="mini-box-bg-2">
                            <div class="box-title">
                                <p>Backups &amp; Support Tickets</p>
                            </div>

                            <div class="row">
                                <div class=" col-sm-12 col-lg-12">
                                    <p class="blue-heding p-margin"><span><b>Latest Backup</b></span></p>

                                    <div class=" col-sm-12 col-lg-12 div-not-available">
                                        <h3 class="box-dasbhboard-h3 div-not-available-text">Not Available</h3>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-12 col-lg-12">
                                    <p class="blue-heding p-margin"><span><b>Support Tickets</b></span></p>

                                </div>
                                <div class=" col-sm-12 col-lg-12">
                                    <div class="row">
                                        <div class=" col-sm-6 col-lg-6 text-center paddingright-8">
                                            <div class="box-dasbhboard">
                                                <h3 class="box-dasbhboard-h3-blue ng-binding">2</h3>
                                                <p class="box-dasbhboard-text-blue">Open</p>
                                            </div>
                                        </div>
                                        <div class=" col-sm-6 col-lg-6 text-center paddingleft-8">
                                            <div class="box-dasbhboard">
                                                <h3 class="box-dasbhboard-h3 ng-binding">0</h3>
                                                <p class="box-dasbhboard-text">Resolved</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
						</div>
                    </div>
	
	</div>
	
	
        </div>
        </div>
	
	
	
