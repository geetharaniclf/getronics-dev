    
	<div id="columns" class="container">
	<div class="row">
             
<div id="left_column" class="column col-xs-12 col-sm-3">
	{include file="{$one_var}control_panel/sidebar.tpl"}
</div>

<div id="center_column" class="center_column col-xs-12 col-sm-9">
		
		
		<div class="row">
    <div class="col-lg-12 themed-nav virtual-machines-main">
      <div class="box-title">
          <p>Users</p>
        </div>
      <div class="mini-box">
        
        <div class="panel-header">
         <aside data-ng-include=" 'views/virtual_machines/virtual_machine_nav.html' " id="nav-container" class="nav-container ng-scope nav-fixed nav-horizontal nav-vertical" data-ng-class="">
		
        <div class="nav-wrapper">
            <ul id="nav" class="nav ng-scope" data-ng-controller="NavCtrl" data-collapse-nav="" data-highlight-active="">
                <li class="">
                    
                    <a href="#"><i class="fa fa-plus"></i><span>New</span></a>
                </li>
                
                <li>
                    <a href="#" ><i class="fa fa-trash-o"></i><span>Delete</span></a>
                    
            
                </li>
                
                <li>
                    <a href="#" >
						<i class="fa fa-file-excel-o" aria-hidden="true"></i>

                        <span data-i18n="Generate Excel">Generate Excel</span> 
                    </a>
                </li>
                <li>
                    <a href="#" >
						<i class="fa fa-file-pdf-o" aria-hidden="true"></i>

                        <span data-i18n="Generate PDF">Generate PDF</span> 
                    </a>
                </li>
            </ul>
        </div>    
     


  </aside>

        </div>
        <div class="divider divider"></div>


        <div class="table-filters" >
            <div class="row">
              <div class="col-xs-12 col-sm-12 ">
                <div class="col-sm-3 col-sm-offset-9 col-xs-4 col-xs-offset-8">
                  <form class="ng-pristine ng-valid">
                    <input placeholder="Search..." class="form-control ng-pristine ng-valid"  data-ng-keyup="search()" type="text">
                  </form>
                </div>
              </div>
            </div>
        </div>

        <div class="panel-body">
          
		
		  <div id="exportable">
	<div class="table-responsive" >
		<table class="table"  cellpadding="0" cellspacing="0">
			<thead>
                <tr>
                  <th width="2%">
				  <input  id="selectAllRecords"  class="ng-pristine ng-valid" type="checkbox"></th>
                  <th width="15%">Name</th>   
                  <th width="15%">Company</th>               
                  <th width="10%">User Id</th>
                  <th width="15%">Role</th>
                  <th width="15%">Contact No.</th>
                  <th width="15%">Edit</th>                  
                </tr>
              </thead>  
			  <tbody>
                <tr>
                  <td><input id="virtual_machine" type="checkbox"></td>
                  <td><a href="#" class="ng-binding">Test User 1</a></td>
                  <td >Company-ABC</td>
                  <td >abc.xya@company-abc.com</td>
                  <td >Billing Admin</td>
                  <td >998778927</td>
                  <td ><a href="#" ng-click="rowform.$show()"> 
                                           <i class="fa fa-pencil-square-o"></i>
                                        </a></td>
                  
                </tr>
             
                <tr>
                  <td><input id="virtual_machine" type="checkbox"></td>
                  <td><a href="#" class="ng-binding">Test User 2</a></td>
                  <td >Company-ABC</td>
                  <td >abc.xya@company-abc.com</td>
                  <td >Tech Admin</td>
                  <td >998778927</td>
                  <td ><a href="#" ng-click="rowform.$show()"> 
                                           <i class="fa fa-pencil-square-o"></i>
                                        </a></td>
                  
                </tr>
              <tr>
                  <td><input id="virtual_machine" type="checkbox"></td>
                  <td><a href="#" >Test User</a></td>
                  <td >Company-ABC</td>
                  <td >abc.xya@company-abc.com</td>
                  <td >End User</td>
                  <td >998778927</td>
                  <td ><a href="#" ng-click="rowform.$show()"> 
                                           <i class="fa fa-pencil-square-o"></i>
                                        </a></td>
                 <tr>
                  <td><input id="virtual_machine" type="checkbox"></td>
                  <td><a href="#" class="ng-binding">Test User 3</a></td>
                  <td >Company-ING</td>
                  <td >abc.xya@company-abc.com</td>
                  <td >Billing Admin</td>
                  <td >998778927</td>
                  <td ><a href="#" ng-click="rowform.$show()"> 
                                           <i class="fa fa-pencil-square-o"></i>
                                        </a></td>
                  
                </tr>
             
                <tr>
                  <td><input id="virtual_machine" type="checkbox"></td>
                  <td><a href="#" class="ng-binding">Test User 4</a></td>
                  <td >Company-ING</td>
                  <td >abc.xya@company-abc.com</td>
                  <td >Tech Admin</td>
                  <td >998778927</td>
                  <td ><a href="#" ng-click="rowform.$show()"> 
                                           <i class="fa fa-pencil-square-o"></i>
                                        </a></td>
                  
                </tr>
              <tr>
                  <td><input id="virtual_machine" type="checkbox"></td>
                  <td><a href="#" >Test User6</a></td>
                  <td >Company-ING</td>
                  <td >abc.xya@company-abc.com</td>
                  <td >End User</td>
                  <td >998778927</td>
                  <td ><a href="#" ng-click="rowform.$show()"> 
                                           <i class="fa fa-pencil-square-o"></i>
                                        </a></td>                 
                </tr>
              </tbody>				  
		</table>
	</div>

	
	<div class="col-lg-12 text-right">
	
	
	<ul class="pagination-sm pagination " >
  <li class=""><a href="" class="ng-binding">First</a></li>
  <li><a href="">Previous</a></li>
 <li class="active"><a href="">1</a></li>
 <li class=""><a href="">2</a></li>
 <li class=""><a href="">3</a></li>
  <li><a href="">Next</a></li>
<li><a href="">Last</a></li>
</ul>
	
	
	</div>
	
	</div>
		  
		  
		  
		  
		  
		 
        </div>
      </div>
	  
    </div>
  </div>
		
	
	</div>
	
	
	</div>
	</div>
	
	
	
	
	