	<div id="columns" class="container">
	<div class="row">
             
<div id="left_column" class="column col-xs-12 col-sm-3">
	{include file="{$one_var}control_panel/sidebar.tpl"}
</div>

<div id="center_column" class="center_column col-xs-12 col-sm-9">
		
		
		<div class="row">
    <div class="col-lg-12 themed-nav virtual-machines-main">
      <div class="box-title">
          <p>VIRTUAL DATACENTER</p>
        </div>
      <div class="mini-box">
        
        <div class="panel-header">
         <aside  id="nav-container" class="nav-container ng-scope nav-fixed nav-horizontal nav-vertical">
		
        <div class="nav-wrapper">
            <ul id="nav" class="nav ng-scope" data-ng-controller="NavCtrl" data-collapse-nav="" data-highlight-active="">
                <li class="">
                    
                    <a href="http://cloudselect.online/Getronics/en/virtual-data-center/150-axian-virtual-data-center-professional.html"><i class="fa fa-plus"></i><span>New</span></a>
                </li>
                <li>
                    <a ><i class="fa fa-play"></i><span>Start</span></a>
             
                </li>
                <li>
                    <a href="#" ><i class="fa fa-square-o"></i><span>Stop</span></a>
            
                </li>
                 
                <li>
                    <a href="#" ><i class="fa fa-repeat"></i><span>Restart</span></a>
            
                </li>
                <li>
                    <a href="#" ><i class="fa fa-trash-o"></i><span>Delete</span></a>
                    
            
                </li>
                <li id="resync_li_tag">
                  <a href="#" ng-click="resyncVirtualMachine()" id="resync_link"><i class="fa fa-retweet"></i><span>Resync</span></a>
            
                </li>
                <li>
                    <a href="#" >
						<i class="fa fa-file-excel-o" aria-hidden="true"></i>

                        <span data-i18n="Generate Excel">Generate Excel</span> 
                    </a>
                </li>
                <li>
                    <a href="#" >
						<i class="fa fa-file-pdf-o" aria-hidden="true"></i>

                        <span data-i18n="Generate PDF">Generate PDF</span> 
                    </a>
                </li>
            </ul>
        </div>    
     


  </aside>

        </div>
        <div class="divider divider"></div>


        <div class="table-filters" >
            <div class="row">
              <div class="col-xs-12 col-sm-12 ">
                <div class="col-sm-3 col-sm-offset-9 col-xs-4 col-xs-offset-8">
                  <form class="ng-pristine ng-valid">
                    <input placeholder="Search..." class="form-control ng-pristine ng-valid"  data-ng-keyup="search()" type="text">
                  </form>
                </div>
              </div>
            </div>
        </div>

        <div class="panel-body">
          
		
		  <div id="exportable">
	<div class="table-responsive" >
	
		<table class="table" cellpadding="0" cellspacing="0">
		<thead>
			<tr>
				<th style="width:20%;">Name</th>
				<th style="text-align:center;width:15%;">Virtual Machines</th>
				<th style="text-align:center;width:15%;">Virtual Networks</th>
				<th style="text-align:center;width:15%;">Load Balancers</th>
				<th style="text-align:center;width:15%;">Firewalls</th>
				<th style="text-align:center;width:15%;">Distributed Firewalls</th> 
			</tr> 
		</thead>                    
		<tbody>
			<tr>
				<td><a href="virtual_data_center_details.html" class="ng-binding">DemoVDC_1</a></td>
				<td style="text-align:center" >4</td>
				<td style="text-align:center" >3</td>
				<td style="text-align:center" >1</td>
				<td style="text-align:center" >2</td>
				<td style="text-align:center" >1</td>
			</tr>
			<tr>
				<td><a href="virtual_data_center_details.html" class="ng-binding">DemoVDC_2</a></td>
				<td style="text-align:center" >6</td>
				<td style="text-align:center" >2</td>
				<td style="text-align:center" >0</td>
				<td style="text-align:center" >1</td>
				<td style="text-align:center" >0</td>
			</tr>
			<tr>
				<td><a href="virtual_data_center_details.html" class="ng-binding">DemoVDC_3</a></td>
				<td style="text-align:center" >3</td>
				<td style="text-align:center" >2</td>
				<td style="text-align:center" >1</td>
				<td style="text-align:center" >0</td>
				<td style="text-align:center" >0</td>
			</tr>
			<tr>
				<td><a href="virtual_data_center_details.html" class="ng-binding">DemoVDC_4</a></td>
				<td style="text-align:center" >2</td>
				<td style="text-align:center" >1</td>
				<td style="text-align:center" >0</td>
				<td style="text-align:center" >0</td>
				<td style="text-align:center" >0</td>
			</tr>
			<tr>
				<td><a href="virtual_data_center_details.html" class="ng-binding">DemoVDC_5</a></td>
				<td style="text-align:center" >2</td>
				<td style="text-align:center" >1</td>
				<td style="text-align:center" >1</td>
				<td style="text-align:center" >2</td>
				<td style="text-align:center" >0</td>
			</tr>
			<tr>
				<td><a href="virtual_data_center_details.html" class="ng-binding">ING_VDC_1</a></td>
				<td style="text-align:center" >1</td>
				<td style="text-align:center" >1</td>
				<td style="text-align:center" >0</td>
				<td style="text-align:center" >0</td>
				<td style="text-align:center" >0</td>
			</tr>
		</tbody>
		</table>

	
	
	</div>

	
	<div class="col-lg-12 text-right">
	
	
	<ul class="pagination-sm pagination " >
  <li class=""><a href="" class="ng-binding">First</a></li>
  <li><a href="">Previous</a></li>
 <li class="active"><a href="">1</a></li>
 <li class=""><a href="">2</a></li>
 <li class=""><a href="">3</a></li>
  <li><a href="">Next</a></li>
<li><a href="">Last</a></li>
</ul>
	
	
	</div>
	
	</div>
		  
		  
		  
		  
		  
		 
        </div>
      </div>
	  
    </div>
  </div>
		
	
	</div>
	
	
	</div>
	</div>
	
	
	
	
	