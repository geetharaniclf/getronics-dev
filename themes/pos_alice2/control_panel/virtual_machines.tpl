

<div id="columns" class="container">
    <div class="row">

        <div id="left_column" class="column col-xs-12 col-sm-3">
            {include file="{$one_var}control_panel/sidebar.tpl"}
        </div>

        <div id="center_column" class="center_column col-xs-12 col-sm-9">


            <div class="row detail_col">
                <div class="col-lg-12 themed-nav virtual-machines-main">
                    <div class="box-title">
                        <p>Virtual Machines</p>
                    </div>
                    <div class="mini-box">

                        <div class="panel-header">
                            <aside data-ng-include=" 'views/virtual_machines/virtual_machine_nav.html' " id="nav-container" class="nav-container ng-scope nav-fixed nav-horizontal nav-vertical" data-ng-class="">

                                <div class="nav-wrapper">
                                    <ul id="nav" class="nav ng-scope" data-ng-controller="NavCtrl" data-collapse-nav="" data-highlight-active="">
                                        <li class="">

                                            <a href="#"><i class="fa fa-plus"></i><span>New</span></a>
                                        </li>
                                        <li>
                                            <a onclick="startvm();"><i class="fa fa-play"></i><span>Start</span></a>

                                        </li>
                                        <li>
                                            <a onclick="stopvm();"><i class="fa fa-square-o"></i><span>Stop</span></a>

                                        </li>

                                        <li>
                                            <a href="#" ><i class="fa fa-repeat"></i><span>Restart</span></a>

                                        </li>
                                        <li>
                                            <a href="#" ><i class="fa fa-trash-o"></i><span>Delete</span></a>


                                        </li>
                                        <li id="resync_li_tag">
                                            <a href="#" ng-click="resyncVirtualMachine()" id="resync_link"><i class="fa fa-retweet"></i><span>Resync</span></a>

                                        </li>
                                        <li  onclick="generatexls();">
                                            <a href="#" >
                                                <i class="fa fa-file-excel-o" aria-hidden="true"></i>

                                                <span data-i18n="Generate Excel">Generate Excel</span> 
                                            </a>
                                        </li>
                                        <li onclick="generatepdf();">
                                            <a href="#" >
                                                <i class="fa fa-file-pdf-o" aria-hidden="true"></i>

                                                <span data-i18n="Generate PDF">Generate PDF</span> 
                                            </a>
                                        </li>
                                    </ul>
                                </div>    



                            </aside>

                        </div>
                        <div class="divider divider"></div>


                        <div class="table-filters" >
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 ">
                                    <div class="col-sm-3 col-sm-offset-9 col-xs-4 col-xs-offset-8">
                                        <form class="ng-pristine ng-valid">
                                            <input placeholder="Search..." class="form-control ng-pristine ng-valid search_field" onkeyup="search($(this));" type="text">
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel-body body_table">


                            <div id="exportable">
                                <div class="table-responsive" >
                                    <table class="table"  cellpadding="0" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th width="2%">
                                                    <input  id="selectAllRecords"  class="ng-pristine ng-valid" type="checkbox"></th>
                                                <th width="20%">Name</th>
                                                <th width="20%">Service Name</th>
                                                <th width="10%">Status</th>
                                                <th width="10%">Private IP</th>
                                                <th width="10%">Public IP</th>
                                                <th width="10%">Type</th>
                                                <th width="18%">Provider</th>                  
                                            </tr>
                                        </thead>  
                                        <tbody>
                                            {if !empty($rowsData)}

                                                {foreach $rowsData as $rowData}
                                                    <tr class="row_tbl">
                                                        <td><input id="virtual_machine" class="input_chcek" type="checkbox" data="{$rowData['guid']}"></td>
                                                        <td><a data="{$rowData['guid']}" href="#" class="ng-binding machine_name">{$rowData['name']}</a></td>
                                                        <td >{$rowData['service_name']}</td>
                                                        {if $rowData['state']=='Stopping'}
                                                            <td ng-switch-when="Stopping" class="state" id="{$rowData['guid']}" class="ng-scope">
                                                                <span id="spinner_IBMSLVM-1">
                                                                    <img src="{$img_dir}images/select2-spinner.gif" class="ajax-loader">
                                                                </span>&nbsp;&nbsp;<j>Stopping</j>
                                                    </td>
                                                {/if}

                                                {if $rowData['state']=='Running'}
                                                    <td ng-switch-when="Running" class="state" id="{$rowData['guid']}" class="ng-scope">
                                                        <span id="spinner_IBMSLVM-1">
                                                            <i class="fa fa-circle" aria-hidden="true" style="color:#65d0a0"></i>

                                                        </span>&nbsp;&nbsp;<j>Running</j>
                                                    </td>
                                                {/if}
                                                {if $rowData['state']=='Installed'}
                                                    <td ng-switch-when="Installed" class="state" id="{$rowData['guid']}" class="ng-scope">
                                                        <span id="spinner_IBMSLVM-1">
                                                            <i class="fa fa-circle" aria-hidden="true" style="color:#65d0a0"></i>
                                                        </span>&nbsp;&nbsp;<j>Installed</j>
                                                    </td>
                                                {/if}
                                                {if $rowData['state']=='Deleted'}
                                                    <td ng-switch-when="Deleted" class="state" id="{$rowData['guid']}" class="ng-scope">
                                                        <span id="spinner_IBMSLVM-1">
                                                            <i class="fa fa-circle" aria-hidden="true"></i>
                                                        </span>&nbsp;&nbsp;<j>Deleted</j>
                                                    </td>
                                                {/if}
                                                {if $rowData['state']=='ShuttingDown'}
                                                    <td ng-switch-when="ShuttingDown" class="state" id="{$rowData['guid']}" class="ng-scope">
                                                        <span id="spinner_IBMSLVM-1">
                                                            <i class="fa fa-circle" aria-hidden="true"></i>
                                                        </span>&nbsp;&nbsp;<j>ShuttingDown</j>
                                                    </td>
                                                {/if}

                                                <td >{$rowData['private_ip']}</td>
                                                <td >{$rowData['public_ip']}</td>
                                                <td >{$rowData['type']}</td>
                                                <td >{$rowData['provider']}</td>

                                                </tr>      
                                            {/foreach}
                                        {/if}

                                        </tbody>				  
                                    </table>
                                </div>


                                <div class="col-lg-12 text-right">


                                    <!--<ul class="pagination-sm pagination " >
                              <li class=""><a href="" class="ng-binding">First</a></li>
                              <li><a href="">Previous</a></li>
                             <li class="active"><a href="">1</a></li>
                             <li class=""><a href="">2</a></li>
                             <li class=""><a href="">3</a></li>
                              <li><a href="">Next</a></li>
                            <li><a href="">Last</a></li>
                            </ul>-->

                                    {include file="{$one_var}control_panel/pagination.tpl"}


                                </div>

                            </div>






                        </div>
                    </div>

                </div>
            </div>


        </div>


    </div>
</div>

<script>
    $(document).ready(function () {
        $(document).on('click', '.page_update li a', function () {

            var sval = $('.search_field').val();

            var url = $(this).attr('href');
            if (url != '') {

                var res = url.replace("?", "&");

            }
            if (typeof res === 'undefined') {
                res = '';
            }
            change_data(sval, res);
            return false;

        });
        call_update();



    });

    function call_update() {



        if ($('.row_tbl').length)
        {


            $('.state').each(function () {

                var guid = $(this).attr('id')

                var state = $(this).children('j').html();

                update_state(guid, state, $(this));

            });
        }

        setTimeout(function () {
            call_update();
        }, 10000);
    }

    function search(search) {

        var sval = $(search).val();
        var p = '';
        // alert(window.location);
        change_data(sval, p);
    }



    function change_data(sval, p) {

        $.ajax({
            type: 'post',
            url: baseUri + 'index.php?controller=cpajax',
            data: 'search=' + sval + '&page=virtual_machines' + p,
            success: function (resp)
            {
                $('.body_table').html(resp);
            }
        });
// call_update();

    }

    function generatexls() {
        var sval = $('.search_field').val();
        var p = $('.page_update .current span span').html();
        p = '&p=' + p;
        if (typeof p === 'undefined') {
            p = '';
        }

        document.location.href = baseUri + '/index.php?controller=cpajax&search=' + sval + '&page=excl' + p;


        return false;
    }

    function generatepdf() {
        var sval = $('.search_field').val();
        var p = $('.page_update .current span span').html();
        p = '&p=' + p;
        if (typeof p === 'undefined') {
            p = '';
        }

        if ($('.row_tbl').length)
        {
            document.location.href = baseUri + '/index.php?controller=cpajax&search=' + sval + '&page=pdf' + p;
        } else {
            alert('No data found to export.');
        }





        return false;
    }

    function update_state(guid, state, that) {
        //history.pushState(null, null, '/en/step2'); 

        $.ajax({
            type: 'post',
            url: baseUri + 'index.php?controller=cpajax',
            data: 'guid=' + guid + '&page=update_state&state=' + state,
            success: function (resp)
            {
                if (resp != state) {
                    $(that).html(resp);
                }
                if (resp == '') {
                    $(that).parent('.row_tbl').remove();
                }
            }
        });

    }



    $(document).on('click', '.machine_name', function () {
        var uid = $(this).attr('data');
        var url = window.location
        history.pushState(null, null, url + '&vmdetail=' + uid);


        $.ajax({
            type: 'post',
            url: baseUri + 'index.php?controller=cpajax',
            data: 'page=virtual_machines&vmdetail=' + uid,
            success: function (resp)
            {
                console.log(resp);
                if (resp == '') {
                    alert('No virtual machine foung. Reloading page.....');
                    location.reload();
                } else {
                    $('.detail_col').html(resp);
                }
            }
        });

        return false;
    });


function startvm(){ 
    var arrayguid = [];
     $('.checked input:checked').each(function (index) {
         var guid = $(this).attr('data');
         arrayguid[index]=[guid];
     });
     if(arrayguid!=''){
  
  $.ajax({
            type: 'post',
            url: baseUri + 'index.php?controller=cpajax',
            data: 'page=startvm&arrayguid=' + arrayguid,
            success: function (resp)
            {
                
            }
        });
  
  
  
 }
 
 return false;
}

function stopvm(){ 
    var arrayguid = [];
     $('.checked input:checked').each(function (index) {
         var guid = $(this).attr('data');
         arrayguid[index]=[guid];
     });
     if(arrayguid!=''){
  
  $.ajax({
            type: 'post',
            url: baseUri + 'index.php?controller=cpajax',
            data: 'page=stopvm&arrayguid=' + arrayguid,
            success: function (resp)
            {
                
            }
        });
  
  
  
 }
 
}
$(document).on('change','#selectAllRecords', function(){
   if($(this).is(':checked')){
        $('.input_chcek').each(function () {
        // alert($(this).attr('data'));
        $('.input_chcek').attr('checked','checked');
        $(this).parent('span').addClass('checked');
     });
       
   }else{
        $('.input_chcek').each(function () {
        
        $('.input_chcek').removeAttr('checked');
        $(this).parent('span').removeClass('checked');
     });
   }
        
    });
</script>


