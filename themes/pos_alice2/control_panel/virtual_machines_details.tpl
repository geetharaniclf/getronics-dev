	<script type='text/javascript'>
//<![CDATA[
$(window).load(function(){
$(document).ready(function() {
    $(".tabs-menu a").click(function(event) {
        event.preventDefault();
        $(this).parent().addClass("current");
        $(this).parent().siblings().removeClass("current");
        var tab = $(this).attr("href");
        $(".tab-content").not(tab).css("display", "none");
       $(".inner-tab-content").not(tab).css("display", "none");
		$(".listing_user").css('display', 'block');
        <!-- $(".tab-content").firstChild('div.nav-wrapper').next(div).attr("display", "block"); -->
        <!-- $(this).div('.nav-wrapper').next.css("display", "block"); -->
		
        $(tab).fadeIn();
    });

    $(".inner-tabs-menu a").click(function(event) {
        event.preventDefault();
        $(this).parent().addClass("current_1");
        $(this).parent().siblings().removeClass("current_1");
        var tab = $(this).attr("href");
       $(".inner-tab-content").not(tab).css("display", "none");
        $(".listing_user").css('display', 'none');
        $(tab).fadeIn();
    });
});
});

//]]> 

</script>
	
    
	<div id="columns" class="container">
	<div class="row">
             
<div id="left_column" class="column col-xs-12 col-sm-3">
	{include file="{$one_var}control_panel/sidebar.tpl"}
</div>

	<div id="center_column" class="center_column col-xs-12 col-sm-9">
		
		
	<div class="row">
	<div class="col-xsm-12 col-lg-12">
		<div class="heading"><h3  >{$data['name']} </h3></div>
	</div>
	<div class="col-xsm-12 col-lg-12">
		<div class="pull-right">
		  <i class="fa fa-list"></i>&nbsp;&nbsp;<a href="ctrlpanel?page=virtual_machines">VIRTUAL MACHINES&nbsp;&nbsp;</a>
		</div>
	</div>

	</div>
		
	<div class="virtal-machine-details">
		<div class="mini-box" >
		   <div class="ui-tab" id="tabs-container">
			<ul class="nav nav-tabs tabs-menu nav-pills menu_class" >
				<li       class="current">
				<a   href="#tab-1"   >DETAILS</a>
				</li>
				<li      class=" ">
				<a   href="#tab-2">NETWORK</a>
				</li>
				<li      class=" ">
				<a  href="#tab-3"    >STORAGE</a>
				</li>

				<li      class=" ">
				<a  href="#tab-4" >PERFORMANCE</a>
				</li>
			</ul>
	  
	  
		  <!---- First Tab Starts ---->
		  <div id="tab-1" class="tab-content">  
			<section style="margin-top:10px">
			<div class="panel-body">
				<div class="table-responsive dashboard-table">
					<table class="table ">
						  <tbody>
							<tr>
								<td><strong>Server ID</strong></td>
								<td class="ng-binding">{$data['name']}</td>  
							</tr>
							<tr>
								<td><strong>Name</strong></td>
								<td class="ng-binding">{$data['service_name']}</td>
							 
							</tr>
						   
							<tr>
								<td><strong>Location</strong></td>
								<td class="ng-binding">{$data['location']}</td>
							 
							</tr>
							<tr>
								<td><strong>CPU &amp; RAM</strong></td>
								<td class="ng-binding">{$data['CPUS']} CPU(s) &amp; {$data['RAM']} MB RAM</td>
							 
							</tr>
							<tr>
								<td><strong>OS</strong></td>
								<td class="ng-binding">{$data['os_name']}</td>
							 
							</tr>
						</tbody>
					</table>
				</div>    
			</div>
			</section>  
		  </div>
		  <!---- First Tab Ends ---->
		  <!---- Second Tab Starts ---->
		  <div id="tab-2" class="tab-content">
			
			<section class="panel" style="margin-top:20px">
			<div class="panel-heading" style="color:#3c8dbc; padding:0px">
			<div class="col-xsm-12 col-md-12"><strong>IP ADDRESS</strong></div>
			<div class="col-xsm-1 col-md-1"><hr style="border-top: 1px solid #3c8dbc"></div>
			</div>
			<div class="panel-body"> 
			<div class="row">
			<div class="col-xsm-12 col-md-12 col-lg-12">
			<div class="table-responsive dashboard-table">
				<table class="table ">
					<thead>
						<tr>
				 
							<th>NIC</th>
							<th>Private IP</th>
							<th>Public IP</th>
						</tr>
					</thead>
					<tbody>					   
						<tr>			 
							<td class="ng-binding"></td>
							<td class="ng-binding">10.110.119.174</td>
							<td class="ng-binding">119.81.164.234</td>
						</tr><tr>			 
							<td class="ng-binding"></td>
							<td class="ng-binding">10.110.119.174</td>
							<td class="ng-binding">119.81.164.234</td>
						</tr><tr>			 
							<td class="ng-binding"></td>
							<td class="ng-binding">10.110.119.174</td>
							<td class="ng-binding">119.81.164.234</td>
						</tr>

					</tbody>
				</table>
			</div>    
			</div>
			</div>    
			</div>
			</section>
			<section class="panel ng-scope">
			<div class="panel-heading" style="color:#3c8dbc; padding:0px">
			<div class="col-xsm-12 col-md-12"><strong>FIREWALL RULES</strong></div>
			<div class="col-xsm-1 col-md-1"><hr style="border-top: 1px solid #3c8dbc"></div>
			</div>
			<div class="panel-body"> 
			<div class="row">
			<div class="col-xsm-12 col-md-12 col-lg-12">
				<div class="table-responsive dashboard-table">
					<table class="table ">
						<thead>
							<tr>
								<th>Enforced</th>
								<th>Source</th>
								<th>Destination</th>
								<th>Port Range</th>
								<th>Application Protocol</th>
					 
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>ABC</td>
								<td>Source</td>
								<td>Destination</td>
								<td>Port Range</td>
								<td>Application Protocol</td>                                                     
							</tr>
							<tr>
								<td>ABC</td>
								<td>Source</td>
								<td>Destination</td>
								<td>Port Range</td>
								<td>Application Protocol</td>                                                     
							</tr>
							<tr>
								<td>ABC</td>
								<td>Source</td>
								<td>Destination</td>
								<td>Port Range</td>
								<td>Application Protocol</td>                                                     
							</tr>	
						</tbody>
					</table>
				</div>    
			</div>
			</div>
			</div>
			</section>								
			<section class="panel ng-scope">
			<div class="panel-heading" style="color:#3c8dbc; padding:0px">
			<div class="col-xsm-12 col-md-12"><strong>LOAD BALANCER POOLS</strong></div>
			<div class="col-xsm-1 col-md-1"><hr style="border-top: 1px solid #3c8dbc"></div> 
			</div>    
			<div class="panel-body"> 
			<div class="row">
			<div class="col-xsm-12 col-md-12 col-lg-12">
			<div class="table-responsive dashboard-table">
				<table class="table ">
					<thead>
						<tr>		 
							<th>Name</th>
							<th>Enabled</th>
							<th>Virtual IP Address</th>
							<th>NAT IP Address</th>				
						</tr>
					</thead>
					<tbody>
						<tr>		 
							<td>XYZ</td>
							<td>Enabled</td>
							<td>10.110.119.174</td>
							<td>119.81.164.234</td>				
						</tr>
						<tr>		 
							<td>XYZ</td>
							<td>Enabled</td>
							<td>10.110.119.174</td>
							<td>119.81.164.234</td>				
						</tr>
						<tr>		 
							<td>XYZ</td>
							<td>Enabled</td>
							<td>10.110.119.174</td>
							<td>119.81.164.234</td>				
						</tr>

					</tbody>
				</table>
			</div>  
			</div>
			</div>  
			</div>
			</section>

		  
		  </div>
		  <!---- Second Tab Ends ---->
		  <!---- Third Tab Starts ---->
		  <div id="tab-3" class="tab-content">
			<section class="panel">
			<div class="panel-body">
			   <div class="row">
					<div class="col-xsm-12 col-md-12 col-lg-12">
						<div class="">
							<a style="color:#3c8dbc; margin-bottom:10px; display:table;" href="#" >Add Virtual Disk</a>
						</div>
					</div>		
					<div class="col-xsm-12 col-md-12 col-lg-12">
						<div class="table-responsive dashboard-table">
							<table class="table">
								<thead>
									<tr>
										<th><strong>Disk Name</strong></th>
										<th><strong>Total Size</strong></th>
										<th><strong>Action</strong></th>
										<th><strong>Status</strong></th>
									</tr>
								</thead>
								<tbody>						
									<tr>
										<td><strong>Total Storage</strong></td>
										<td>0 GB</td>
										<td>-</td>
										<td>-</td>
									</tr>
									<tr>
										<td><strong>Total Storage</strong></td>
										<td>0 GB</td>
										<td>-</td>
										<td>-</td>
									</tr>
									<tr>
										<td><strong>Total Storage</strong></td>
										<td>0 GB</td>
										<td>-</td>
										<td>-</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
			   </div>
			</div>
			</section>
		  
		  </div>
		  <!---- Third Tab Ends ---->
		  <!---- Four Tab Starts ---->
		  <div id="tab-4" class="tab-content">
			<img src="{$img_dir}images/virtual-details-graph.jpg" alt="" style="padding:20px;">
		  
		  </div>
		  <!---- Four Tab Ends ---->  
	  </div>
	</div>
	</div>
	
	
	
		<div class="row cp-banner-main">
	<div class=" col-xsm-12 col-lg-12">
	<div class="mini-box" style="border-top: 3px solid #3c8dbc  ;
	border-radius: 0px;background-color: white;
	background: linear-gradient(to bottom, white, 90%, #DCDCDC  );">
	<div class="banner-content">
	<div class="col-lg-6 col-md-6 extra-padding">
	<span style="color:'#494949"><b>CLOUDSELECT ENTERPRISE MOBILITY </b></span> 
	<hr> 
	<h3 style="font-size:22px;color:#3c8dbc; margin-bottom:22px;">Tailored Solutions based on the different needs of users in your organisations</h3><h3> 
	</h3><h4 style="font-size:13px; line-height: 1.5;width: 415px;">
	<strong>CloudSelect's Enterprise Mobility portfolio offers best of breed Mobility, VDI, Application and Managed Services to deliver world class client solutions enabling Mobility, BYOD and workforce liberation strategies .</strong></h4><h4>
	<br>

	<a href="#" target="_blank" class="btn btn-w-md btn-gap-v btn-blue" style="">View Plans</a> 
	</h4></div> 
	<div class="col-lg-6 col-md-6">
	<div class="container">
	<img src="{$img_dir}images/em.png" alt=""></div> 
	</div>       
	</div> 
	</div> 
	</div> 
	</div>
	
	
	
		
	</div>
	
	
	</div>
	</div>
	
	
	
	
