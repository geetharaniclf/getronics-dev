    
	<div id="columns" class="container">
	<div class="row">
             
<div id="left_column" class="column col-xs-12 col-sm-3">
	{include file="{$one_var}control_panel/sidebar.tpl"}
</div>

<div id="center_column" class="center_column col-xs-12 col-sm-9">
		
		
		<div class="row">
    <div class="col-xs-12 col-lg-12 themed-nav">
						 <div class="box-title">
                              <p>Digital Workspace Management</p>
                            </div>
                          <div class="mini-box">

                            <div class="panel-header">
                             <aside data-ng-include=" 'views/virtual_machines/virtual_machine_nav.html' " id="nav-container" class="nav-container ng-scope nav-fixed nav-horizontal nav-vertical" data-ng-class="">

                            <div class="nav-wrapper">
                                <ul id="nav" class="nav ng-scope" data-ng-controller="NavCtrl" data-collapse-nav="" data-highlight-active="">
                                    <li class="">

                                        <a href="#"><i class="fa fa-plus"></i><span>New</span></a>
                                    </li>

                                    <li>
                                        <a href="#" ><i class="fa fa-trash-o"></i><span>Delete</span></a>


                                    </li>

                                    <li>
                                        <a href="#" >
                                            <i class="fa fa-file-excel-o" aria-hidden="true"></i>

                                            <span data-i18n="Generate Excel">Generate Excel</span> 
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" >
                                            <i class="fa fa-file-pdf-o" aria-hidden="true"></i>

                                            <span data-i18n="Generate PDF">Generate PDF</span> 
                                        </a>
                                    </li>
                                </ul>
                            </div>    



                      </aside>

                            </div>
                            <div class="divider divider"></div>
     
		
    </div>
  </div>
		
	
	</div>
	
	
	</div>
	</div>
	
	
	
	
	
