$('.carousel').carousel({
    interval: false
});

$(document).on('submit', 'form', function(){
    $(document).on('click', '.next', function(){
        if($('.stepHtml').html() == '3') {
            // $('body').addClass('blue-bg');
            // $('body').removeClass('blue-bg');
        }
    });

    $(document).on('click', '.pre', function(){
        if($('.stepHtml').html() != '3') {
            // $('body').addClass('blue-bg');
            // $('body').removeClass('blue-bg');
        }
    });
    
    return false;
});

$(document).on('click', '.next', function(){
        if($('.stepHtml').html() == '3') {
            // $('body').addClass('blue-bg');
            // $('body').removeClass('blue-bg');
        }
    });

    $(document).on('click', '.pre', function(){
        if($('.stepHtml').html() != '3') {
            // $('body').addClass('blue-bg');
            // $('body').removeClass('blue-bg');
        }
    });

$('.selector').css('width', '100%');

var reposposeCaptcha = '';
var captchaContainer = null;
    var loadCaptcha = function() {
      captchaContainer = grecaptcha.render('captcha_container', {
        'sitekey' : '6LdEOwwUAAAAAPtdosQfQVH4fHnGkOm9phkmnZaL',
        'callback' : function(response) {
          reposposeCaptcha = response;
        }
      });
    };

new Vue({
  el: '#registration',
  data: {
    captchaError: '',
    success: '',
    stepNumber: 1,
    states: [],
    countries: countries,
    validatonMessage: {
        firstname: firstname,
        lastname: lastname,
        email: email,
        phone_no_business: phone_no_business,
        password: password,
        confirm_password: confirm_password,
    },
    errors: {step1: {}, step2: {}},
    duplicateEmail: {},
    errorsFlag: {
      step1: true,
      step2: true
    },
    userData: {},
    step2: [
        'company_name',
        'address1',
        'id_country',
        'state',
        'city',
        'postcode'
    ],
    user: {
      step1: {},
      step2: {id_country: 0, state: 0, already_registered: null},
    },
    validationRules: {
        
        firstname: function( input){
            return /^[a-zA-ZäöüÄÖÜß-]+$/.test(input);
        },
        lastname: function( input){
            return /^[a-zA-Z9äöüÄÖÜß-]+$/.test(input);
        },
        email: function( input){
            return  /(.+)@(.+){2,}\.(.+){2,}/.test(input);
        },
        phone_no_business: function( input){
            return  $.isNumeric(input);
        },
        password: function( input){
            return  /^.*(?=.{8,})(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).*$/.test(input);
        },
         confirm_password: function( input, password){
            return input === password;
        },
    }

  },
  methods: {
    getNext: function(){
      $('.carousel').carousel('next');
          this.stepNumber = $('.carousel .active').index('.item') + 2;
    },

    getPrev: function()
    {
        $('.carousel').carousel('prev');
        this.stepNumber = this.stepNumber - 1;
    },

    reverseMessage: function () {
      this.message = this.message.split('').reverse().join('')
    },

    validateError: function()
    {
        var count = 0,
        Validate = this.validationRules,
        errors = {step1: {}},
        validatonMessage = this.validatonMessage,
        user = this.user;
	
        $.each(this.user.step1, function( index, value ) {
            if (value)  {
                if (!Validate[ index ]( value, user.step1.password)) {
                    errors.step1[index] = validatonMessage[index];
                }
                count = count + 1;
            }
        });

        this.errors = errors;
        
        if (count > 5 && Object.keys(this.errors.step1).length == 0 && !this.duplicateEmail) {
            this.errorsFlag.step1 = false;
        } else {
            this.errorsFlag.step1 = true;
        }
    },

    checkDuplicateEmail: function()
    {
        this.errorsFlag.step1 = true;
        this.$http.post('register', this.user, function(data) {
           
            if (data['error']) {
                if (data['errors']['email']) {
                    this.errorsFlag.step1 = true;
                    this.duplicateEmail = data.errors;
                } else {
                    this.validateError();
                    this.duplicateEmail = '';
                }
            } else {
                this.errorsFlag.step1 = true;
            }
        });
    },

    getStates: function()
    {
        this.$http.post('/Getronics/ajax-call.php?rand=' + new Date().getTime()+'&action=getstate&id_country='+this.user.step2.id_country, [], function(data) {
                this.states = data;
                this.user.step2.state = '';
        });
    },

    setDebug: function()
    {
        var errorFlag = false;
        var user = this.user;
        var countries = this.countries;
        var errors =  {step2: {}};
        var Validate = this.validationRules;
		
        $.each(this.step2, function( index, value ) { 
            if (value == 'postcode') {
                if (user.step2[value]) {
                   if (user.step2[value].length != countries[user.step2.id_country]['zip_code_format'].length){
                        errors.step2 = {'postcode': 'Postal code must be numeric  must be ' + 
                        countries[user.step2.id_country]['zip_code_format'].length + ' digit format.'};
                        errorFlag = true;
                   }
                }
            }
            if (!user.step2[value]) {
                errorFlag = true;
                return;
            }
        });
        
        this.errors = errors;
        this.errorsFlag.step2 = errorFlag;
    },
    getCompany: function()
    {
        
        if (this.user.step2.contract_id) {
            if (this.user.step2.contract_id.length > 6) {
                this.errorsFlag.step2  = true;
                this.errors.step2 = {contract_id: 'Invalid Contract Id'};
                this.user.step2 = {already_registered: 0, contract_id: this.user.step2.contract_id};
                this.$http.get('validateContractId.php?contractId=' + this.user.step2.contract_id, function(data) {
                    if (data.message == 'success') {
                        this.errorsFlag.step2  = '';
                        this.errors.step2 = {};
                        this.user.step2 = {companyName: data.companyName, company_id: 
                         data.companyId, already_registered: 0, contract_id: this.user.step2.contract_id };
                        return;
                    }
                    this.errors.step2 = {contract_id: 'Invalid Contract Id'};
                    // alert(this.errors.step2.contract_id);
                    $('#contracterror').html('Invalid Contract Id');
                });
            }
        }
    },
    register: function()
    {
        this.captchaError = false;
		reposposeCaptcha = true;
        if (!reposposeCaptcha) {
            this.captchaError = true;
            return false;
        }

        this.user.step1['captcha'] = reposposeCaptcha;
        this.makePostData();
        this.$http.post('register',this.user, function(data) {
            if (data.success == 1) {
                $('.carousel').carousel('next');
                this.stepNumber = this.stepNumber + 1;
                this.success = 1;
            }
        });
    },

    makePostData: function()
    {
        var userData = [];
		
        $.each(this.user.step1, function( index, value ) {
            userData.push(value);
        });
		
        $.each(this.user.step2, function( index, value ) {
            userData.push(value);
        });

        $.each(this.user.step3, function( index, value ) {
            userData.push(value);
        });

        
        this.userData = userData;
    }

  }
})
