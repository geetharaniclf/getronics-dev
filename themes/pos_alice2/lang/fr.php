<?php

global $_LANG;
$_LANG = array();
$_LANG['my-account_5336e0dfb849ba7a2e376521f77d6c93'] = 'Mon panneau de configuration';
$_LANG['sidebar_0bf06122738ba2002686649d2fbe810b'] = 'CIRCUIT UNIFIÉ';
$_LANG['sidebar_1e1dad7ddd6adc1015c0d138df6cf41c'] = 'REVENDEUR UNIFIER';
$_LANG['sidebar_256fc6e4dbf98308ceca2b9b924b25af'] = 'SOUTIEN';
$_LANG['sidebar_3b9dc956aae8e46c4ca4837b237bf2c7'] = 'FACTURES - SA';
$_LANG['sidebar_4020d50ee12e4b5063fb4137dce9b739'] = 'UTILISATEURS';
$_LANG['sidebar_5291d3d22deaf0fb06faa005ed28a420'] = 'MACHINE VIRTUELLE';
$_LANG['sidebar_5a48bbc726f19023a1330169a29a5377'] = 'O365 GESTION';
$_LANG['sidebar_6b33cfbc39f2a78e66aaa692464a02a5'] = 'FACTURATION';
$_LANG['sidebar_a989d7c920de1b0513967c8fa3dbe80b'] = 'GESTION DE WAAS';
$_LANG['sidebar_b360314f6ab61924dfc4c98e71845b0b'] = 'SURVEILLANCE';
$_LANG['sidebar_e7c84d094f0ec822a8e6a8cd1aa1129c'] = 'DEMANDE DE SERVICE';
$_LANG['sidebar_ea7021f308d7d4e691093dc16f6a8c8d'] = 'TABLEAU DE BORD';
$_LANG['sidebar_fa24bd896131490bb2685baf7bafec84'] = 'GESTION DES LOCATAIRES';

?>