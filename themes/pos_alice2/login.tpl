{*
* 2007-2014 CloudSelect
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@cloudselect.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade CloudSelect to newer
* versions in the future. If you wish to customize CloudSelect for your
* needs please refer to http://www.cloudselect.com for more information.
*
*  @author CloudSelect SA <contact@cloudselect.com>
*  @copyright  2007-2014 CloudSelect SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of CloudSelect SA
*}
{capture name=path}
	{if !isset($email_create)}{l s='Authentication'}{else}
		<a href="{$link->getPageLink('authentication', true)|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Authentication'}">{l s='Authentication'}</a>
		<span class="navigation-pipe">{$navigationPipe}</span>{l s='Create your account'}
	{/if}
{/capture}


{include file="$tpl_dir./errors.tpl"}

{if isset($success) && $success == 1}
<div class="alert alert-success">
	
		{l s='Your account has been successfully activated. Please login with your email and password.'}
	
</div>
{elseif isset($success) && $success == 2}
<div class="alert alert-success">
	
		{l s='Thank you for your registration.'}<br> {l s='Currently your account is inactive. Please check your email and click on activation link.'}
	
</div>
{/if}

	<!--{if isset($authentification_error)}
	<div class="alert alert-danger">
		{if {$authentification_error|@count} == 1}
			<p>{l s='There\'s at least one error'} :</p>
			{else}
			<p>{l s='There are %s errors' sprintf=[$account_error|@count]} :</p>
		{/if}
		<ol>
			{foreach from=$authentification_error item=v}
				<li>{$v}</li>
			{/foreach}
		</ol>
	</div>
	{/if}-->
	<div class="row">

		<div class="col-xs-12 col-sm-6">
			<form action="login" method="post" id="login_form" class="box no-border mrgin-logn">
				<!--<h3 class="page-subheading">{l s='Already registered?'}</h3>-->
				<div class="form_content clearfix">
                <h1 class="page-heading">{l s='Login'}</h1>
					<div class="form-group">
						<label for="email">{l s='Email address'}</label>
						<input class="is_required validate account_input form-control" data-validate="isEmail" type="text" id="email" name="email" value="{if isset($smarty.post.email)}{$smarty.post.email|stripslashes}{/if}" />
					</div>
					<div class="form-group">
						<label for="passwd">{l s='Password'}</label>
						<span><input class="is_required validate account_input form-control" type="password" data-validate="isPasswd" id="passwd" name="passwd" value="{if isset($smarty.post.passwd)}{$smarty.post.passwd|stripslashes}{/if}" /></span>
					</div>
					
					<p class="lost_password form-group">
                    <label class="hide-row">&nbsp;</label>
                    <a href="{$link->getPageLink('password')|escape:'html':'UTF-8'}" title="{l s='Recover your forgotten password'}" rel="nofollow">{l s='Forgot your password?'}</a></p>
					
					
					<p class="submit form-group">
                    <label class="hide-row">&nbsp;</label>
                    <div class="btn-login">
						<input type="hidden" class="hidden" name="back" value="{$smarty.server.HTTP_REFERER}" />
						<button type="submit" id="SubmitLoginNew" name="SubmitLoginNew" class="button btn btn-default button-medium">
							<span>
								
								{l s='Login'}
							</span>
						</button>
                        
                        <input type="hidden" class="hidden" name="back" value="{$smarty.server.HTTP_REFERER}" />
						<button class="button btn btn-default button-medium" type="submit" id="SubmitCreateNew" name="SubmitCreateNew">
								
								{l s='Register'}

						</button>
						<input type="hidden" class="hidden" name="SubmitCreate" value="{l s='Create an account'}" />
                        </div>
					</p>

				</div>
			</form>
		</div>
	</div>
	

