<!-- Block user information module NAV  -->
<div class="header_user_info">
	<div class="top-userinfo">
	    {if $is_logged}
	        <div class="dropdown">
	            <a class="account" data-toggle="dropdown" href="#"><i class="icon-user"></i><span> {$cookie->customer_firstname} {$cookie->customer_lastname}</span></a>
	             <ul class="dropdown-menu" role="menu">
	                <li><a href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}" title="{l s='View my account' mod='blockuserinfo'}"><i class="icon-suitcase"></i> <span>{l s='Account' mod='blockuserinfo'}</span></a></li>
	                <li><a href="{$link->getPageLink('history', true)|escape:'html':'UTF-8'}" title="{l s='Orders' mod='blockuserinfo'}"><i class="icon-sort-numeric-asc"></i> <span>{l s='Orders' mod='blockuserinfo'}</span></a></li>
	                <li><a href="{$link->getPageLink('addresses', true)|escape:'html':'UTF-8'}" title="{l s='Addresses' mod='blockuserinfo'}"><i class="icon-map-marker"></i> <span>{l s='Addresses' mod='blockuserinfo'}</span></a></li>
	                <li><a href="{$link->getPageLink('identity', true)|escape:'html':'UTF-8'}" title="{l s='Information' mod='blockuserinfo'}"><i class="icon-info"></i> <span>{l s='Personal info' mod='blockuserinfo'}</span></a></li>
	                <li><a href="ctrlpanel" title="{l s='Control Panel' mod='blockuserinfo'}"><i class="icon-gears"></i> <span>{l s='Control Panel' mod='blockuserinfo'}</span></a></li>
	                <li><a href="{$link->getPageLink('contact', true)|escape:'html':'UTF-8'}" title="{l s='Contact Us' mod='blockuserinfo'}"><i class="icon-envelope"></i> <span>{l s='Contact Us' mod='blockuserinfo'}</span></a></li>
	                
	                {capture assign="HOOK_USER_INFO"}{hook h="displayUserInfo"}{/capture}
	                {if !empty($HOOK_USER_INFO)}{$HOOK_USER_INFO}{/if}
	                
	                <li class="divider"></li>
	                <li><a class="logout" href="{$link->getPageLink('index', true, NULL, "mylogout")|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Log me out' mod='blockuserinfo'}"><i class="icon-sign-out"></i> <span>{l s='Sign out' mod='blockuserinfo'}</span></a></li>
	            </ul>
	        </div>
	    {else}
	        <a class="login" href="{$link->getPageLink('login', true)|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Log in to your CloudSelect account' mod='blockuserinfo'}">
	            {l s='Sign in' mod='blockuserinfo'}
	        </a>       
	    {/if}
    	</div>
</div>
<!-- /Block user information module NAV -->
