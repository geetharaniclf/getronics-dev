<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title>{l s='Axians Digitalhub'}</title>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="{$css_dir}bootstrap.min.css" type="text/css" media="" />
	<link rel="stylesheet" href="{$css_dir}authentication.css" type="text/css" media="" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<!-- <script src="https://www.google.com/recaptcha/api.js?onload=loadCaptcha&render=explicit" async defer></script>-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
</head>
<body class="blue-bg" id="registration" >
{literal} {{ this.errors }} {/literal}
	<div class="wrapper">
		<div class="">
		<header>
		  <div class="container">
		    <div class="row">
		      <div class="col-md-6">
		        <div id="header_logo"> <a title="CloudSelect" href="/"> <img  alt="CloudSelect" src="{$img_dir}cloudselect-32-theme1-logo-1491516418.jpg" class="logo img-responsive"> </a> </div>
		      </div>
		      <div class="col-md-6">
		        <div class="back pull-right"> <a href="/Getronics/">< {l s='Back to Home'}</a> </div>
		      </div>
		    </div>
		  </div>
		</header>
		<!-- content-section -->
		<section class="registration-content">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="form-heading" v-if="!success">
							<h2>{l s='Create your account'}</h2>
							<span class="stepHtml hide">{literal}{{stepNumber}}{/literal}</span>
							<span > {literal} {{ stepNumber }} {/literal}/ 3</span> 
						</div>
						<div class="form-heading" v-if="success">

							<h2 style="font-size:100px"><i class="fa fa-check-circle-o fa-6"></i><h2>
							<h2>{l s='Registration successfull.'}</h2>
							
						</div>

						<div class="col-md-12 ">
							<div class="carousel slide" data-ride="carousel">
								<div class="carousel-inner" role="listbox">
									<div class="registration-body step2 item active">
										<div class="row">
											<div class="col-md-6 col-sm-12 col-xs-12 right-border">
												<div class="left-img">
													<img src="{$img_dir}/reg-step-2.png">
												</div>
											</div>
											<div class="col-md-6 col-sm-12 col-xs-12">
												<div class="detail-sec">
													<div class="middle">
														<div class="side-heading">
															<h3>{l s='Personal Info'}</h3>
															<p class="helpValidation">
															{l s='Please provide your basic information.'}
															

															</p>
														</div>
														<form >
														<div v-bind:class="{ 'form-group': true, 'has-error': errors.step1.fname }">
															<input type="text" class="form-control" placeholder="{l s='First Name'}"
															 v-model="user.step1.firstname" v-on:keyup="validateError()">
															 <p class="helpValidation">{literal} {{ errors.step1.firstname }} {/literal}</p>
														</div>
														<div v-bind:class="{ 'form-group': true, 'has-error': errors.step1.lastname }" >
															<input type="text" class="form-control" 
															placeholder="{l s='Last Name'}" v-model="user.step1.lastname" v-on:keyup="validateError()">
															<p class="helpValidation"> {literal} {{ errors.step1.lastname }} {/literal}</p>
														</div>

														

														<div class="form-group">
															<div class="input-group">
																<span class="input-group-addon"> &nbsp; <i class="fa fa-envelope"></i> &nbsp;</span>
																<input type="email" required class="form-control" 
																placeholder="{l s='Email'}" v-model="user.step1.email" v-on:keyup="validateError()" 
																v-on:change="checkDuplicateEmail()">
																<a class="input-group-addon" v-show="user.step1.email">
																	<span  v-show="!errors.step1.email">
																		&nbsp; <i class="fa fa-check-circle"></i> &nbsp;
																	</span>

																	<span  v-show="errors.step1.email">
																		&nbsp; <i class="fa fa-times"></i> &nbsp;
																	</span>
																</a>
																
															</div>
															<p class="helpValidation">{literal} {{ errors.step1.email }} {/literal}</p>
															<p class="helpValidation">{literal} {{ duplicateEmail.email }} {/literal}</p>
															
														</div>

														<div class="form-group">
															<div class="input-group">
																<span class="input-group-addon"> &nbsp; <i class="fa fa-plus"></i> &nbsp;</span>
																<input type="text" class="form-control" 
																placeholder="{l s='Contact'}" v-model="user.step1.phone_no_business" v-on:keyup="validateError()">
															</div>
															<p class="helpValidation">{literal} {{ errors.step1.phone_no_business }} {/literal}</p>
														</div>
														<div class="form-group">
															<input type="password" class="form-control" 
															placeholder="{l s='Password'}" v-model="user.step1.password" v-on:keyup="validateError()">
															<p class="helpValidation">{literal} {{ errors.step1.password }} {/literal}</p>
														</div>
														<div class="form-group">
															<input type="password" class="form-control" placeholder="{l s='Confirm Password'}" 
															v-model="user.step1.confirm_password" v-on:keyup="validateError()">
															<p class="helpValidation">{literal} {{ errors.step1.confirm_password }} {/literal}</p>
														</div>
														<div class="form-group pull-right">
															<button type="submit" v-on:click="getNext()" class="btn btn-info" :disabled="errorsFlag.step1" >{l s='NEXT'}</button>
														</div>
													</div>
												</div>
											</div>
										</div>
										</form>
										<a href="#" class="next" v-show="!errorsFlag.step1" v-on:click="getNext()">
											<img src="{$img_dir}/next.png">
										</a>

										<!-- <a href="#" class="next" v-on:click="getNext()">
											<img src="{$img_dir}/next.png">
										</a> -->
									</div>
									 <div class="registration-body step2 item">
										<div class="row">
											<div class="col-md-6 right-border">
												<div class="left-img">
													<img src="{$img_dir}/reg-step-1.png">
												</div>
											</div>
											<div class="col-md-6">
												<div class="detail-sec">
													<div class="middle">
														<div class="side-heading">
															<h3>{l s='Setup Company Profile'}</h3>
															<p>{l s='Praesent sapien massa, convallis a pellentesque nec, ege dis.'}</p>
														</div>
														
															<div class="existing clearfix">
																<input type="hidden" v-model="user.step2.already_registered">
																<span>{l s='Existing Company'} ?</span>
																<ul>
																<li id="right" v-bind:class="{ 'active': user.step2.already_registered == 0 }" 
																v-on:click="user.step2.already_registered = 0"></li>
																<li id="wrong" v-bind:class="{ 'active': user.step2.already_registered > 0 }" v-on:click="user.step2.already_registered = 1"></li>
																</ul>

															</div>

														<div  v-show="user.step2.already_registered == 0">
														<form>
															<div class="form-group">
																	
																		<input type="text" v-model="user.step2.contract_id" class="form-control" placeholder="{l s='Contract ID'}" v-on:keyup="getCompany()">
																	
																	
																<p class="helpValidation" id="contracterror">{literal} {{ errors.step2.contract_id }} {/literal}</p>
																{literal} {{ errors.step2 }} {/literal}
															</div>
															
															<div class="form-group"  v-show="user.step2.company_id" >
																<input type="text" class="form-control" readonly 
																 v-model="user.step2.companyName" >
															</div>
															<input type="hidden" v-model="user.step2.company_id">
															<div class="form-group pull-right">
																<button type="submit" class="btn btn-default" v-if="!errorsFlag.step2" v-on:click="getNext()">
																	NEXT
																</button>
															</div>
														</form>
														</div>
														<div  v-show="user.step2.already_registered > 0">
														
														<form>
															<div class="form-group">
																<input type="text" class="form-control" placeholder="{l s='Company Name'}" 
																v-model="user.step2.company_name" v-on:keyup="setDebug()">
																<p class="helpValidation">{literal} {{ errors.step2.company_name }} {/literal}</p>
															</div>


															<div class="form-group">
																<input type="text" class="form-control" placeholder="{l s='Address 1'}" 
																v-model="user.step2.address1" v-on:keyup="setDebug()">
																<p class="helpValidation">{literal} {{ errors.step2.address1 }} {/literal}</p>
															</div>


															<div class="form-group">
																<select class="form-control"  v-on:change="getStates()" v-model="user.step2.id_country" style="width:100%">
																	<option value="">{l s='Select Country'}</option>
																	<option v-for="country in countries" value="{literal}{{ country.id_country }}{/literal}"> {literal}  {{ country.name }} {/literal} </option>
																</select>
															</div>
															<div class="form-group">
																<select class="form-control"   v-on:change="setDebug()" v-model="user.step2.state" style="width:100%">
																<option value="">{l s='Select State'}</option>
																	<option v-for="state in states" value="{literal}{{ state.id_state }}{/literal}">
																	 {literal}  {{ state.name }} {/literal} </option>
																</select>
															</div>
															<div class="form-group">
																<input type="text" class="form-control" placeholder="{l s='City'}" v-on:keyup="setDebug()"
																 v-model="user.step2.city">
															</div>
															<div class="form-group">
																<input type="text" class="form-control" placeholder="{l s='Zip Code'}"v-on:keyup="setDebug()" 
																 v-model="user.step2.postcode">
																 <p class="helpValidation">{literal} {{ errors.step2.postcode }} {/literal}</p>
															</div>
															<div class="form-group pull-right">
																<button type="submit"  v-on:click="getNext()" :disabled="errorsFlag.step2" class="btn btn-info">{l s='NEXT'}</button>
															</div>
															</form>
														</div>
													</div>
												</div>
											</div>
										</div>
										<a href="#" class="pre"  v-on:click="getPrev()">
											<img src="{$img_dir}/pre.png">
											</a>
										<a href="#" class="next" v-if="!errorsFlag.step2" v-on:click="getNext()">
											<img src="{$img_dir}/next.png">
										</a>
									</div>
									<div class="registration-body step3 item">
										<div class="row">
											<div class="col-md-6 col-sm-12 col-xs-12 right-border">
												<div class="left-img">
													<img src="{$img_dir}/reg-step-3.png">
												</div>
											</div>
											<div class="col-md-6 col-sm-12 col-xs-12">
												<div class="detail-sec">
													<div class="middle">
														<div class="side-heading">
															<h3>{l s='Complete Registration'}</h3>
														</div>
														<form>
														<div class="check-group clearfix">
															<div class="checkbox">
																<input type="checkbox" id="test1" v-model="user.step3.data_privacy">
																<label for="test1"></label>
																<span>{l s='Please confirm our general '}
																{if $lang_iso == en }
																	<a href="/themes/cloudselect/docs/en/Axians-IT-Solutions_General-TCs-of-Delivery-and-Service.pdf" target="_blank">{l s='Terms & Conditions'}</a>
																{else}
																	<a href="/themes/cloudselect/docs/de/AGB-Allgemeine-Liefer-und-Leistungsbedingungen_Axians-IT-Solutions.pdf"  target="_blank">{l s='Terms & Conditions'}</a>
																{/if}
																</span>
																<span class="helpValidation" v-if="!user.step3.data_privacy"></span>
															</div>

															<div class="checkbox">
																<input type="checkbox" id="test2" v-model="user.step3.news">
																<label for="test2"></label>
																<span>{l s='Sign up for our newsletter!'}</span>
															</div>
															<!---<div class="checkbox">
																<input type="checkbox" id="test3" v-model="user.step3.special">
																<label for="test3"></label>
																<span>{l s='Receive special offers from our partners!'}</span>
															</div>--->
														</div>

														<!--<div class="captcha clearfix">
															 <div id="captcha_container"></div>
															 <p class="helpValidation" v-if="captchaError" >{l s='Prove you are not a bot'}.</p>
														</div>-->
														</form>
														<div class="form-group pull-right">
																<button type="submit" :disabled="!user.step3.data_privacy" v-on:click="register()" class="btn btn-default">{l s='Register'}</button>
														</div>
														
													</div>
												</div>
											</div>
										</div>
										<a href="#" class="pre" v-on:click="getPrev()"><img src="{$img_dir}/pre1.png"></a>
									</div>

									<div class="registration-body item">
										<div class="row">
											<div class="col-md-12" align="center">
												{l s='Thank you for your registration.'}<br> {l s='Currently your account is inactive. Please check your email and click on activation link.'}
											</div>	
										</div>
									</div>
								</div> <!-- END CAROSEL Container -->
							</div> <!-- End carosel -->
						</div> <!-- END Wrapper -->
				</div> <!-- END ROW -->
			</div> <!-- END container -->
		</section>
		<!-- content-section  close--> 
	</div>
</div>
<script type="text/javascript">	
var countries = {$countries|json_encode};
</script>

     <script src="{$js_dir}bootstrap.min.js" async defer></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.18/vue.min.js"></script>
     <!--<script src="{$js_dir}vue.min.js" async defer></script>-->
     <script src="{$js_dir}vue-resource.min.js" async defer></script>
     <script src="{$js_dir}registration.js" async defer></script>
	
<script type="text/javascript">
var confirm_password = 'Password and confirm password must be the same';
var email = 'Not a valid Email';
var firstname = 'First Name is required and must contain letters only';
var lastname = 'Last Name is required and must contain letters only';
var password = 'Password must be at least 8 characters and must contain at least one lower case letter, one upper case letter and one digit.';
var phone_no_business = 'Should contain numbers only';
</script>
