{if $enddate!=null && $enddate >0 }
	<div class="counter_out ccounter_{$idproduct}">
		<div class="out">
			<input class="knob days" data-width="50" data-height="50" data-thickness=".03" data-min="0" data-max="365" data-displayPrevious=true data-fgColor="#f9ba48" data-bgColor="#ebebeb" data-readOnly="true" value="1">
			<span class="txt fontcustom1">{l s='Days' mod='poscountdown'}</span>
		</div>
		<div class="out">
			<input class="knob hour" data-width="50" data-height="50" data-thickness=".03" data-min="0" data-max="24" data-displayPrevious=true data-fgColor="#f9ba48" data-bgColor="#ebebeb" data-readOnly="true" value="1">
			<span class="txt fontcustom1">{l s='Hours' mod='poscountdown'}</span>
		</div>
		<div class="out">
			<input class="knob minute" data-width="50" data-height="50" data-thickness=".03" data-min="0" data-max="60" data-displayPrevious=true data-fgColor="#f9ba48" data-bgColor="#ebebeb" data-readOnly="true" value="1">
			<span class="txt fontcustom1">{l s='Mins' mod='poscountdown'}</span>
		</div>
		<div class="out">
			<input class="knob second" data-width="50" data-height="50" data-thickness=".03" data-min="0" data-max="60" data-displayPrevious=true data-fgColor="#f9ba48" data-bgColor="#ebebeb" data-readOnly="true" value="0">
			<span class="txt fontcustom1">{l s='Secs' mod='poscountdown'}</span>
		</div>
	</div>
    <script type="text/javascript">
        $(document).ready(function () {
			try {
				$(".ccounter_{$idproduct}").ccountdown({$enddate|date_format:"%Y"}, {$enddate|date_format:"%m"}, {$enddate|date_format:"%d"},"{$enddate|date_format:'%H'}:{$enddate|date_format:'%M'}");
			}
			catch(err) {}
        });
    </script>
{/if}
