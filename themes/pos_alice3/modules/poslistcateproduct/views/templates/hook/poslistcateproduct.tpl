<div class="left-scroll">
<ul id="scroll-item" class="hidden-md hidden-sm hidden-xs">
	{foreach from=$categorythumbs item=categorythumb}
	<li>
		<a href="#{$categorythumb.id_category}">
		{if $categorythumb.thumb}
		<img src="{$categorythumb.thumb}" alt=""/>
		{else}
		<img src="{$link->getMediaLink("`$smarty.const._MODULE_DIR_`poslistcateproduct/images/")}menu_thumb.png"/>
		{/if}
		</a>
	</li>
	{/foreach}
</ul>
<div class="back-top mypresta_scrollup"><a href= "#"><span>{l s='back to top' mod='posscroll'}</span></a></div> 
</div>
{$count=0}
{foreach from=$productCates item=productCate name=poslistcateproduct}
<section class="poslistcateproduct poslistcateproduct_{$count} block_product" id="{$productCate.id_category}">
	<div class="pos-title">
		<div class="row">
			<div class=" col-xs-12 col-sm-5 col-md-4 col-lg-3">
				<h2>
					{$productCate.category_name}
					{if $productCate.categorythumb}
					<img src="{$productCate.categorythumb}" alt=""/>
					{else}
					<img src="{$link->getMediaLink("`$smarty.const._MODULE_DIR_`poslistcateproduct/images/")}menu_thumb.png"/>
					{/if}
				</h2>
			</div>
			<div class=" col-xs-12 col-sm-7 col-md-8 col-lg-9">
			<ul>
			{foreach from=$productCate.tabproducts item=tabproduct name=myLoop}				
				<li class="{if $smarty.foreach.myLoop.first}active{/if} title " rel="tab_{$productCate.id_category}_{$tabproduct.id}" >{$tabproduct.title}</li>				
			{/foreach}
			</ul>
			{if $configure['show_sub'] && $productCate.list_subcategories}
			<ul class="subcategories-list hidden-md hidden-sm hidden-xs">
				{foreach from=$productCate.list_subcategories item=subcategories}
					<li><a href="{$link->getCategoryLink($subcategories['id_category'])}" target="_blank">{$subcategories.name}</a></li>
				{/foreach}
			</ul>
			{/if}
			<div class="btn-group hidden-lg">
			  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<i class="icon icon-align-justify"></i>
			  </button>
			  <ul class="dropdown-menu">
				{foreach from=$productCate.list_subcategories item=subcategories}
					<li><a href="{$link->getCategoryLink($subcategories['id_category'])}" target="_blank">{$subcategories.name}</a></li>
				{/foreach}
			  </ul>
			</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class=" col-xs-12 col-sm-5 col-md-12 col-lg-3 hidden-md hidden-sm hidden-xs">
			{if $productCate.image}
			<div class="thumb-category">
				{if $productCate.url}<a href="{$productCate.url}">{/if}<img src="{$link->getMediaLink("`$smarty.const._MODULE_DIR_`poslistcateproduct/images/`$productCate.image|escape:'htmlall':'UTF-8'`")}" alt="" />{if $productCate.url}</a>{/if}
			</div>
			{/if}
		</div>
		<div class=" col-xs-12 col-sm-12 col-md-12 col-lg-9">
			<div class="pos-content row">		
				{foreach from=$productCate.tabproducts item=tabproduct name=myLoop}
						<div class="tabslider_content"  id="tab_{$productCate.id_category}_{$tabproduct.id}">
							{if $tabproduct.products}
							<div class="listcateproducts">
							{foreach from=$tabproduct.products item=product name=myLoop}
								{if $smarty.foreach.myLoop.index % $configure.row == 0 || $smarty.foreach.myLoop.first }
								<div class="listcate_item">
								{/if}
									<div class="item-product">
										<div class="products-inner">
											<a class="bigpic_{$product.id_product}_tabcategory product_image" href="{$product.link|escape:'html'}" title="{$product.name|escape:html:'UTF-8'}">
												<img class="img-responsive" src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'home_default')|escape:'html'}" alt="{$product.name|escape:html:'UTF-8'}" />	
												{hook h="rotatorImg" product=$product}			
											</a>
											 {if isset($product.new) && $product.new == 1}
												<a class="new-box" href="{$product.link|escape:'html':'UTF-8'}">
													<span class="new-label">{l s='New' mod='poslistcateproduct'}</span>
												</a>
											{/if}
											{if isset($product.on_sale) && $product.on_sale && isset($product.show_price) && $product.show_price && !$PS_CATALOG_MODE}
												<a class="sale-box" href="{$product.link|escape:'html':'UTF-8'}">
													<span class="sale-label">{l s='Sale!' mod='poslistcateproduct'}</span>
												</a>
											{/if}
											<div class="actions">							
												<div class="actions-inner">
													<ul class="add-to-links">																					
														<li>
															{hook h='displayProductListFunctionalButtons' product=$product}
														</li>
														<li>
														{if isset($comparator_max_item) && $comparator_max_item}
														  <a class="add_to_compare" href="{$product.link|escape:'html':'UTF-8'}" data-id-product="{$product.id_product}" title="{l s='Add to Compare' mod='poslistcateproduct'}">{l s='Compare' mod='poslistcateproduct'}
														
														  </a>
														 {/if}
														</li>
														<li>
															{if isset($quick_view) && $quick_view}
																<a class="quick-view" title="{l s='Quick view' mod='poslistcateproduct'}" href="{$product.link|escape:'html':'UTF-8'}">
																	<span>{l s='Quick view' mod='poslistcateproduct'}</span>
																</a>
															{/if}
														</li>	
													</ul>
												</div>
											</div>				
										</div>
										<div class="product-contents">
											<h5 class="product-name"><a href="{$product.link|escape:'html'}" title="{$product.name|truncate:80:'...'|escape:'htmlall':'UTF-8'}">{$product.name|truncate:45:'...'|escape:'htmlall':'UTF-8'}</a></h5>
											{capture name='displayProductListReviews'}{hook h='displayProductListReviews' product=$product}{/capture}
											{if $smarty.capture.displayProductListReviews}
												<div class="hook-reviews">
												{hook h='displayProductListReviews' product=$product}
												</div>
											{/if}  
											{if (!$PS_CATALOG_MODE AND ((isset($product.show_price) && $product.show_price) || (isset($product.available_for_order) && $product.available_for_order)))}
											<div class="price-box">
												{if isset($product.show_price) && $product.show_price && !isset($restricted_country_mode)}
												{hook h="displayProductPriceBlock" product=$product type='before_price'}
												<span class="price product-price">
													{if !$priceDisplay}{convertPrice price=$product.price}{else}{convertPrice price=$product.price_tax_exc}{/if}
												</span>
												{if $product.price_without_reduction > 0 && isset($product.specific_prices) && $product.specific_prices && isset($product.specific_prices.reduction) && $product.specific_prices.reduction > 0}
													{hook h="displayProductPriceBlock" product=$product type="old_price"}
													<span class="old-price product-price">
														{displayWtPrice p=$product.price_without_reduction}
													</span>
													{hook h="displayProductPriceBlock" id_product=$product.id_product type="old_price"}
													{if $product.specific_prices.reduction_type == 'percentage'}
														<span class="price-percent-reduction">-{$product.specific_prices.reduction * 100}%</span>
													{/if}
												{/if}
												{hook h="displayProductPriceBlock" product=$product type="price"}
												{hook h="displayProductPriceBlock" product=$product type="unit_price"}
												{hook h="displayProductPriceBlock" product=$product type='after_price'}
											{/if}
											</div>
											{/if}												
											<div class="cart">
												{if ($product.id_product_attribute == 0 || (isset($add_prod_display) && ($add_prod_display == 1))) && $product.available_for_order && !isset($restricted_country_mode) && $product.minimal_quantity <= 1 && $product.customizable != 2 && !$PS_CATALOG_MODE}
												{if ($product.allow_oosp || $product.quantity > 0)}
												{if isset($static_token)}
													<a class="button ajax_add_to_cart_button btn btn-default" href="{$link->getPageLink('cart',false, NULL, "add=1&amp;id_product={$product.id_product|intval}&amp;token={$static_token}", false)|escape:'html':'UTF-8'}" rel="nofollow"  title="{l s='Add to cart' mod='poslistcateproduct'}" data-id-product="{$product.id_product|intval}">
														<span>{l s='Add to cart' mod='poslistcateproduct'}</span>
														
													</a>
												{else}
												<a class="button ajax_add_to_cart_button btn btn-default" href="{$link->getPageLink('cart',false, NULL, 'add=1&amp;id_product={$product.id_product|intval}', false)|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Add to cart' mod='poslistcateproduct'}" data-id-product="{$product.id_product|intval}">
													<span>{l s='Add to cart' mod='poslistcateproduct'}</span>
												</a>
												   {/if}      
												{else}
												<span class="button ajax_add_to_cart_button btn btn-default disabled" >
													<span>{l s='Add to cart' mod='poslistcateproduct'}</span>
												</span>
												{/if}
												{/if}
											</div>		
										</div>								
									</div>
								{if $smarty.foreach.myLoop.iteration % $configure.row == 0 || $smarty.foreach.myLoop.last  }				
								</div>
								{/if}	
							{/foreach}	
							</div>
							{else}
								<p>{l s='There is no product in this tab' mod='poslistcateproduct'}</p>
							{/if}
						</div>							
				{/foreach}
			</div>
		</div>	
	</div>
	{if $productCate.description}
	<div class="description-list">
		{$productCate.description}
	</div>
	{/if}
</section>
<script type="text/javascript">

$(document).ready(function() {

	$(".poslistcateproduct_{$count} .tabslider_content").hide();
	$(".poslistcateproduct_{$count} .tabslider_content:first").show(); 

	$(".poslistcateproduct_{$count} .title").click(function() {
		$(".poslistcateproduct_{$count} .title").removeClass("active");
		$(this).addClass("active");
		$(".poslistcateproduct_{$count} .tabslider_content").hide();
		$(".poslistcateproduct_{$count} .tabslider_content").removeClass("animatetab");
		var activeTab = $(this).attr("rel"); 
		$("#"+activeTab).fadeIn().addClass("animatetab");  
	});
});

</script>
{$count= $count+1}
{/foreach}

<script type="text/javascript">
	$(document).ready(function() {
		var owl = $(".listcateproducts");
		owl.owlCarousel({
		items : {$configure.number_item},
		itemsDesktop : [1199,{$configure.items_md}],
		itemsDesktopSmall : [991,{$configure.items_sm}],
		itemsTablet: [767,{$configure.items_xs}],
		itemsMobile : [479,{$configure.items_xxs}],
		autoPlay :  {if $configure.auto_play}{if $configure.auto_time}{$configure.auto_time}{else}3000{/if}{else} false{/if},
		stopOnHover: true,
		addClassActive: true,
		slideSpeed : {if $configure.speed_slide}{$configure.speed_slide}{else}1000{/if},
		scrollPerPage: true,
		navigation : {if $configure.show_arrow} true {else} false {/if},
		pagination : {if $configure.show_pagination} true {else} false {/if},
		 afterAction: function(el){
		   this.$owlItems.removeClass('first-active')
		   this.$owlItems .eq(this.currentItem).addClass('first-active')  
		}
		});
	});
</script>			
<script>
	(function($){
		$(window).on("load",function(){
			
			/* Page Scroll to id fn call */
			$("#scroll-item a").mPageScroll2id({
				highlightSelector:"#scroll-item a",
				offset:70,
			});
		});
	})(jQuery);
</script>
