<div class="pos-logo-container col-sm-6 col-md-4 col-xs-12">
	<div class="pos-logo">
		<div class="pos-title"><h2>{l s='Our Brands' mod='poslogo'}</h2></div>
			<div class="container-inner">
				<div class="row pos-content">
					<div class="logo-slider">
						{foreach from=$logos item=logo name=myLoop}
							{if $smarty.foreach.myLoop.index % 4 == 0 || $smarty.foreach.myLoop.first }
							<div>
							{/if}
								<div class="item-banklogo">
									<a href ="{$logo.link}">
										<img class="replace-2x img-responsive" src ="{$logo.image}" alt ="{l s='Logo' mod='poslogo'}" />
									</a>
								</div>
							{if $smarty.foreach.myLoop.iteration % 4 == 0 || $smarty.foreach.myLoop.last  }
							</div>
							{/if}
						{/foreach}
					
					</div>
				</div>	
			</div>				
	</div> 
</div>
<script type="text/javascript"> 
		$(document).ready(function() {
			var owl = $(".logo-slider");
			owl.owlCarousel({
			items :2,
			addClassActive: true,
			navigation :true,
			slideSpeed :1000,
			pagination : false,
			itemsDesktop : [1199,2],
			itemsDesktopSmall : [911,2],
			itemsTablet: [767,3],
			itemsMobile : [360,2],
			afterAction: function(el){
			   this.$owlItems.removeClass('first-active')
			   this.$owlItems .eq(this.currentItem).addClass('first-active')  
			}
			}); 
		});
</script>